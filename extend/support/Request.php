<?php
/**
 * 对外请求接口
 */

namespace support;

use request\erp\UserRelationRequest;
use request\erp\LoginRequest;
use request\express\Kuaidi100ComQueryRequest;
use request\express\Kuaidi100QueryRequest;

class Request {

    /**
     * 快递100 查询
     * 订单同步
     * @param $order
     * @return Kuaidi100QueryRequest
     */
    public static function expressQuery($shipping_code, $company_code = '') {
        $request = new Kuaidi100QueryRequest();
        $request->setParams([
            'com' => $company_code,
            'num' => $shipping_code
        ]);
        $request->run();
        return $request->getResponse();
    }

    /**
     * 快递100 根据单号查企业
     * 订单同步
     * @param $shipping_code
     * @return Kuaidi100ComQueryRequest
     */
    public static function expressComQuery($shipping_code) {
        $request = new Kuaidi100ComQueryRequest();
        $request->setParams($shipping_code);
        $request->run();
        return $request->getResponse();
    }

    /**
     * ERP 登录
     *
     * @param $params
     * @return mixed
     */
    public static function erpLogin($params) {
        $request = new LoginRequest();
        $request->setParams($params);
        $request->run();
        return $request->getResponse();
    }

    /**
     * ERP 获取用户层级关系
     *
     * @param $params
     * @return mixed
     */
    public static function erpUserRelation($params) {
        $request = new UserRelationRequest();
        $request->setParams($params);
        $request->run();
        return $request->getResponse();
    }
}