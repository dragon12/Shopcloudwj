<?php
/**
 * 订单关务及仓库对接
 */

namespace ordersync;

use app\common\model\Order;
use OSS\Result\PutLiveChannelResult;

class OrderSync {

    /**
     * 179号文订单推送
     * 订单同步
     * @param $order
     * @return AllinpaySyncRequest
     */
    public static function PlatDataUpload($order) {
        $request = new PlatDataUploadSyncRequest();
        $request->setParams($order);
        $request->run();
        return $request;
    }

    /**
     * 通联支付单推送
     * 订单同步
     * @param $order
     * @return AllinpaySyncRequest
     */
    public static function AllinpaySync($order) {
        $request = new AllinpaySyncRequest();
        $request->setParams($order);
        $request->run();
        return $request;
    }

    /**
     * 万嘉跨境仓库接口 - 获取token
     * 订单同步
     */
    public static function WMSToken() {

        $request = new WMSTokenRequest();
        $request->run();
        return $request->getResponse();
    }

    /**
     * 万嘉跨境仓库接口
     * 订单同步
     * @param $order
     * @return WMSSyncRequest
     */
    public static function WMSSync($order) {
        $request = new WMSSyncRequest();
        $request->setImportType(1);
        $request->setParams($order);
        $request->run();
        return $request;
    }

    /**
     * 万嘉跨境仓库接口 - 下发撤回
     * 订单同步
     * @param $order
     * @return WMSSyncRequest
     */
    public static function WMSSyncRetract($order) {
        $request = new WMSSyncRequest();
        $request->setImportType(3);
        $request->setParams($order);
        $request->run();
        return $request;
    }

    /**
     * 万嘉跨境仓库接口
     * 订单状态同步（获取）
     */
    public static function WMSQuery($order) {
        $request = new WMSQueryRequest();
        $request->setParams($order);
        $request->run();
        return $request;
    }

    /**
     * 单一窗口
     * 订单同步
     * @param $order
     * @return CustomsSyncRequest
     */
    public static function CustomsSync($order) {
        $request = new CustomsSyncRequest();
        $request->setParams($order);
        $request->run();

        return $request;
    }

    /**
     * 单一窗口 获取回执下载列表
     * 订单同步
     * @return array
     */
    public static function CustomsQuery() {
        $request = new CustomsQueryRequest();
        $request->run();

        $request->getResponse();
        $result = $request->getFilesList();
        return $result;
    }

    /**
     * 单一窗口 下载回执文件
     * 订单同步
     * @param $download
     * @return array [\SimpleXMLElement]
     */
    public static function CustomsDownloadOneFile($download) {
        $request = new CustomsDownloadOneFileRequest();
        $request->setParams($download);
        $request->run();

        $request->getResponse();
        $result = $request->getDownloadFileInfo();
        static::CustomsDownloadSuccessNotify($download);       // 通知已下载

        return $result;
    }

    /**
     * 单一窗口 下载回执文件完成通知
     * 订单同步
     * @param $download
     * @return CustomsDownloadSuccessNotifyRequest
     */
    public static function CustomsDownloadSuccessNotify($download) {
        $request = new CustomsDownloadSuccessNotifyRequest();
        $request->setParams($download);
        $request->run();

        return $request->getResponse();
    }
}