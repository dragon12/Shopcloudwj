<?php
/**
 * 单一窗口 * 报文回执下载后通知
 */
namespace ordersync;

use ordersync\component\Customs;
use request\Request;

class CustomsDownloadSuccessNotifyRequest extends Request {

    protected $method = 'soap';
    protected $apiMethodName = 'notifySdeportDownloadSuccess';
    use Customs;

    public function __construct()
    {
        $url = 'https://edi1500.sditds.com:7702/service_1501_webservice/ws/edi/EnteTransportRecvFiles?wsdl';
        $this->setUrl($url);

        $context = stream_context_create( array (
            'ssl' => array (
                'verify_peer' => false,
                'allow_self_signed' => true
            ),
        ));
        $options['stream_context'] = $context;
        $this->setRequestOptions($options);
    }

    /**
     * 请求数据转化
     * @param array $params
     * @param int $dimension // 数组维度
     * @return array
     */
    protected function requestTransfer($params = [], $dimension = 1)
    {
        return [
            'enteLoginInfo' => [
                'loginName' => config('customs_config.loginName'),
                'loginPassword' => $this->strEncrypt(config('customs_config.loginPassword')),
            ],
            'fileTempID' => $params['file_temp_id'],
            'expandAttribute' => [
                'expandList' => null,
            ]
        ];
    }
}