<?php
/**
 * 万嘉关务 * 订单同步接口
 */
namespace ordersync;

use request\Request;
use request\RequestException;

class WMSQueryRequest extends Request {

    protected $method = 'soap';
    protected $apiMethodName = 'GetReturnStatus';

    public function __construct()
    {
        $url = rtrim(config('wms_api'), '/') . '/CrossBorderWebService/CuDeclformService.asmx?WSDL';
        $this->setUrl($url);
    }

    /**
     * 请求数据转化
     * @param $params
     * @param int $dimension // 数组维度
     * @return array
     */
    protected function requestTransfer($order, $dimension = 1) {

        return [
            'orderNo' => $order['order_sn'],
            'token' => OrderSync::WMSToken(),
        ];
    }

    /**
     * 验证接口返回数据
     * @param $response
     * @throws RequestException
     */
    protected function checkResponse($response)
    {
//        throw new RequestException('1111');
    }

    /**
     * 获取响应数据
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     * @throws RequestException
     */
    public function setResponse($response)
    {
        $objectXml = simplexml_load_string($response);//将文件转换成 对象
        $xmlJson = json_encode($objectXml);//将对象转换个JSON
        $this->response = json_decode($xmlJson,true);//将json转换成数组
        $this->checkResponse($this->response);
    }
}