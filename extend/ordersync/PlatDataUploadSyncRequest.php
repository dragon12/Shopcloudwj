<?php
/**
 * 单一窗口 * 订单同步接口
 */
namespace ordersync;

use app\common\model\Area;
use app\common\model\Goods;
use request\Request;
use request\RequestException;
use RobRichards\XMLSecLibs\XMLSecurityDSig;
use RobRichards\XMLSecLibs\XMLSecurityKey;

class PlatDataUploadSyncRequest extends Request {

    protected $method = 'post';
    protected $apiMethodName = 'uploadFiles';
    protected static $priKey;
    protected static $cert;

    public function __construct()
    {
        $url = 'https://customs.chinaport.gov.cn/ceb2grab/grab/realTimeDataUpload';
        $this->setUrl($url);
    }

    /**
     * 请求数据转化
     * @param $item
     * @param int $dimension // 数组维度
     * @return array
     */
    protected function requestTransfer($item, $dimension = 1)
    {
        $payExInfoData = json_decode($item['upload_data'], true);
        $signData = json_decode($item['sign_data'], true);
        $payExInfoData['serviceTime'] = $item['service_time'];
        $payExInfoData['signValue'] = $signData[0];
        $payExInfoStr = json_encode($payExInfoData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return [
            'payExInfoStr' => $payExInfoStr,
        ];
    }

    /**
     * @return string
     */
    public function getResultName()
    {
        return $this->getApiMethodName();
    }

    /**
     * 验证接口返回数据
     * @param $response
     * @throws RequestException
     */
    protected function checkResponse($response)
    {
        if($response['code'] != '10000') {
            throw new RequestException($response['message']);
        }
    }

    /**
     * 获取响应数据（无需获取，只做上传操作）
     * @return mixed
     */
    public function getResponse()
    {
        return true;
    }


}