<?php
/**
 * 单一窗口 * 订单同步接口
 */
namespace ordersync;

use ordersync\component\Customs;
use request\Request;

class CustomsQueryRequest extends Request {

    protected $method = 'soap';
    protected $apiMethodName = 'waitDownloadList';
    protected $otherResultNames = ['filesList'];
    protected $filesList = [];
    use Customs;

    public function __construct()
    {
        $url = 'https://edi1500.sditds.com:7702/service_1501_webservice/ws/edi/EnteTransportRecvFiles?wsdl';
        $this->setUrl($url);

        $context = stream_context_create( array (
            'ssl' => array (
                'verify_peer' => false,
                'allow_self_signed' => true
            ),
        ));
        $options['stream_context'] = $context;
        $this->setRequestOptions($options);
    }

    /**
     * @return array
     */
    public function getFilesList()
    {
        $list = isset($this->filesList['fileInfo']) ? $this->filesList['fileInfo'] : [];
        if($list && is_one_dimension($list)) $list = [$list];   // 若返回一维数组，转化为二维
        return $list;
    }

    /**
     * @param array $filesList
     */
    public function setFilesList($filesList)
    {
        $this->filesList = json_decode(json_encode($filesList), true);
    }

    /**
     * 请求数据转化
     * @param array $params
     * @param int $dimension // 数组维度
     * @return array
     */
    protected function requestTransfer($params = [], $dimension = 1)
    {
        return [
            'enteLoginInfo' => [
                'loginName' => config('customs_config.loginName'),
                'loginPassword' => $this->strEncrypt(config('customs_config.loginPassword')),
            ],
            'expandAttribute' => null
        ];
    }
}