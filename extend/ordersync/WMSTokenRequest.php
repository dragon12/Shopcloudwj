<?php
/**
 * 万嘉关务 * token 获取
 */
namespace ordersync;

use app\common\model\Area;
use request\Request;
use request\RequestException;

class WMSTokenRequest extends Request {

    protected $method = 'soap';
    protected $apiMethodName = 'GetToken';

    public function __construct()
    {
        $url = rtrim(config('wms_api'), '/') . '/CrossBorderWebService/SecurityService.asmx?WSDL';
        $this->setUrl($url);
    }

    /**
     * 请求数据转化
     * @param $params
     * @param int $dimension // 数组维度
     * @return array
     */
    protected function requestTransfer($params = null, $dimension = 1) {
        return [
            'appId' => '87-64-FB-B5-16-0E-18-A3-84-54-69-E0-62-70-4F-F8-E2-A7-27-7C-75-35-7B-FF',       // 万嘉平台APPID
            'appSecret' => 'E2-29-EB-AF-89-D4-10-F6',                                                   // 万嘉平台appSecret
        ];
    }

    /**
     * 验证接口返回数据
     * @param $response
     * @throws RequestException
     */
    protected function checkResponse($response)
    {
        if($response['Status'] == 0) {
            throw new RequestException($response['Remark']);
        }
    }

    /**
     * 获取响应数据
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response['Remark'];
    }

    /**
     * @param mixed $response
     * @throws \request\RequestException
     */
    public function setResponse($response)
    {
        $objectXml = simplexml_load_string($response);//将文件转换成 对象
        $xmlJson = json_encode($objectXml);//将对象转换个JSON
        $this->response = json_decode($xmlJson,true);//将json转换成数组

        $this->checkResponse($this->response);
    }
}