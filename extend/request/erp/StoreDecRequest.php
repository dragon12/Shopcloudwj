<?php
/**
 * ERP 库存调整
 */
namespace request\erp;

use request\Request;
use request\RequestException;

class StoreDecRequest extends BaseRequest {

    protected $method = 'post';
    protected $action = '/Api/userDeduction.html';

    /**
     * @param $member_info
     * @param int $dimension
     * @return array|mixed
     */
    protected function requestTransfer($order, $dimension = 1)
    {
        return [
            'user' => $member_info['member_name']
        ];
    }
}