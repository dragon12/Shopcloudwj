<?php
/**
 * ERP 登录
 */
namespace request\erp;

use request\Request;
use request\RequestException;

class LoginRequest extends BaseRequest {

    protected $method = 'post';
    protected $action = '/Api/verifyUserPwd.html';

    /**
     * @param $params  ['user' => ***, 'pwd' => ***]
     * @param int $dimension
     * @return array|mixed
     */
    protected function requestTransfer($params, $dimension = 1)
    {
        $params['pwd'] = $this->encrypt($params['pwd']);
        return $params;
    }
}