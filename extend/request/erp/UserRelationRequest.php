<?php
/**
 * ERP 获取用户邀请人
 */
namespace request\erp;

use request\Request;
use request\RequestException;

class UserRelationRequest extends BaseRequest {

    protected $method = 'post';
    protected $action = '/Api/getUserRelation.html';

    /**
     * @param $member_info
     * @param int $dimension
     * @return array|mixed
     */
    protected function requestTransfer($member_info, $dimension = 1)
    {
        return [
            'user' => $member_info['member_name']
        ];
    }
}