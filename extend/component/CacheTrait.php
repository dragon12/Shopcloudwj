<?php
/**
 * 表数据，缓存
 *
 * @author  Dr. Dragon
 * @explain 子类需指定 table
 *
 */

namespace component;

use think\Cache;
use think\Db;

/**
 * @mixin \Think\Model
 */
trait CacheTrait
{
    public function rm($name) {
        return Cache::rm($this->getCacheKey($name));
    }

    public function remember($name, $callback) {
        return Cache::remember($this->getCacheKey($name), $callback);
    }

    public function getCacheKey($name) {
        return static::class .'_'.$name;
    }
}