<?php
/**
 * 手机短信类
 */

namespace sendmsg;
use sendmsg\Sms\AliyunSms;
use think\Db;
use think\Log;

class Sms
{

    // 获取余额
    public function get_balance()
    {
        $user_id = urlencode(config('smscf_wj_username')); // 这里填写用户名
        $key = urlencode(config('smscf_wj_key')); // 这里填接口安全密钥
        $target = 'http://106.ihuyi.cn/webservice/sms.php?method=GetNum';
        $post_data = "account=".$user_id ."&password=".$key;
        $gets = $this-> xml_to_array($this->post($post_data, $target));

        if($gets['GetNumResult']['code'] == 2){
            return $gets['GetNumResult']['num'];
        } else {
            return -1;
        }
    }

    /*     * 发送手机短信
     * @param unknown $mobile 手机号
     * @param unknown $params 短信内容
    */
    public function send($mobile, $type_code, $params, $membertpl = false)
    {
        $temp_info = Db::name('mailmsgtemlates')->where(['mailmt_code' => $type_code])->find();

        // 发送短信
        (new AliyunSms())->send($mobile, $temp_info['mailmt_temp_code'], $params);
        $temp_info['sms_log'] = sc_replace_text($temp_info['mailmt_content'], $params);
        return $temp_info;
    }

    /*
    您于{$send_time}绑定手机号，验证码是：{$verify_code}。【{$site_name}】
    -1	没有该用户账户
    -2	接口密钥不正确 [查看密钥]不是账户登陆密码
    -21	MD5接口密钥加密不正确
    -3	短信数量不足
    -11	该用户被禁用
    -14	短信内容出现非法字符
    -4	手机号格式不正确
    -41	手机号码为空
    -42	短信内容为空
    -51	短信签名格式不正确接口签名格式为：【签名内容】
    -6	IP限制
   大于0 短信发送数量
    http://utf8.api.smschinese.cn/?Uid=本站用户名&Key=接口安全秘钥&smsMob=手机号码&smsText=验证码:8888
    */
    private function mysend_sms($mobile, $content)
    {
        $user_id = urlencode(config('smscf_wj_username')); // 这里填写用户名
        $key = urlencode(config('smscf_wj_key')); // 这里填接口安全密钥
        if (!$mobile || !$content || !$user_id || !$key)
            return false;
        if (is_array($mobile)) {
            $mobile = implode(",", $mobile);
        }
        $mobile=urlencode($mobile);
        $content=urlencode($content);
        $target = 'http://106.ihuyi.cn/webservice/sms.php?method=Submit';
        $post_data = "account=".$user_id ."&password=".$key ."&mobile=".$mobile."&content=".$content;
        $gets = $this-> xml_to_array($this->post($post_data, $target));

        $data = date("Y-m-d H:i:s").' 返回码 : '. $gets['SubmitResult']['code'] .', 返回描述 : '.$gets['SubmitResult']['msg'].' . 发送号码 : '.$mobile.' , 短信详情 : '.urldecode($content).PHP_EOL;
        Log::write($data);

        if($gets['SubmitResult']['code'] == 2){
            return true;
        } else {
            return false;
        }

    }

    //请求数据到短信接口，检查环境是否 开启 curl init。
    private function post($curlPost,$url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
        $return_str = curl_exec($curl);
        curl_close($curl);
        return $return_str;
    }

    //将 xml数据转换为数组格式。
    private function xml_to_array($xml){
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if(preg_match_all($reg, $xml, $matches)){
            $count = count($matches[0]);
            for($i = 0; $i < $count; $i++){
                $subxml= $matches[2][$i];
                $key = $matches[1][$i];
                if(preg_match( $reg, $subxml )){
                    $arr[$key] = $this-> xml_to_array( $subxml );
                }else{
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }
}
