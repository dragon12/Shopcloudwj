<?php

/**
 * 微信支付接口类
 * JSAPI 适用于微信内置浏览器访问WAP时支付
 */
class allinpay_minipro {

    protected $config;

    public function __construct($payment_info = array()) {

        require_once PLUGINS_PATH . '/payments/allinpay/lib/AppUtil.php';
        $this->config = $payment_info['payment_config'];
    }

    public function get_payform($order_info) {
        $params = array();
        $paytype=$this->config['sub_payment_code'];

        $acct = $order_info['openid'];
        $params["sub_appid"] = $this->config['allinpay_sub_appid2'];

        $params["cusid"] = $this->config['allinpay_mch_id'];
        $params["appid"] = $this->config['allinpay_appid'];
        $params["version"] = '11';
        $params["trxamt"] = bcmul($order_info['api_pay_amount'] , 100);
        $params["reqsn"] = $order_info['pay_sn'].'_'.mt_rand(10000, 99999);//订单号,自行生成
        $params["paytype"] = $paytype;
        $params["randomstr"] = TIMESTAMP.rand(1000,9999);//
        $params["body"] = config('site_name') . $order_info['pay_sn'] . '订单';
        $params["acct"] = $acct;
        $params["notify_url"] = str_replace('/index.php', '', HOME_SITE_URL) . '/payment/allinpay_minipro_notify.html';
        $params["sign"] = \AppUtil::SignArray($params,$this->config['allinpay_key']);//签名

        $paramsStr = \AppUtil::ToUrlParams($params);
        //测试地址：https://test.allinpaygd.com/apiweb/unitorder/pay
        //测试商户号：990581007426001
        //测试appid：00000051
        //测试key：allinpay888
        $url = "https://vsp.allinpay.com/apiweb/unitorder/pay";
        $rsp = http_request($url,'POST', $paramsStr);

        // 记录支付请求
        \think\Db::name('orderpay')->where(['pay_id' => $order_info['pay_id']])->update([
            'request_data' => json_encode([
                'url'   => $url,
                'params_str' => $paramsStr
            ])
        ]);

        $res = json_decode($rsp, true);

        if(!empty($res['errmsg'])) {
            throw new \think\Exception($res['errmsg']);
        }
    
        header('Content-Type:application/json');
        output_data(json_decode($res['payinfo'], true));
        die;
        
    }

}
