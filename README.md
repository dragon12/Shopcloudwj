# Shopcloudb2c

#### Description
B2C商城

#### Software Architecture
Software architecture description

## 安装部署
	本程序依赖Thinkphp5框架，推荐框架版本 ThinkPHP5.0.23完整版
	
## 相关依赖SDK安装
	1.阿里云OSS  composer require aliyuncs/oss-sdk-php   
	介绍地址：https://help.aliyun.com/document_detail/32099.html?spm=5176.87240.400427.47.eaLg1R
	2.phpmailer  composer require phpmailer/phpmailer

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
