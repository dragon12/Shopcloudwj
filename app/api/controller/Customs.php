<?php

namespace app\api\controller;

use think\Db;

class Customs extends BaseApi {

    /**
     * 179号文下发接收
     */
    public function check() {

        $openReq = $this->request->param('openReq', '', 'trim');
        $openReq = json_decode($openReq, true);

        if(!$openReq) {
            exit(json_encode([
                "code" => "20000",
                "message" => "参数错误",
                "serviceTime" => time() * 1000
            ], JSON_UNESCAPED_UNICODE));
        }

        $order_no = $openReq['orderNo'];    // 申报订单的订单编号
        $session_id = $openReq['sessionID'];   // 海关发起请求时，平台接收的会话ID。
        $service_time = $openReq['serviceTime']; // 调用时的系统时间

        // 记录
        Db::name('platdataopen')->insert([
            'order_no' => $order_no,
            'session_id' => $session_id,
            'service_time' => $service_time,
            'create_time' => time(),
            'update_time' => time(),
        ]);
        exit(json_encode([
            "code" => "10000",
            "message" => "",
            "serviceTime" => $service_time
        ], JSON_UNESCAPED_UNICODE));
    }
}