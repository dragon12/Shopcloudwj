<?php
namespace app\common\validate;
use think\Validate;

class Country extends Validate
{
    protected $rule = [
        ['country_name', 'require', '地区名称不能为空'],
        ['country_order', 'number', '排序必须为数字'],
//        ['area_region', 'length:0,9', '大区名称必须小于三个字符'],
    ];

    protected $scene = [
        'add' => ['country_name', 'country_order', 'country_code'],
        'edit' => ['country_name', 'country_order', 'country_code'],
    ];
}