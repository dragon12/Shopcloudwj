<?php

namespace app\common\model;


use think\Model;

class Country extends Model
{

    // 获取字典数据
    public function dictGet() {
        //获取所有的国家，转换成键值对形式输出
        $country_list = db('country')->field('*')->select();
        $list = [];
        foreach ($country_list as $k => $item){
            $list[$item['country_id']] = $item['country_name'];
        }
        return $list;
    }

    // 获取字典数据
    public function dictIDCodeGet() {
        //获取所有的国家，转换成键值对形式输出
        $country_list = db('country')->field('*')->select();
        $list = [];
        foreach ($country_list as $k => $item){
            $list[$item['country_id']] = $item['country_code'];
        }
        return $list;
    }

    // 获取字典数据
    public function dictIDHSCodeGet() {
        //获取所有的国家，转换成键值对形式输出
        $country_list = db('country')->field('*')->select();
        $list = [];
        foreach ($country_list as $k => $item){
            $list[$item['country_id']] = $item['country_hs_code'];
        }
        return $list;
    }

    // 获取字典数据 - 图标
    public function dictIDIconGet() {
        //获取所有的国家，转换成键值对形式输出
        $country_list = db('country')->field('*')->select();
        $list = [];
        foreach ($country_list as $k => $item){
            $list[$item['country_id']] = country_thumb($item['country_icon']);
        }
        return $list;
    }

    /**
     * 新增国家
     * @access public
     * @author yunku
     * @param array $data 参数内容
     * @return boolean
     */
    public function addCountry($data)
    {
        // 删除缓存
        $this->dropCache();
        return db('country')->insertGetId($data);
    }

    /**
     * 更新编辑信息
     * @access public
     * @author yunku
     * @param array $data 参数内容
     * @param array $condition 条件
     * @return bool
     */
    public function editCountry($data = array(), $condition = array())
    {
        // 删除缓存
        $this->dropCache();
        return db('country')->where($condition)->update($data);
    }
    /**
     * 删除国家
     * @access public
     * @author yunku
     * @param unknown $condition 条件
     * @return boolean
     */
    public function delCountry($condition)
    {
        // 删除缓存
        $this->dropCache();
        return db('country')->where($condition)->delete();
    }


    /**
     * 删除缓存数据
     * @access public
     * @author yunku
     */
    public function dropCache()
    {
        $this->cachedData = null;

        dkcache('country');
    }


    /**
     * 获取国家列表
     * @access public
     * @author yunku
     * @param array $condition 条件
     * @param obj $fields 字段
     * @param array $group 分组
     * @param int $limit 数量限制
     * @param str $order 排序
     * @return array
     */
    public function getCountryList($condition = array(), $fields = '*', $group = '', $limit = '', $order='')
    {
        return db('country')->where($condition)->field($fields)->limit($limit)->order($order)->group($group)->select();
    }


    /**
     * 从缓存获取分类 通过分类id
     * @access public
     * @author yunku
     * @param int $id 分类id
     * @return array
     */
    public function getAreaInfoById($id)
    {
        $data = $this->getCache();
        return $data['name'][$id];
    }

    protected function getCache()
    {
        // 对象属性中有数据则返回
        if ($this->cachedData !== null)
            return $this->cachedData;

        // 缓存中有数据则返回
        if ($data = rkcache('country')) {
            $this->cachedData = $data;
            return $data;
        }

        // 查库
        $data = $this->_getAllCOuntry();
        wkcache('country', $data);
        $this->cachedData = $data;

        return $data;
    }
    protected $cachedData;

    /**
     * 获取所有地区
     * @access public
     * @author yunku
     * @return array
     */
    private function _getAllCountry()
    {
        $data = array();
        $area_all_array = db('country')->limit(false)->select();

        static $member = [];
        foreach ((array)$area_all_array as $a) {
            if(!isset($member[$a['member_id']])) {
                if($a['member_id'] != 0) {
                    $m = Member::get($a['member_id']);
                    $member[$a['member_id']] = [
                        'member_id' => $m['member_id'],
                        'member_name' => $m['member_name'],
                    ];
                } else {
                    $member[$a['member_id']] = [
                        'member_id' => 0,
                        'member_name' => '',
                    ];
                }
            }

            $data['name'][$a['country_id']] = $a['area_name'];
            $data['area_member'][$a['country_id']] = $member[$a['member_id']];
            $data['member_area'][$a['member_id']][] = $a['country_id'];
            $data['children'][$a['area_parent_id']][] = $a['area_id'];

        }

        wkcache('country', $data);
        $this->cachedData = $data;

        return $data;
    }

    /**
     * 获取地国家详情
     * @access public
     * @author yunku
     * @param int $condition 条件
     * @param array $fileds 字段
     * @return array
     */
    public function getCountryInfo($condition = array(), $fileds = '*')
    {
        return db('country')->where($condition)->field($fileds)->find();
    }

}