<?php

namespace app\common\model;


use think\Db;
use think\Model;

class Complaint extends Model
{
    public $page_info;
    /**
     * 投诉数量
     * @access public
     * @author yunku
     * @param array $condition 检索条件
     * @return int
     */
    public function getComplaintCount($condition)
    {
        return db('complaint')->where($condition)->count();
    }

    /**
     * 添加订单投诉
     * @access public
     * @author yunku 
     * @param array $data 参数内容
     * @return int
     */
    public function addComplaint($data)
    {
//        $complaint_list =$this->getComplaintList($data);
//        $condition = array();
//        foreach ($complaint_list as $k => $item){
//            $condition = $item;
//            if($condition['order_id'] === $data['order_id'] ){
//                throw new Exception('此订单您已投诉，请勿重复投诉！');
//            }
//        }
        return db('complaint')->insertGetId($data);
    }

    /**
     * 订单投诉列表
     * @access public
     * @author yunku 
     * @param array $condition 检索条件
     * @param str $field 字段
     * @param int $page 分页信息
     * @param str $order 排序
     * @return array
     */
    public function getComplaintList($condition, $field = '*', $page=10, $order = 'complaint_id desc')
    {
         $res=db('complaint')->where($condition)->field($field)->order($order)->paginate($page,false,['query' => request()->param()]);
         $this->page_info=$res;
         return $res->items();
    }
    /**
     * 获取投诉信息
     * @access public
     * @author yunku 
     * @param array $condition 投诉条件
     * @return array
     */
    public function getComplaintInfo($condition)
    {
        return db('complaint')->where($condition)->find();
    }

    /**
     * 删除投诉
     * @access public
     * @author yunku
     * @param array $condition 检索条件
     */
    public function delComplaint($condition)
    {
        return db('complaint')->where($condition)->delete();
    }

    /**
     * 回复投诉
     * @access public
     * @author yunku 
     * @param array $condition 条件
     * @param array $data 参数内容
     * @return type
     */
    public function editComplaint($condition, $data)
    {
        $data['consult_replytime'] = TIMESTAMP;
        return db('complaint')->where($condition)->update($data);
    }
}