<?php


namespace app\common\model;

use think\Model;

class MemberGroup extends Model
{

    /**
     * 客户组
     */
    public static $member_group = [
        MEMBER_AGENT_GROUP    => '批发商',
        MEMBER_CUSTOMER_GROUP => '普通会员',
    ];
    /**
     * 级别
     */
    public static $member_level = [
        MEMBER_GROUP_LEVEL_1 => '一级',
        MEMBER_GROUP_LEVEL_2 => '二级',
    ];

    # 会员组字典
    public function dictGroupGet()
    {
        return self::$member_group;
    }
    # 会员组级别
    public function dictTypeGet()
    {
        return self::$member_level;
    }

    /**
     * group_config
     */
    public function getGroupConfigAttr($v)
    {
        return $v ? json_decode($v, true) : [];
    }

    /**
     * 获取当前会员组列表信息
     */
    public function getMemberGroupConfig()
    {
        # 查库
        $data = $this->all();

        # 组合
        $return = [];
        foreach ($data as $value) {
            $item = $value['group_config'];
            $return[$item['member_group']] = [ // member_group 1=>代理商 2=>普通会员
                # 1级
                1 => [
                    'group_name'        => $value['group_name'],
                    'level_name'        => '一级' . $value['group_name'],
                    'commission_rate'   => $item['commission_rate_1'], // 佣金比例
                    'price'             => $item['price_1'],           // 价格比例
                    'read_bonded_goods' => $item['read_bonded_goods'], // 是否可以查看保税货物信息
                ],
                # 2级
                2 => [
                    'group_name'        => $value['group_name'],
                    'level_name'        => '二级' . $value['group_name'],
                    'commission_rate'   => $item['commission_rate_2'], // 佣金比例
                    'price'             => $item['price_2'],           // 价格比例
                    'read_bonded_goods' => $item['read_bonded_goods'], // 是否可以查看保税货物信息
                ],
            ];
        }

        return $return;
    }

}
