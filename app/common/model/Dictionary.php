<?php

namespace app\common\model;

use think\Model;

class Dictionary extends Model {

    //查询字典单位
    public function getUnitList($condition, $field = '*', $page = 0, $order = 'key asc') {
        return db('dictionary')->where($condition)->field($field)->order($order)->select();
    }
}