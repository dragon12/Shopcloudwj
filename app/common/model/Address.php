<?php

namespace app\common\model;

use think\Model;

class Address extends Model {

    /**
     * 取得买家默认收货地址
     * @author yunku
     * @param array $condition 获取条件
     * @param string $order  排序
     * @return array
     */
    public function getDefaultAddressInfo($condition = array(), $order = 'trade_mode asc, address_is_default desc,address_id desc') {
        return $this->getAddressInfo($condition, $order);
    }

    /**
     * 取得单条地址信息
     * @author yunku
     * @param array $condition 条件
     * @param type $order 排序
     * @return string
     */
    public function getAddressInfo($condition, $order = '') {
        $addr_info = db('address')->where($condition)->order($order)->find();
        return $addr_info;
    }

    /**
     * 读取地址列表
     * @author yunku
     * @param array $condition 查询条件
     * @param type $order 排序
     * @return array  数组格式的返回结果
     */
    public function getAddressList($condition, $order = 'address_id desc') {
        $address_list = db('address')->where($condition)->order($order)->select();
        if (empty($address_list))
            return array();

        foreach ($address_list as $key => $item) {
            $address_list[$key]['trade_mode_text'] = dict_convert('trade_mode_short', $item['trade_mode']);
        }
        return $address_list;
    }

    /**
     * 取数量
     * @author yunku
     * @param array $condition 条件
     * @return int
     */
    public function getAddressCount($condition = array()) {
        return db('address')->where($condition)->count();
    }

    /**
     * 新增地址
     * @author yunku
     * @param array $data 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addAddress($data) {
        //当设置为默认地址，则此用户其他的地址设置为非默认地址
        if($data['address_is_default'] == 1){
            db('address')->where([
                'member_id' => $data['member_id'],
                'trade_mode' => $data['trade_mode'],
            ])->update(array('address_is_default' => 0));
        }

        $address_id = isset($data['address_id']) ? $data['address_id'] : 0;
        if(isset($data['address_id'])) unset($data['address_id']);
        if($address_id) {
            if(isset($data['trade_mode'])) unset($data['trade_mode']);
            $this->editAddress($data, ['address_id' => $address_id]);
            return $address_id;
        } else {
            if ($data['trade_mode'] == 2) {
                // 添加实名认证通知
                $this->realNameValidCheck($data['member_id']);
            }
            return db('address')->insertGetId($data);
        }
    }

    // 实名认证检查
    public function realNameValidCheck($member_id) {
        $check = Address::where([
            'member_id' => $member_id,
            'trade_mode' => TRADE_BBC
        ])->find();
        if($check) return;
        // 发送实名认证通知
    }

    /**
     * 取单个地址
     * @author yunku
     * @param int $id 地址ID
     * @return array 数组类型的返回结果
     */
    public function getOneAddress($id) {
        if (intval($id) > 0) {
            $result = db('address')->where('address_id',intval($id))->find();
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 更新地址信息
     * @author yunku
     * @param array $update 更新数据
     * @param array $condition 更新条件
     * @return bool 布尔类型的返回结果
     */
    public function editAddress($update, $condition) {
        //当设置为默认地址，则此用户其他的地址设置为非默认地址
        if($update['address_is_default'] == 1 && $condition['member_id'] > 0){
            db('address')->where([
                'member_id' => $condition['member_id'],
                'trade_mode' => $update['trade_mode'],
            ])->update(array('address_is_default' => 0));
        }

        if (isset($update['trade_mode']) && $update['trade_mode'] == 2 && isset($condition['member_id'])) {
            // 添加实名认证通知
            $this->realNameValidCheck($update['member_id']);
        }
        return db('address')->where($condition)->update($update);
    }

    /**
     * 验证地址是否属于当前用户
     * @author yunku
     * @param array $member_id 会员id
     * @param array $address_id 地址id
     * @return bool 布尔类型的返回结果
     */
    public function checkAddress($member_id, $address_id) {
        /**
         * 验证地址是否属于当前用户
         */
        $check_array = self::getOneAddress($address_id);
        if ($check_array['member_id'] == $member_id) {
            unset($check_array);
            return true;
        }
        unset($check_array);
        return false;
    }

    /**
     * 删除地址
     * @author yunku
     * @param array $condition记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delAddress($condition) {
        return db('address')->where($condition)->delete();
    }

}
