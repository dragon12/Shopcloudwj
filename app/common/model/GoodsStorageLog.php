<?php


namespace app\common\model;

use think\Model;

class GoodsStorageLog extends Model{

    public function addStorage($data){
        $data["ip"] = $_SERVER['REMOTE_ADDR'];
        $data["create_time"] = time();
        $data["order_sn"] = empty($data["order_sn"]) ? time().rand(1000,9999) : $data["order_sn"];
        $res = db("goods_storage_log")->insertGetId($data);
        return $res;
    }


    public function addStorageAll($data){
        $data_sort = [];
        foreach ($data as $k => $v){
            $data_sort[$k] = $v;
            $data_sort[$k]["ip"] = $_SERVER['REMOTE_ADDR'];
            $data_sort[$k]["create_time"] = time();
            $data_sort[$k]["order_sn"] = empty($v["order_sn"]) ? time().rand(1000,9999) : $v["order_sn"];
        }
        $res = db("goods_storage_log")->insertAll($data_sort);
        return $res;
    }

}