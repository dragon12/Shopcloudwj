<?php

namespace app\common\model;
use think\Db;
use think\Model;

class Inviter extends Model {

    public $page_info;


    /**
     * 是否允许有直属
     * @param $inviter
     * @return bool
     */
    public static function allowDistribute($inviter) {
        return ($inviter['inviter_group'] == MEMBER_DOCTOR_GROUP && $inviter['inviter_type'] == MEMBER_TYPE_1) || $inviter['inviter_type'] == MEMBER_TYPE_2 || $inviter['inviter_id'] == DEFAULT_AGENT || $inviter['inviter_type'] == MEMBER_TYPE_1;
    }

    /**
     * 获取ID
     * @param $inviter
     * @return int
     */
    public static function getDistributeId($inviter) {
        return static::allowDistribute($inviter) ? $inviter['inviter_id'] : 0;
    }

    /**
     * 增加帮助
     * @access public
     * @author yunku
     * @param type $inviter_array 帮助内容
     * @param type $upload_ids 更新ID
     * @return type
     */
    public function addInviter($inviter_array) {
        $inviter_id = db('inviter')->insertGetId($inviter_array);

        return $inviter_id;
    }

    /**
     * 变更积分
     * @access public
     * @author yunku
     * @param type $change_type
     * @param type $data
     * @return type
     */
    public function changePoints($inviter_type, $change_type, $data = array()) {
        $data_log = array();
        $data_change = array();
        $data_msg = array();

        $data_log['lg_member_id'] = $data['member_id'];
        $data_log['lg_member_name'] = $data['member_name'];
        $data_log['lg_member_type'] = $data['member_type'];
        $data_log['lg_addtime'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = url('home/Predeposit/pd_log_list');
        switch ($change_type) {
            case 'order_inviter':
                $data_log['lg_av_points'] = $data['points'];
                $data_log['lg_desc'] = $data['lg_desc'];
                $data_log['lg_order_sn'] = $data['order_sn'];
                $data_change['inviter_points'] = Db::raw('inviter_points+'.$data['points']);
                $data_change['inviter_points_amount'] = Db::raw('inviter_points_amount+'.$data['points']);
                break;
            case 'settle_inviter':
                $data_log['lg_av_points'] = -$data['points'];
                $data_log['lg_desc'] = $data['lg_desc'];
                $data_log['lg_order_sn'] = $data['order_sn'];
                $data_change['inviter_points'] = Db::raw('inviter_points-'.$data['points']);
                $data_change['inviter_settle_time'] = time();
                break;
            default:
                exception('参数错误');
                break;
        }

        if($inviter_type == 3) {
            $data_change['member_points'] = Db::raw('member_points+'.$data['points']);
            model('member')->editMember(array('member_id' => $data['member_id']), $data_change);
        } else {
            model('inviter')->editInviter(array('inviter_id' => $data['member_id']), $data_change);
        }

        $insert = db('inviter_points_log')->insertGetId($data_log);
//        if (!$insert) {
//            exception('操作失败');
//        }

        // 支付成功发送买家消息
//        $message = array();
//        $message['code'] = 'predeposit_change';
//        $message['member_id'] = $data['member_id'];
//        $data_msg['av_amount'] = sc_price_format($data_msg['av_amount']);
//        $data_msg['freeze_amount'] = sc_price_format($data_msg['freeze_amount']);
//        $message['param'] = $data_msg;
//        \mall\queue\QueueClient::push('sendMemberMsg', $message);
        return $insert;
    }

    /**
     * 积分日志列表
     * @access public
     * @author yunku
     * @param type $condition
     * @param type $page
     * @param type $field
     * @return type
     */
    public function getPointslogList($condition, $page = '', $field = '*',$limit='') {
        $order = isset($condition['order']) ? $condition['order'] : 'lg_id desc';
        if ($page) {
            $result = db('inviter_points_log')->where($condition)->field($field)->order($order)->paginate($page,false,['query' => request()->param()]);
            $this->page_info = $result;
            return $result->items();
        } else {
            return db('inviter_points_log')->where($condition)->field($field)->order($order)->limit($limit)->select();
        }
    }

    /**
     * 修改帮助记录
     * @access public
     * @author yunku
     * @param  array $condition 条件
     * @param  array $data 数据
     * @return boolean
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editInviter($condition, $data) {
        if (empty($condition)) {
            return false;
        }
        if (is_array($data)) {
            $result = db('inviter')->where($condition)->update($data);
            return $result;
        } else {
            return false;
        }
    }

    public function getInviterInfo($condition,$fields = 'm.member_name, m.inviter_id as parent_inviter_id,i.*'){
        if (empty($condition)) {
            return false;
        }
        $result = db('inviter')->alias('i')->join('__MEMBER__ m', 'i.inviter_id=m.member_id')->field($fields)->where($condition)->find();
        $result and isset($result['inviter_state']) and $result['inviter_state_format'] = config('inviter_state.'.$result['inviter_state']);
        return $result;
    }

    /**
     * 帮助记录
     * @access public
     * @author yunku
     * @param type $condition 条件
     * @param type $page 分页
     * @param type $limit 限制
     * @param type $fields 字段
     * @return array
     */
    public function getInviterList($condition = array(), $page = '', $limit = '', $fields = '*') {
        if($page) {
            $res=db('inviter')->alias('i')->join('__MEMBER__ m', 'i.inviter_id=m.member_id')->field($fields)->where($condition)->order('inviter_applytime desc')->paginate($page,false,['query' => request()->param()]);
            $this->page_info=$res;
            $result=$res->items();
        }else{
            $result = db('inviter')->alias('i')->join('__MEMBER__ m', 'i.inviter_id=m.member_id')->field($fields)->where($condition)->limit($limit)->order('inviter_applytime desc')->select();
        }
        return $result;
    }

    /**
     * 新增店铺
     * @author yunku
     * @param array $data 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function inviterAdd($data) {
        return db('inviter')->insertGetId($data);
    }

    /**
     * 读取列表
     * @author yunku
     * @param array $condition 查询条件
     * @param type $order 排序
     * @return array  数组格式的返回结果
     */
    public function getListInviter($condition, $order = 'inviter_id desc') {
        $inviter_list = db('inviter')->where($condition)->order($order)->select();
        if (empty($inviter_list))
            return array();
        return $inviter_list;
    }

    /**
     * 更新地址信息
     * @author yunku
     * @param array $update 更新数据
     * @param array $condition 更新条件
     * @return bool 布尔类型的返回结果
     */
    public function changeInviterType($update, $condition) {
        return db('inviter')->where($condition)->update($update);
    }


}
