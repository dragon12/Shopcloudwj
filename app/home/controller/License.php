<?php

namespace app\home\controller;

use app\admin\controller\Db;
use think\Lang;
use think\Loader;

class License extends BaseMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/index.lang.php');
    }

    public function index() {
        $id = $this->request->param('id',0,'intval');
        $upload_file = UPLOAD_SITE_URL.'/'.ATTACH_COMMON;

        if($id==1){
            $business_license= $upload_file.'/'.config('business_license_1');
        }else{
            $business_license = $upload_file.'/'.config('business_license_2');
        }

        $this->assign('business_license', $business_license);
        return $this->fetch($this->template_dir . 'index');
    }

    /**
     * 获取营业执照
     */
    public function license (){

    }
}
