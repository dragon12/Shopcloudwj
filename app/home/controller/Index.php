<?php

namespace app\home\controller;

use app\admin\controller\Db;
use think\Lang;
use think\Loader;

class Index extends BaseMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/index.lang.php');
    }

    public function index() {
        // PC 功能隐藏
        die();
        $this->assign('index_sign', 'index');

        $this->getIndexData();
        //国家
        $country_list = db('country')->order('country_order desc')->limit(5)->select();
        $this->assign('country_list', $country_list);

        //楼层数据
        $floor_block=array();
        $where = array();
        $where['gc_parent_id'] = 0;
        $where['gc_show'] = 1;
        $where['gc_sort'] = ['gt', 0];
	    $goodsclass_list = db('goodsclass')->where($where)->order('gc_sort asc')->select();
	    $group_trade_mode = [];
        foreach ($goodsclass_list as $key => $goodsclass) {
            $floor_list = $this->getFloorList($goodsclass['gc_id']);
            foreach ($floor_list['goods_list'] as $k => $item){
                //dump($item['gc_list']);
                foreach ($item['gc_list'] as $val){
                    $val['trade_mode_format'] = config('trade_mode.'.$val['trade_mode']);
                    $group_trade_mode[] = $val;
                }
                $floor_list['goods_list'][$k]['gc_list'] = $group_trade_mode;
            };
            $floor_block[$key] = $floor_list;
            $floor_block[$key]['gc_name'] = $goodsclass['gc_name'];
           // dump( $floor_block[$key]['gc_list']);
        }
      //  dump($floor_block);
        //楼层数据
        $this->assign('floor_block', $floor_block);

        //显示订单信息
        if (session('is_login')) {
            //交易提醒 - 显示数量
            $order_model = model('order');
            $member_order_info['order_nopay_count'] = $order_model->getOrderCountByID(session('member_id'), 'NewCount');
            $member_order_info['order_noreceipt_count'] = $order_model->getOrderCountByID(session('member_id'), 'SendCount');
            $member_order_info['order_noeval_count'] = $order_model->getOrderCountByID(session('member_id'), 'EvalCount');
            $this->assign('member_order_info', $member_order_info);
        }
        //SEO 设置
        $seo = model('seo')->type('index')->show();
        $this->_assign_seo($seo);
        return $this->fetch($this->template_dir . 'index');
    }
    
    private function getIndexData()
    {
        $goods_model = new \app\common\model\Goods();
        $index_data = rcache("index_data");
        if (empty($index_data)) {
            $index_data = array();
            $index_data['recommend_list'] = $goods_model->getGoodsOnlineList(array('goods_commend' => !0), 'goods_id,goods_name,goods_advword,goods_image,goods_promotion_price,goods_price,trade_mode', 0, '',5,'goods_commonid');
            $group = [];
            foreach ($index_data['recommend_list'] as $item){
                $item['trade_mode_format'] = config('trade_mode.'.$item['trade_mode']);
                $group[] = $item;
            }
            $index_data['recommend_list'] = $group;


            //国家馆精选
            $index_data['country_recommend_list'] = $goods_model->getGoodsOnlineList(array(
                'home_country_recommend' => ['egt', 0],
            ), '*', 0, '',5,'goods_commonid');
            //跨境购
            $index_data['cross_border_list'] = $goods_model->getGoodsOnlineList(array(
                'is_self_operated' => 1,
            ), '*', 0, '',5,'goods_commonid');

            //限时折扣
            $index_data['promotion_list'] = model('pxianshigoods')->getXianshigoodsCommendList(5);

            $index_data['new_list'] = model('goods')->getGoodsOnlineList(array(), '', '', 'goods_addtime desc', 5,'goods_commonid');
            $group_new_list = [];
            foreach ($index_data['new_list'] as $item){
                $item['trade_mode_format'] = config('trade_mode.'.$item['trade_mode']);
                $group_new_list[] = $item;
            }
            $index_data['new_list'] = $group_new_list;

            $index_data['groupbuy_list'] = model('groupbuy')->getGroupbuyCommendedList(5);
            $group_groupbuy_list = [];
            foreach ($index_data['groupbuy_list'] as $item){
                $item['trade_mode_format'] = config('trade_mode.'.$item['trade_mode']);
                $group_groupbuy_list[] = $item;
            }
            $index_data['groupbuy_list'] = $group_groupbuy_list;

            //友情链接
            $index_data['link_list'] = model('link')->getLinkList();
            //获取第一文章分类的前三篇文章
            $index_data['index_articles'] = db('article')->where('ac.ac_code', 'notice')->where('a.article_show', 1)->alias('a')->field('a.article_id,a.article_url,a.article_title')->order('a.article_sort asc,a.article_time desc')->limit(3)->join('__ARTICLECLASS__ ac', 'a.ac_id=ac.ac_id')->select();
            wcache('index_data',$index_data);
        }
        $this->assign('country_recommend_list', $index_data['country_recommend_list']);
        $this->assign('cross_border_list', $index_data['cross_border_list']);
        $this->assign('recommend_list', $index_data['recommend_list']);
        $this->assign('promotion_list', $index_data['promotion_list']);
        $this->assign('new_list', $index_data['new_list']);
        $this->assign('groupbuy_list', $index_data['groupbuy_list']);
        $this->assign('link_list', $index_data['link_list']);
        $this->assign('index_articles', $index_data['index_articles']);
    }
    

    private function getFloorList($cate_id) {
        $prefix = 'home-index-floor-';
        $result = rcache($cate_id,$prefix);
        if (empty($result)) {
            //获取此楼层下的所有分类
            $goods_class_list = db('goodsclass')->where('gc_parent_id=' . $cate_id)->select();
            //获取每个分类下的商品
            $goods_list = array();
            $goods_list[0]['gc_name'] = lang('hot_recommended');
            $goods_list[0]['gc_id'] = $cate_id;
            $goods_list[0]['gc_list'] = model('goods')->getGoodsOnlineList(array('gc_id' => $cate_id),'*', 0, 'goods_commend desc,goods_id desc', 10,'goods_commonid');

            $hot_goods_class_list = db('goodsclass')->where('gc_parent_id=' . $cate_id)->order('gc_sort desc')->limit(5)->select();
            foreach ($hot_goods_class_list as $key => $hot_goods_class) {
                $data = array();
                $data['gc_name'] = $hot_goods_class['gc_name'];
                $data['gc_id'] = $hot_goods_class['gc_id'];
                $data['gc_list'] = model('goods')->getGoodsOnlineList(array('gc_id' => $data['gc_id']),'*', 0, 'goods_commend desc,goods_id desc', 10,'goods_commonid');
                $goods_list[] = $data;
            }
            $result['goods_list'] = $goods_list;
            $result['goods_class_list'] = $goods_class_list;
            wcache($cate_id, $result,$prefix, 3600);
        }
        return $result;
    }

    //json输出商品分类
    public function josn_class() {
        /**
         * 实例化商品分类模型
         */
        $goodsclass_model = model('goodsclass');
        $goods_class = $goodsclass_model->getGoodsclassListByParentId(intval(input('get.gc_id')));
        $array = array();
        if (is_array($goods_class) and count($goods_class) > 0) {
            foreach ($goods_class as $val) {
                $array[$val['gc_id']] = array(
                    'gc_id' => $val['gc_id'],
                    'gc_name' => htmlspecialchars($val['gc_name']),
                    'gc_parent_id' => $val['gc_parent_id'],
                    'gc_sort' => $val['gc_sort']
                );
            }
        }
        echo $_GET['callback'] . '(' . json_encode($array) . ')';
    }

    /**
     * json输出地址数组 public/static/plugins/area_datas.js
     */
    public function json_area() {
        echo $_GET['callback'] . '(' . json_encode(model('area')->getAreaArrayForJson()) . ')';
        exit();
    }

    /**
     * json输出地址数组 
     */
    public function json_area_show() {
        $area_info['text'] = model('area')->getTopAreaName(intval($_GET['area_id']));
        echo $_GET['callback'] . '(' . json_encode($area_info) . ')';
    }

    //判断是否登录
    public function login() {
        echo (session('is_login') == '1') ? '1' : '0';
    }

    /**
     * 查询每月的周数组
     */
    public function getweekofmonth() {
        Loader::import('mall.datehelper');
        $year = input('get.y');
        $month = input('get.m');
        $week_arr = getMonthWeekArr($year, $month);
        echo json_encode($week_arr);
        die;
    }

    /**
     * 头部最近浏览的商品
     */
    public function viewed_info() {
        $info = array();
        if (session('is_login') == '1') {
            $member_id = session('member_id');
            $info['m_id'] = $member_id;
            if (config('voucher_allow') == 1) {
                $time_to = time(); //当前日期
                $info['voucher'] = db('voucher')->where(
                                array(
                                    'voucher_owner_id' => $member_id, 'voucher_state' => 1,
                                    'voucher_startdate' => array('elt', $time_to),
                                    'voucher_enddate' => array('egt', $time_to)
                        ))->count();
            }
            $time_to = strtotime(date('Y-m-d')); //当前日期
            $time_from = date('Y-m-d', ($time_to - 60 * 60 * 24 * 7)); //7天前
            $consult_mod=model('consult');
            $info['consult'] = $consult_mod->getConsultCount(array(
                        'member_id' => $member_id, 'consult_replytime' => array(
                            array(
                                'gt', strtotime($time_from)
                            ), array('lt', $time_to + 60 * 60 * 24), 'and'
                        )
                    ));
        }
        $goods_list = model('goodsbrowse')->getViewedGoodsList(session('member_id'), 5);
        if (is_array($goods_list) && !empty($goods_list)) {
            $viewed_goods = array();
            foreach ($goods_list as $key => $val) {
                $goods_id = $val['goods_id'];
                $val['url'] = url('Goods/index', ['goods_id' => $goods_id]);
                $val['goods_image'] = goods_thumb($val, 60);
                $viewed_goods[$goods_id] = $val;
            }
            $info['viewed_goods'] = $viewed_goods;
        }
        echo json_encode($info);
    }
    /**
     * 弹出广告
     */
    public function adv(){
        $condition['ap_ispopup'] = 1;
        $adv = \think\Db::name('advposition')->where($condition)->order('ap_id desc')->find();

        if($adv){
            $data = \think\Db::name('adv')->where(['ap_id' => $adv['ap_id'],'adv_device' => 'PC'])->order('adv_id desc')->find();
            if($data){
                $data['adv_code'] = adv_image($data['adv_code']);
                // $activity = Db::name('activity')->where(['activity_id' => $data['adv_link']])->find();
                if(time() >= $data['adv_startdate'] && time() <= $data['adv_enddate']){
                    echo json_encode($data);
                }else{
                    echo json_encode([]);
                }
            }else{
                echo json_encode([]);
            }

        }else{
            echo json_encode([]);
        }

    }


}
