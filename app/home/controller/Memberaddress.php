<?php

namespace app\home\controller;

use think\Lang;

class Memberaddress extends BaseMember {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/memberaddress.lang.php');
    }

    /*
     * 收货地址列表
     */

    public function index() {
        $address_model=model('address');
        $address_list = $address_model->getAddressList(array('member_id'=>session('member_id')));
        $this->assign('address_list', $address_list);

        /* 设置买家当前菜单 */
        $this->setMemberCurMenu('member_address');
        /* 设置买家当前栏目 */
        $this->setMemberCurItem('my_address');
        return $this->fetch($this->template_dir . 'index');
    }

    public function add() {
        if (!request()->isPost()) {
            $area_mod=model('area');
            $region_list = $area_mod->getAreaList(array('area_parent_id'=>'0'));
            $this->assign('region_list', $region_list);
            $address = array(
                'address_realname' => '',
                'area_id' => '',
                'city_id' => '',
                'address_detail' => '',
                'address_tel_phone' => '',
                'address_mob_phone' => '',
                'address_is_default' => '',
                'area_info' => '',
                'address_longitude' => '',
                'address_latitude' => '',
                'real_name' => '',
                'id_card_no' => '',
                'id_card_front' => '',
                'id_card_back' => '',
            );
            $this->assign('address', $address);
            /* 设置买家当前菜单 */
            $this->setMemberCurMenu('member_address');
            /* 设置买家当前栏目 */
            $this->setMemberCurItem('my_address_add');
            $this->assign('baidu_ak', config('baidu_ak'));

            $popup = $this->request->param('popup', 0, 'intval');
            
            return $this->fetch($this->template_dir .( $popup ? 'popup_form' : 'form' ));
        } else {
            $data = array(
                'member_id' => session('member_id'),
                'address_realname' => input('post.true_name'),
                'area_id' => input('post.area_id'),
                'city_id' => input('post.city_id'),
                'address_detail' => input('post.address'),
                'address_longitude' => input('post.longitude'),
                'address_latitude' => input('post.latitude'),
                'address_tel_phone' => input('post.tel_phone'),
                'address_mob_phone' => input('post.mob_phone'),
                'address_is_default' => input('post.is_default') == 1 ? 1 : 0,
                'area_info' => input('post.area_info', '', 'trim'),
                'real_name' => input('post.real_name', '', 'trim'),
                'id_card_no' => input('post.id_card_no', '', 'trim'),
                'id_card_front' => input('post.id_card_front', '', 'trim'),
                'id_card_back' => input('post.id_card_back', '', 'trim'),
            );
            if($data['real_name'] || $data['id_card_no'] || $data['id_card_front'] || $data['id_card_back']) {
                if(!$data['real_name'] || !$data['id_card_no'] || !$data['id_card_front'] || !$data['id_card_back']) {
                    sc_json_encode(10001, '请完善实名信息');
                }
            }
            //验证数据  BEGIN
            $memberaddress_validate = validate('memberaddress');
            if (!$memberaddress_validate->scene('add')->check($data)) {
                sc_json_encode(10001,$memberaddress_validate->getError());
            }
            //验证数据  END
            $address_model=model('address');
            $result = $address_model->addAddress($data);
            sc_json_encode(10000,lang('sc_common_save_succ'));
        }
    }

    public function edit() {

        $address_id = intval(input('param.address_id'));
        if (0 >= $address_id) {
            sc_json_encode(10001,lang('param_error'));
        }
        $address_model=model('address');
        $address = $address_model->getAddressInfo(array('member_id' => session('member_id'), 'address_id' => $address_id));
        if (empty($address)) {
            sc_json_encode(10001,lang('address_does_not_exist'));
        }
        if (!request()->isPost()) {
            $area_mod=model('area');
            $region_list = $area_mod->getAreaList(array('area_parent_id'=>'0'));
            $this->assign('region_list', $region_list);
            $this->assign('address', $address);
            /* 设置买家当前菜单 */
            $this->setMemberCurMenu('member_address');
            /* 设置买家当前栏目 */
            $this->setMemberCurItem('my_address_edit');
            $this->assign('baidu_ak', config('baidu_ak'));
            return $this->fetch($this->template_dir . 'form');
        } else {
            $data = array(
                'address_realname' => input('post.true_name'),
                'area_id' => input('post.area_id'),
                'city_id' => input('post.city_id'),
                'address_detail' => input('post.address'),
                'address_longitude' => input('post.longitude'),
                'address_latitude' => input('post.latitude'),
                'address_tel_phone' => input('post.tel_phone'),
                'address_mob_phone' => input('post.mob_phone'),
                'address_is_default' => input('post.is_default') == 1 ? 1 : 0,
                'area_info' => input('post.area_info'),
                'real_name' => input('post.real_name'),
                'id_card_no' => input('post.id_card_no'),
                'id_card_front' => input('post.id_card_front'),
                'id_card_back' => input('post.id_card_back')
            );
            if($data['real_name'] || $data['id_card_no'] || $data['id_card_front'] || $data['id_card_back']) {
                if(!$data['real_name'] || !$data['id_card_no'] || !$data['id_card_front'] || !$data['id_card_back']) {
                    sc_json_encode(10001, '请完善实名信息');
                }
            }

            //验证数据  BEGIN
            $memberaddress_validate = validate('memberaddress');
            if (!$memberaddress_validate->scene('edit')->check($data)) {
                sc_json_encode(10001,$memberaddress_validate->getError());
            }
            //验证数据  END

            $result = $address_model->editAddress($data,array('member_id' => session('member_id'), 'address_id' => $address_id));
            if ($result) {
                sc_json_encode(10000,lang('sc_common_save_succ'));
            } else {
                sc_json_encode(10001,lang('sc_common_save_fail'));
            }
        }
    }

    public function drop() {
        $address_id = intval(input('param.address_id'));
        if (0 >= $address_id) {
            sc_json_encode(10001,lang('empty_error'));
        }
        $address_model=model('address');
        $result = $address_model->delAddress(array('address_id'=>$address_id));
        if ($result) {
            sc_json_encode(10000,lang('sc_common_del_succ'));
        } else {
            sc_json_encode(10001,lang('sc_common_del_fail'));
        }
    }


    /**
     *    栏目菜单
     */
    function getMemberItemList() {
        $item_list = array(
            array(
                'name' => 'my_address',
                'text' => lang('my_address'),
                'url' => url('Memberaddress/index'),
            ),
            array(
                'name' => 'my_address_add',
                'text' => lang('new_address'),
                'url' => url('Memberaddress/add'),
            ),
        );
        if (request()->action() == 'edit') {
            $item_list[] = array(
                'name' => 'my_address_edit',
                'text' => lang('edit_address'),
                'url' => "javascript:void(0)",
            );
        }
        return $item_list;
    }

    /**
     * 上传证件
     *
     */
    private function upload_pic() {
        $id_card_pic = array();
        $id_card_pic[1] = 'id_card1';
        $id_card_pic[2] = 'id_card2';
        $pic_array = array();
        $dir = BASE_UPLOAD_PATH.DS.ATTACH_PATH . DS . 'address' . DS;
        $count = 1;
        foreach ($id_card_pic as $pic) {
            if (!empty($_FILES[$pic]['name'])) {
                $upload=request()->file($pic);
                $result = $upload->rule('uniqid')->validate(['ext' => ALLOW_IMG_EXT])->move($dir);
                if ($result) {
                    $pic_array[$count] = $result->getFilename();
                } else {
                    $pic_array[$count] = '';
                }
            }
            $count++;
        }
        return $pic_array;
    }

}

?>
