<?php

/**
 * 买家
 */

namespace app\home\controller;
use think\Lang;

class BaseMember extends BaseHome {

    protected $member_info = array();   // 会员信息

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/basemember.lang.php');
        /* 不需要登录就能访问的方法 */
        if (!in_array(request()->controller() ,array('cart')) && !in_array(request()->action(), array('ajax_load', 'add', 'del')) && !session('member_id')) {
            $ref_url = request_uri();
            $this->redirect(HOME_SITE_URL.'/Login/login.html?ref_url='.urlencode($ref_url));
        }
        //会员中心模板路径
        $this->template_dir = 'default/member/' . strtolower(request()->controller()) . '/';
        $this->member_info = $this->getMemberAndGradeInfo(true);
        $this->assign('member_info', $this->member_info);
    }

    /**
     *    当前选中的栏目
     */
    protected function setMemberCurItem($curitem = '') {
        $this->assign('member_item', $this->getMemberItemList());
        $this->assign('curitem', $curitem);
    }

    /**
     *    当前选中的子菜单
     */
    protected function setMemberCurMenu($cursubmenu = '') {
        $member_menu = $this->getMemberMenuList();
        $this->assign('member_menu', $member_menu);
        $curmenu = '';
        foreach ($member_menu as $key => $menu) {
            foreach ($menu['submenu'] as $subkey => $submenu) {
                if ($submenu['name'] == $cursubmenu) {
                    $curmenu = $menu['name'];
                    $nav = $submenu['text'];
                }
            }
        }
        
        // 面包屑
        $nav_link = array();
        $nav_link[] = array('title' => lang('sc_index'), 'link' => HOME_SITE_URL);
        if ($curmenu == '') {
            $nav_link[] = array('title' => lang('sc_user_center'));
        } else {
            $nav_link[] = array('title' =>  lang('sc_user_center'), 'link' => url('Member/index'));
            $nav_link[] = array('title' => $nav);
        }


        $this->assign('nav_link_list', $nav_link);


        //当前一级菜单
        $this->assign('curmenu', $curmenu);
        //当前二级菜单
        $this->assign('cursubmenu', $cursubmenu);
    }

    /*
     * 获取卖家栏目列表,针对控制器下的栏目
     */

    protected function getMemberItemList() {
        return array();
    }

    /*
     * 获取卖家菜单列表
     */

    private function getMemberMenuList() {
        $menu_list = array(
            'info' =>
            array(
                'name' => 'info',
                'text' => lang('sc_data_management'),
                'url' => url('Memberinformation/index'),
                'submenu' => array(
                    array('name' => 'member_information', 'text' => lang('sc_account_information'), 'url' => url('Memberinformation/index'),),
                    array('name' => 'member_security', 'text' =>lang('sc_account_security'), 'url' => url('Membersecurity/index'),),
                    array('name' => 'member_address', 'text' => lang('sc_member_path_address'), 'url' => url('Memberaddress/index'),),
                    array('name' => 'member_message', 'text' => lang('sc_my_news'), 'url' => url('Membermessage/message'),),
                    array('name' => 'member_goodsbrowse', 'text' => lang('sc_my_footprint'), 'url' => url('Membergoodsbrowse/listinfo'),),
                    array('name' => 'member_connect', 'text' => lang('sc_third_party_account_login'), 'url' => url('Memberconnect/weixinbind'),),
                )
            ),
            'trade' =>
            array(
                'name' => 'trade',
                'text' => lang('sc_order_manage'),
                'url' => url('Memberorder/index'),
                'submenu' => array(
                    array('name' => 'member_order', 'text' => lang('sc_real_order'), 'url' => url('Memberorder/index'),),
//                    array('name' => 'member_vr_order', 'text' =>lang('sc_virtual_orders'), 'url' => url('Membervrorder/index'),),
                    array('name' => 'member_favorites', 'text' => lang('sc_favorites'), 'url' => url('Memberfavorites/fglist'),),
                    array('name' => 'member_evaluate', 'text' => lang('sc_trading_evaluation'), 'url' => url('Memberevaluate/index'),),
                    array('name' => 'predeposit', 'text' => lang('sc_account_balance'), 'url' => url('Predeposit/index'),),
                    array('name' => 'member_points', 'text' => lang('sc_my_points'), 'url' => url('Memberpoints/index'),),
//                    array('name' => 'member_voucher', 'text' => lang('sc_member_path_myvoucher'), 'url' => url('Membervoucher/index'),),
                )
            ),
            'server' =>
            array(
                'name' => 'server',
                'text' => lang('sc_customer_service'),
                'url' => url('Memberrefund/index'),
                'submenu' => array(
                    array('name' => 'member_refund', 'text' => lang('sc_refund_and_return'), 'url' => url('Memberrefund/index'),),
                    array('name' => 'member_consult', 'text' => lang('sc_commodity_consulting'), 'url' => url('Memberconsult/index'),),
                    array('name' => 'member_inform', 'text' => lang('sc_violation_to_report'), 'url' => url('Memberinform/index'),),
                    array('name' => 'member_mallconsult', 'text' => lang('sc_platform_for_customer_service'), 'url' => url('Membermallconsult/index'),),
                    array('name' => 'member_order_complaint', 'text' => '投诉订单', 'url' => url('Memberordercomplaint/index'),),
                )
            )

        );
        return $menu_list;
    }

}

?>
