<?php

/**
 * 页面中的常用文字
 */
$lang['sc_select_all'] = '全选';
$lang['sc_ensure_del'] = '您确定要删除吗?';
$lang['sc_delete'] = "删除";
$lang['sc_message'] = '站内消息';
$lang['sc_handle'] = '操作';
$lang['sc_edit'] = '编辑';
$lang['sc_ok'] = '确定';
$lang['sc_cancel'] = '取消';
$lang['sc_hidden'] = '隐藏';
$lang['sc_search'] = '搜索';
$lang['sc_submit'] = '提交';
$lang['sc_please_choose'] = '-请选择-';
$lang['sc_yes'] = '是';
$lang['sc_no'] = '否';
$lang['sc_all'] = '全部';
$lang['sc_sort'] = '排序';
$lang['sc_view'] = '查看';
$lang['sc_detail'] = '详细';
$lang['sc_close'] = '关闭';
$lang['sc_groupbuy_flag'] = '抢';
$lang['sc_groupbuy'] = '抢购活动';
$lang['sc_mansong_flag'] = '满';
$lang['sc_mansong'] = '满即送';
$lang['sc_zhe'] = '折';
$lang['sc_yuan'] = '元';
$lang['sc_xianshi'] = '限时折扣';
$lang['sc_bundling_flag'] = '组';
$lang['sc_bundling'] = '优惠套装';
$lang['sc_or'] = '或';
$lang['sc_state'] = '状态';
$lang['sc_manage'] = '管理';
$lang['sc_other'] = '其他';
$lang['sc_you_in'] = '您于';
$lang['sc_gift'] = '赠品';
$lang['sc_goods_price'] = '原价';
$lang['sc_to'] = '至';
$lang['sc_collect'] = '收藏';

$lang['common_areas'] = '常用地区';


$lang['sc_zh_cn'] = '中文版';
$lang['sc_en_us'] = '英文版';

//header中的文字
$lang['sc_hello'] = '您好';
$lang['sc_guest'] = '游客';
$lang['sc_login'] = '登录';
$lang['sc_register'] = '注册';


//公共语言包
$lang['order_state_cancel'] = '<span style="color:#999">已取消</span>';
$lang['order_state_new'] = '<span style="color:#36C">待付款</span>';
$lang['order_state_pay'] = '<span style="color:#F30">待发货</span>';
$lang['order_state_send'] = '<span style="color:#F30">待收货</span>';
$lang['order_state_success'] = '<span style="color:#999">交易完成</span>';
$lang['order_state_eval'] = '<span style="color:#999">已评价</span>';

$lang['param_error'] = '参数错误';
$lang['sc_checkcode'] = '验证码';
$lang['wrong_null'] = '请填写验证码';
$lang['no_record'] = '暂无符合条件的数据记录';
$lang['no_login'] = '您还没有登录';
$lang['sc_please_choose'] = '请选择...';

$lang['currency'] = '&yen;';
$lang['currency_zh'] = '元';
$lang['ten_thousand'] = '万';
$lang['piece'] = '件';
$lang['individuals'] = '个';
$lang['sc_goods'] = "商品";
$lang['sc_colon'] = '：';
$lang['sc_comma'] = '，';
$lang['sc_percent'] = '%';
$lang['sc_brand'] = "品牌";
$lang['sc_pointprod'] = "积分中心";
$lang['home_member_privary'] = '保密';
$lang['classification'] = '分类';
$lang['song_typeface'] = '宋体';
$lang['sc_more'] = '更多';
$lang['sc_index'] = "首页";
$lang['unit_price_down'] = '件，单价直降';
$lang['homepage'] = '首页';
$lang['sc_day'] = '天';
$lang['sc_hour'] = '时';
$lang['sc_minute'] = '分';
$lang['sc_second'] = '秒';
/**
 * nav中的文字
 */
$lang['sc_all_goods_class'] = '所有分类';
$lang['sc_common_op_confirm'] = '您确定要删除该信息吗？';
$lang['sc_common_op_succ'] = '操作成功';
$lang['sc_common_op_fail'] = '操作失败';
$lang['sc_common_del_succ'] = '删除成功';
$lang['sc_common_del_fail'] = '删除失败';
$lang['sc_common_update_succ'] = '修改成功';
$lang['sc_common_update_fail'] = '修改失败';
$lang['sc_common_save_succ'] = '保存成功';
$lang['sc_common_save_fail'] = '保存失败';
$lang['sc_common_goods_null'] = '暂无商品';
$lang['sc_common_loading'] = '加载中...';
$lang['sc_common_shipping_free'] = '免运费';

$lang['member_map_address'] = '详细地址';
$lang['member_login'] = '会员登录';
$lang['points_unit'] = '积分';
$lang['credit_unit'] = '分';

$lang['sc_buying_goods'] = "已买到的商品";
$lang['sc_check_cart'] = "查看购物车";
$lang['sc_goods_num_one'] = "共";
$lang['sc_index_no_record'] = '没有找到符合条件的商品';
$lang['sc_buy_now'] = '立即购买';


//调试模式语言包
$lang['sc_notallowed_login'] = "您的账号已经被管理员禁用，请联系平台客服进行核实";
$lang['sc_new_message'] = "新消息";

//订单
$lang['order_log_eval'] = '评价了交易';

/**
 *  登录-公共语言	
 */
$lang['login_index_login_success'] = '登录成功';
$lang['login_index_login_again'] = '用户名或密码错误，请重新登录';
$lang['login_index_input_username'] = '用户名不能为空';
$lang['login_index_input_password'] = '密码不能为空';
$lang['login_index_input_checkcode'] = '验证码不能为空';
$lang['login_index_wrong_checkcode'] = '验证码错误';
$lang['login_index_change_checkcode'] = '看不清，换一张';
$lang['login_index_username'] = '用户名';
$lang['login_index_login_fail'] = '登录失败';
$lang['login_index_account_stop'] = '账号被停用';
$lang['login_notlogged'] = '未登录';
$lang['sc_mobile'] = '手机';
$lang['sc_email'] = '邮箱';

/**
 * 快速登录弹出框体
 */
$lang['quick_login_register'] = '免费注册';
$lang['quick_login_forget'] = '忘记密码';

//home
$lang['provinces'] = '省份';
$lang['welcome_to'] = '您好，欢迎来到';
$lang['exit'] = '退出';
$lang['please_log'] = '请登录';
$lang['wechat_end'] = '微信端';
$lang['click_here_send_message'] = '点击这里给我发消息';
$lang['platform_customer_service'] = '平台客服';

//dispatch_jump
$lang['page_automatic'] = '页面自动';
$lang['jump'] = '跳转';
$lang['waiting_time'] = '等待时间';

//mall_footer
$lang['about_us'] = '关于我们';
$lang['contact_us'] = '联系我们';
$lang['marketing_center'] = '营销中心';
$lang['mobile_mall'] = '手机商城';
$lang['link'] = '友情链接';
$lang['sales_alliance'] = '销售联盟';
$lang['mall_community'] = '商城社区';
$lang['mall_public_benefit'] = '商城公益';

//mall_header
$lang['search_merchandise_keywords'] = '请输入您要搜索的商品关键字';
$lang['sc_my_user_center'] = '我的用户中心';
$lang['sc_my_order'] = '我的订单';
$lang['sc_favorites'] = "我的收藏";
$lang['sc_consulting_reply'] = '咨询回复';
$lang['sc_vouchers'] = '代金券';
$lang['sc_my_points'] = '我的积分';
$lang['sc_recently_browsed_items'] = '最近浏览的商品';
$lang['sc_full_history'] = '全部浏览历史';
$lang['sc_shopping_cart_settlement'] = '购物车结算';
$lang['sc_latest_additions'] = '最新加入的商品';
$lang['sc_shopping_cart_empty'] = '您的购物车中暂无商品，赶快选择心爱的商品吧！';
$lang['sc_checkout_cart'] = '结算购物车中的商品';
$lang['sc_all_commodity_classes'] = '所有商品分类';

//mall-top
$lang['sc_attention'] = '关注';
$lang['sc_continuous_update'] = '持续更新';
$lang['sc_purchase_authorization'] = '购买授权';
$lang['sc_user_center'] = '用户中心';
$lang['sc_care_about'] = '我收藏的商品';
$lang['sc_merchandise_collection'] = '商品收藏';
$lang['sc_customer_center'] = '客户中心';
$lang['sc_help_center'] = '帮助中心';
$lang['sc_after_sales_service'] = '售后服务';
$lang['sc_current_level'] = '当前等级';
$lang['sc_current_experience'] = '当前经验值';
$lang['sc_mobile_shopping_better'] = '手机购物更优惠';
$lang['sc_comparison'] = '商品对比';
$lang['sc_top'] = '顶部';
$lang['sc_my_shopping_cart'] = '我的购物车';
$lang['sc_complaint_center'] = '客服中心';

//basemall basemember 重复语言包
$lang['sc_cart'] = "购物车";
$lang['sc_quantity'] = "数量";
$lang['sc_none'] = '无';
$lang['sc_agree'] = '同意';
$lang['sc_disagree'] = '不同意';
$lang['sc_planting_goods'] = '种商品';
$lang['sc_goods_name'] = '商品名称';
$lang['sc_unit_price'] = '单价';
$lang['sc_price'] = '价格';
$lang['goods_class_index_area'] = '不限地区';
$lang['refund_buyer_add_time'] = '申请时间';
$lang['order_time'] = '下单时间';
$lang['sc_payment_method'] = '支付方式';
$lang['home_default_sort'] = '默认排序';

//point
$lang['available_credit'] = '可用积分';
$lang['balance'] = '余额';

return $lang;
?>
