<?php
/**
 * header中的文字
 */
$lang['sc_myvoucher'] = '我的代金券';
/**
 * 左侧导航中的固定文字
 */
$lang['sc_data_management'] = '资料管理';
$lang['sc_order_manage'] = '交易管理';
$lang['sc_account_information'] = '账户信息';
$lang['sc_account_security'] = '账户安全';
$lang['sc_my_news'] = '我的消息';
$lang['sc_my_footprint'] = '我的足迹';
$lang['sc_third_party_account_login'] = '第三方账号登录';
$lang['sc_real_order'] = '实物订单';
$lang['sc_virtual_orders'] = '虚拟订单';
$lang['sc_trading_evaluation'] = '交易评价/晒单';
$lang['sc_account_balance'] = '账户余额';
$lang['sc_customer_service'] = '客户服务';
$lang['sc_refund_and_return'] = '退款及退货';
$lang['sc_commodity_consulting'] = '商品咨询';
$lang['sc_violation_to_report'] = '违规举报';
$lang['sc_platform_for_customer_service'] = '平台客服';
$lang['sc_distribution_information'] = '分销信息';
$lang['sc_distribution_member'] = '分销会员';
$lang['sc_distribution_commission'] = '分销佣金';
$lang['sc_become_member'] = '成为分销员';
$lang['sc_modify_profile'] = '修改头像';
$lang['sc_modify_data'] = '修改资料';
$lang['sc_exit_safely'] = '安全退出';


$lang['sc_member_path_points'] = '积分明细';
$lang['sc_member_path_pointorder_list'] = '兑换列表';
$lang['sc_member_path_pointorder_info'] = '兑换详细';
$lang['sc_member_path_myvoucher'] = '我的代金券';
$lang['sc_member_path_address'] = '收货地址';
$lang['sc_member_path_collect_list'] = '收藏商品';
$lang['sc_member_path_order_list'] = '订单列表';
$lang['sc_member_path_all_consult'] = '全部咨询';
$lang['sc_member_path_unreplied_consult'] = '未回复咨询';
$lang['sc_member_path_replied_consult'] = '已回复咨询';
$lang['sc_member_path_qq_bind'] = 'QQ绑定';
$lang['sc_member_path_sina_bind'] = '新浪微博绑定';
$lang['sc_member_path_buyer_refund'] = '退款申请';
$lang['sc_member_path_buyer_return'] = '退货申请';

$lang['sc_member_path_deliverno'] = '等待发货的订单';
$lang['sc_member_path_delivering'] = '发货中的订单';
$lang['sc_member_path_delivered'] = '已收到货的订单';
$lang['sc_member_path_deliver_info'] = '物流详情';

/**
 * 预存款菜单
 */
$lang['sc_member_path_predeposit_rechargeinfo'] = '充值详细';
$lang['sc_member_path_predeposit_cashadd'] = '提现申请';
$lang['sc_member_path_predeposit_cashinfo'] = '提现详细';

/**
 * 左侧菜单
 */
$lang['sc_pointsnum'] = '积分数';
$lang['sc_predepositnum'] = '预存款';
$lang['sc_order_waitpay'] = '待付款订单';
$lang['sc_order_receiving'] = '待确认收货';
$lang['sc_order_waitevaluate'] = '待评价交易';
?>
