<?php

namespace app\mobile\controller;

use think\Lang;

class Member extends MobileMember {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\member.lang.php');
    }
    
    
    public function index() {
        $member_info = array();

        $member_info['member_id'] = $this->member_info['member_id'];
        $member_info['member_type'] = $this->member_info['member_type'];
        switch ($member_info['member_type']){
            case 0:
                $member_info['member_type_desc'] = "普通用户";
                break;
            case 1:
                $member_info['member_type_desc'] = "一级";
                break;
            case 2:
                $member_info['member_type_desc'] = "二级";
                break;
        }
        $member_info['store_id'] = $this->member_info['store_id'];
        $member_info['member_name'] = $this->member_info['member_name'];
        $member_info['member_avatar'] = get_member_avatar_for_id($this->member_info['member_id']);
        $member_info['member_points'] = $this->member_info['member_points'];
        $member_info['level_name'] = dict_convert('inviter_type', $this->member_info['member_type']);
        $member_info['voucher_count'] = (new \app\common\model\Voucher())->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        $member_info['available_predeposit'] = $this->member_info['available_predeposit'];
        $member_info['freeze_predeposit'] = $this->member_info['freeze_predeposit'];
        $member_info['member_state'] = $this->member_info['member_state'];
        $member_info['member_group'] = $this->member_info['member_group'];

        switch ($member_info['member_group']){
            case 1:
                $member_info['member_group_desc'] = "批发商";
                break;
            case 9:
                $member_info['member_group_desc'] = "普通会员";
                break;
        }


        $member_info['favorites_goods'] = model('favorites')->getGoodsFavoritesCountByMemberId($this->member_info['member_id']);
        // 交易提醒
        $model_order = model('order');
        $member_info['order_nopay_count'] = $model_order->getOrderCountByID($this->member_info['member_id'], 'NewCount');
        $member_info['order_noship_count'] = $model_order->getOrderCountByID($this->member_info['member_id'], 'PayCount');
        $member_info['order_noreceipt_count'] = $model_order->getOrderCountByID($this->member_info['member_id'], 'SendCount');
        $member_info['order_noeval_count'] = $model_order->getOrderCountByID($this->member_info['member_id'], 'EvalCount');
//        $member_info['order_refund_count'] = $model_order->getOrderCountByID($this->member_info['member_id'], '');

        // 售前退款
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_state'] = array('lt', 3);
        $member_info['return'] = model('refundreturn')->getRefundReturnCount($condition);

        // 门店相关信息
        $store_info = \app\common\model\Inviter::get($this->member_info['member_id']);
        $store_info and $store_info['inviter_state_format'] = config('inviter_state.'.$store_info['inviter_state']) and $store_info['inviter_distribute_id'] = \app\common\model\Inviter::getDistributeId($store_info);
        output_data(array(
            'member_info' => $member_info,
            'store_info' => $store_info,
        ));
    }

    public function my_asset() {
        $fields_arr = array('point', 'predepoit', 'available_rc_balance', 'redpacket', 'voucher');
        $fields_str = trim(input('fields'));
        if ($fields_str) {
            $fields_arr = explode(',', $fields_str);
        }
        $member_info = array();
        if (in_array('point', $fields_arr)) {
            $member_info['point'] = $this->member_info['member_points'];
        }
        if (in_array('predepoit', $fields_arr)) {
            $member_info['predepoit'] = $this->member_info['available_predeposit'];
        }
        if (in_array('available_rc_balance', $fields_arr)) {
            $member_info['available_rc_balance'] = $this->member_info['available_rc_balance'];
        }
       /* if (in_array('redpacket', $fields_arr)) {
            $member_info['redpacket'] = model('redpacket')->getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        }*/
        if (in_array('voucher', $fields_arr)) {
            $member_info['voucher'] = model('voucher')->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        }
        output_data($member_info);
    }
}

?>
