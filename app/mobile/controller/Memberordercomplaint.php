<?php

namespace app\mobile\controller;

use think\Db;
use think\Lang;

class Memberordercomplaint extends MobileMember
{

    public function _initialize()
    {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\member.lang.php');
    }

    //用户订单投诉列表
    public function index()
    {
        $order_model = model('order');
        $member_id = $this->member_info['member_id'];
        $member_name = $this->member_info['member_name'];
        $condition = array();
        $condition['member_id'] = $member_id;
        $condition['member_name'] = $member_name;

        $complaint_list = model('complaint')->getComplaintList($condition);

        $order_id = 0;
        $map = [];
        $order_list = [];
        foreach ($complaint_list as $k => $item){
            $order_id = $item['order_id'];
            $order = Db::name('order a')->join('complaint b','a.order_id = b.order_id')->where([
                'a.order_id' => $order_id,
                'b.complaint_id' => $item['complaint_id']
            ])->find();
            $order_list[]['order_info'] = $order;
        }
        output_data($order_list);
    }

    //订单投诉列表
    public function complaint_list(){
        $order_id = intval(input('param.order_id'));
        $member_id = $this->member_info['member_id'];
        $member_name = $this->member_info['member_name'];
        $condition = array();
        $condition['member_id'] = $member_id;
        $condition['member_name'] = $member_name;
        $condition['order_id'] = $order_id;
        $complaint_list = model('complaint')->getComplaintList($condition);

        output_data($complaint_list);
    }
    //投诉详情
    public function complaint_info(){

        $complaint_id = input('post.complaint_id');
        $condition = array();
        $condition['complaint_id'] = $complaint_id;
        $complaint_info = model('complaint')->getComplaintInfo($condition);
        output_data($complaint_info);
    }

    //添加投诉
    public function complaint_add(){
        $member_id = $this->member_info['member_id'];
        $member_name = $this->member_info['member_name'];
        $order_id = input('post.order_id');
        $content = input('post.consult_content');
        $condition = array();
        $condition['member_id'] = $member_id;
        $condition['member_name'] = $member_name;
        $condition['order_id'] = $order_id;
        $condition['consult_content'] = $content;
        $condition['consult_addtime'] = TIMESTAMP;

        $complaint = model('complaint')->addComplaint($condition);
        output_data('1');
    }
}
?>
