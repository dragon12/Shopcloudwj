<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/27
 * Time: 14:40
 */
namespace app\mobile\controller;

use app\common\model\Ppintuan;
use think\lang;
use think\Model;
use think\Db;
use app\home\controller\Buy;
use app\common\model\Address;

class Pintuan extends MobileMall {

    public function _initialize(){
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\pintuan_list lang.php');
    }

    public function index()
    {
        // 当前页数(框架自动判断)
        $page = intval(input("get.page")) ? intval(input("get.page")) : 1;
        // 每页条数
        $page_nums = intval(input("get.page_nums")) ? intval(input("get.page_nums")) : 10;
        $goods_list = model('ppintuan')->getPintuanGoodDataList(["pintuan_state"=>1],$page_nums,'pintuan_end_time asc','p.*,g.goods_price');
        //$goods_list = Db::table('sc_ppintuan')->where('pintuan_state','1')->select();
        $goods_list = $goods_list->toArray();

        $goods_list_sort = $goods_list;
        unset($goods_list_sort["data"]);
        foreach ($goods_list["data"] as $k => $v){
            // 计算折扣之后的价格
            $v["last_price"] = bcmul(bcdiv($v["pintuan_zhe"],10,2),$v["goods_price"],2);
            $goods_list_sort["data"][] = $v;
        }

        output_data($goods_list_sort);
    }


    // 获取拼团商品信息
    public function pintuanGoodsInfo(){
        $collage_id = intval(input("post.collage_id"));
        $goods_id = intval(input("post.goods_id"));
        if(empty($collage_id)){
            output_data(["error"=>"缺少参数"]);
        }
        // 拼团商品信息
        $pt_goods_detail = model("ppintuan")->getPintuanGoodsInfo(["pintuan_id"=>$collage_id,"pintuan_state"=>1],"p.*,gc.goods_commonid,gc.goods_name,gc.gc_name,gc.goods_price,gc.goods_marketprice,gc.goods_image,gc.goods_freight,gc.goods_state");
        if(!$pt_goods_detail){
            output_data(["error"=>"活动不存在"]);
        }
        if($pt_goods_detail["goods_state"]!=1){
            output_data(["error"=>"该商品已下架"]);
        }
        // 图片处理
        $pt_goods_detail["pintuan_image"] = goods_cthumb($pt_goods_detail["pintuan_image"]);

        // 商品详细信息
        $pt_goods_detail_info = model("ppintuan")->getPintuanGoodsDeatilInfo(["gc.goods_commonid"=>$pt_goods_detail["goods_commonid"]],"g.goods_id,g.goods_name,g.goods_price,g.goods_marketprice,g.goods_image,g.goods_click,g.goods_salenum,g.goods_collect,g.goods_freight");
        if(empty($goods_id)) $goods_id = $pt_goods_detail_info[0]["goods_id"];
        $url = $_SERVER['SERVER_NAME']."/mobile/goods/goods_detail?goods_id=".$goods_id;
        $res_data = json_decode(http_request($url),true);

        $pt_goods_detail["goods_detail"] = $res_data["result"]["goods_detail"];
        $pt_goods_detail["goods_detail"]["goods_info"]["goods_attr"] = $res_data["result"]["goods_attr"];
        $pt_goods_detail["goods_image"] = goods_cthumb($pt_goods_detail["goods_image"],240);

        // 拼团记录
        $ppintuangroup_model = model('ppintuangroup');
        $ppintuanorder_model = model('ppintuanorder');
        $ppintuangroup_list = $ppintuangroup_model->getPpintuangroupList(["pintuan_id"=>$collage_id], 10); #获取开团信息
        foreach ($ppintuangroup_list as $key => $ppintuangroup) {
            //获取开团订单下的参团订单
            $condition = array();
            $condition["pintuan_id"] = $collage_id;
            $condition['pintuangroup_id'] = $ppintuangroup['pintuangroup_id'];
            $ppintuangroup_list[$key]['order_list'] = $ppintuanorder_model->getPpintuanorderList($condition);
        }

        $pt_goods_detail["record"] = $ppintuangroup_list;
        output_data($pt_goods_detail);
    }



    // 获取拼团商品信息
    public function pintuanGoodsDetailInfo(){
        $collage_id = intval(input("post.collage_id"));
        if(empty($collage_id)){
            output_data(["error"=>"缺少参数"]);
        }
        $pt_goods_detail = model("ppintuan")->getPintuanGoodsInfo(["pintuan_id"=>$collage_id,"pintuan_state"=>1],"gc.goods_commonid,gc.goods_name,gc.gc_name,gc.goods_price,gc.goods_marketprice,gc.goods_image,gc.goods_freight,goods_state");
        if(!$pt_goods_detail){
            output_data(["error"=>"活动不存在"]);
        }

        if($pt_goods_detail["goods_state"]!=1){
            output_data(["error"=>"该商品已下架"]);
        }

        $pt_goods_detail_info = model("ppintuan")->getPintuanGoodsDeatilInfo(["gc.goods_commonid"=>$pt_goods_detail["goods_commonid"]],"g.goods_id,g.goods_name,g.goods_price,g.goods_marketprice,g.goods_image,g.goods_click,g.goods_salenum,g.goods_collect,g.goods_freight");
        $pt_goods_detail_sort = [];
        foreach ($pt_goods_detail_info as $k => $v){
            $v["goods_image"] = goods_cthumb($v["goods_image"],240);
            $pt_goods_detail_sort[] = $v;
        }
        output_data($pt_goods_detail_sort);
    }


    // 拼团开团表
    public function pintuan_group(){
        $pintuan_id = input("get.pintuan_id");
        if(empty($pintuan_id)){
            output_data(["error"=>"信息不全"]);
        }
        // 当前页数(框架自动判断)
        $page = intval(input("get.page")) ? intval(input("get.page")) : 1;
        // 每页条数
        $page_nums = intval(input("get.page_nums")) ? intval(input("get.page_nums")) : 10;

        $list = model("Ppintuangroup")->getPpintuangroupList(["pintuan_id"=>$pintuan_id,"pintuangroup_state"=>1],$page_nums);
        $res = model("Ppintuangroup")->page_info->toArray();

        $res_list["total"] =  $res["total"];
        $res_list["per_page"] =  $res["per_page"];
        $res_list["current_page"] =  $res["current_page"];
        $res_list["last_page"] =  $res["last_page"];
        $res_list["data"] = $list;

        output_data($res_list);
    }


}
