<?php
namespace app\mobile\controller;
use app\common\model\Smslog;

class Memberaccount extends MobileMember
{
    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    /**
     * 初次绑定手机第一步
     */
    public function bind_mobile_step1()
    {
        if (!input('post.mobile') || !preg_match('/^\d{11}$/', input('post.mobile'))) {
            output_error('请正确输入手机号',['code'=>'']);
        }

        $model_member = model('member');
        $check_mobile = $model_member->getMemberInfo(array(
                                                         'member_mobile' => trim(input('post.mobile')),
                                                         'member_mobilebind' => 1
                                                     ));
        if (is_array($check_mobile) and count($check_mobile) > 0) {
            output_error('手机号码已经被绑定过',['code'=>'']);
        }

        try {
            $mobile = $this->request->param('mobile', '');
            $verify_code = rand(100, 999) . rand(100, 999);

            $smslog_model = new Smslog();
            $result = $smslog_model->sendSms($mobile, SMS_AUTH, $verify_code, $this->member_info['member_id'], $this->member_info['member_name']);

            if ($result['state']) {
                $update_data = array();
                $update_data['auth_code'] = $verify_code;
                $update_data['send_acode_time'] = TIMESTAMP;
                $update_data['send_acodetimes'] = array('exp', 'send_acodetimes+1');
                $update = $model_member->editMemberCommon($update_data, array('member_id' => $this->member_info['member_id']));
                if (!$update) {
                    output_error('系统发生错误1');
                }
                $updates = array();
                $updates['member_mobile'] = input('post.mobile');
                $model_member->editMember(array('member_id' => $this->member_info['member_id']),$updates);
                output_data(array('sms_time' => DEFAULT_CONNECT_SMS_TIME));
            }
            else {
                output_error($result['message']);
            }
        } catch (\Exception $e) {
            output_error($e->getMessage());
        }
    }

    /**
     * 初次绑定手机第二步 - 验证短信码
     */
    public function bind_mobile_step2()
    {
        if (!input('post.auth_code') || !preg_match('/^\d{6}$/', input('post.auth_code'))) {
            output_error('请正确输入短信验证码',['code'=>'-1']);
        }
        $model_member = model('member');
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id' => $this->member_info['member_id']));
        if (empty($member_common_info) || !is_array($member_common_info)) {
            output_error('验证失败',['code'=>'-1']);
        }
        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
            output_error('验证码已失效，请重新获取',['code'=>'-1']);
        }
        if ($member_common_info['auth_code_checktimes'] > 3) {
            output_error('输入错误次数过多，请重新获取',['code'=>'-1']);
        }
        if ($member_common_info['auth_code'] != input('post.auth_code')) {
            $data = array();
            $update_data['auth_code_checktimes'] = array('exp', 'auth_code_checktimes+1');
            $update = $model_member->editMemberCommon($update_data, array('member_id' => $this->member_info['member_id']));
            if (!$update) {
                output_error('系统发生错误',['code'=>'-1']);
            }
            output_error('验证失败',['code'=>'-1']);
        }

        $data = array();
        $data['auth_code'] = '';
        $data['send_acode_time'] = 0;
        $data['auth_code_checktimes'] = 0;
        $update = $model_member->editMemberCommon($data, array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误');
        }
        $updates = array();
        $updates['member_mobilebind'] =1;
        $update = $model_member->editMember(array('member_id' => $this->member_info['member_id']),$updates);
        if (!$update) {
            output_error('系统发生错误');
        }
        output_data('1');
    }

    /**
     * 检测会员手机是否绑定
     * 更改绑定手机 第一步 - 得到已经绑定的手机号
     * 修改密码 第一步 - 得到已经绑定的手机号
     * 修改支付密码 第一步 - 得到已经绑定的手机号
     */
    public function get_mobile_info()
    {
        $data = array();
        $data['state'] = $this->member_info['member_mobilebind'] ? true : false;
        $data['mobile'] = $data['state'] ? encrypt_show($this->member_info['member_mobile'], 4, 4) : $this->member_info['member_mobile'];
        output_data($data);
    }

    /**
     * 检测是否设置了支付密码
     */
    public function get_paypwd_info()
    {
        $data = array();
        $data['state'] = $this->member_info['member_paypwd'] ? true : false;
        output_data($data);
    }

    /**
     * 更改绑定手机 第二步 - 向已经绑定的手机发送验证码
     */
    public function modify_mobile_step2()
    {
        $this->_send_bind_mobile_msg();
    }

    /**
     * 更改密码 第二步 - 向已经绑定的手机发送验证码
     */
    public function modify_password_step2()
    {
        $this->_send_bind_mobile_msg();
    }

    /**
     * 更改支付密码第二步 - 向已经绑定的手机发送验证码
     */
    public function modify_paypwd_step2()
    {
        $this->_send_bind_mobile_msg();
    }

    private function _send_bind_mobile_msg()
    {
       /* if (!preg_match('/^\w{4}$/', $_POST['captcha']) || !captcha_check($_POST['captcha'])) {
            output_error('验证码错误',['code'=>'-1']);
        }*/

        if (!$this->member_info['member_mobilebind'] || !$this->member_info['member_mobile']) {
            output_error('您还未绑定手机号码',['code'=>'-1']);
        }

        $model_member = model('member');
        try {
            $verify_code = rand(100, 999) . rand(100, 999);
            $smslog_model = new Smslog();
            $result = $smslog_model->sendSms($this->member_info['member_mobile'], SMS_AUTH, $verify_code, $this->member_info['member_id'],  $this->member_info['member_name']);
            if($result['state']){
                $update_data = array();
                $update_data['auth_code'] = $verify_code;
                $update_data['send_acode_time'] = TIMESTAMP;
                $update_data['send_acodetimes'] = array('exp', 'send_acodetimes+1');
                $update = $model_member->editMemberCommon($update_data, array('member_id' => $this->member_info['member_id']));
                if (!$update) {
                    output_error('系统发生错误',['code'=>'']);
                }
                output_data(array('sms_time' => DEFAULT_CONNECT_SMS_TIME,'state'=>'1'));
            }else{
                output_error($result['message'],['code'=>'']);
            }

        } catch (\Exception $e) {
            output_error($e->getMessage(),['code'=>'']);
        }
    }

    /**
     * 更改绑定手机 第三步 - 验证短信码
     */
    public function modify_mobile_step3()
    {
        if (!input('post.auth_code') || !preg_match('/^\d{6}$/', input('post.auth_code'))) {
            output_error('请正确输入短信验证码',['code'=>'']);
        }
        $model_member = model('member');
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id' => $this->member_info['member_id']));
        if (empty($member_common_info) || !is_array($member_common_info)) {
            output_error('验证失败',['code'=>'']);
        }
        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
            output_error('验证码已失效，请重新获取',['code'=>'']);
        }
        if ($member_common_info['auth_code_checktimes'] > 3) {
            output_error('输入错误次数过多，请重新获取',['code'=>'']);
        }
        if ($member_common_info['auth_code'] != input('post.auth_code')) {
            $update_data = array();
            $update_data['auth_code_checktimes'] = array('exp', 'auth_code_checktimes+1');
            $update = $model_member->editMemberCommon($update_data, array('member_id' => $this->member_info['member_id']));
            if (!$update) {
                output_error('系统发生错误1',['code'=>'']);
            }
            output_error('验证失败',['code'=>'']);
        }

        $data = array();
        $data['auth_code'] = '';
        $data['send_acode_time'] = 0;
        $data['auth_code_checktimes'] = 0;
        $update = $model_member->editMemberCommon($data, array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误2',['code'=>'']);
        }
        $data = array();
        $data['member_mobile'] = '';
        $data['member_mobilebind'] = 0;
        $update = $model_member->editMember(array('member_id' => $this->member_info['member_id']), $data);
        if (!$update) {
            output_error('系统发生错误3',['code'=>'']);
        }
        output_data('1');
    }

    /**
     * 更改密码 第三步 - 验证短信码
     */
    public function modify_password_step3()
    {
        $this->_modify_pwd_check_vcode();
    }

    /**
     * 更改支付密码 第三步 - 验证短信码
     */
    public function modify_paypwd_step3()
    {
        $this->_modify_pwd_check_vcode();
    }

    public function _modify_pwd_check_vcode()
    {
        if (!input('post.auth_code') || !preg_match('/^\d{6}$/', input('post.auth_code'))) {
            output_error('请正确输入短信验证码',['code'=>'']);
        }
        $model_member = model('member');
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id' => $this->member_info['member_id']));
        if (empty($member_common_info) || !is_array($member_common_info)) {
            output_error('验证失败',['code'=>'']);
        }
        if (TIMESTAMP - $member_common_info['send_acode_time'] > 1800) {
            output_error('验证码已失效，请重新获取');
        }
        if ($member_common_info['auth_code_checktimes'] > 3) {
            output_error('输入错误次数过多，请重新获取');
        }
        if ($member_common_info['auth_code'] != input('post.auth_code')) {
            $data = array();
            $update_data['auth_code_checktimes'] = array('exp', 'auth_code_checktimes+1');
            $update = $model_member->editMemberCommon($update_data, array('member_id' => $this->member_info['member_id']));
            if (!$update) {
                output_error('系统发生错误a');
            }
            output_error('验证失败');
        }

        $data = array();
        $data['auth_code'] = '';
        $data['send_acode_time'] = 0;
        $data['auth_code_checktimes'] = 0;
        $update = $model_member->editMemberCommon($data, array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误b');
        }

        //更改密码授权
        $update = $model_member->editMemberCommon(array('auth_modify_pwdtime' => TIMESTAMP), array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误c',['code'=>'']);
        }

        output_data('1');
    }

    /**
     * 更改密码 第四步 - 检查是否有权修改密码
     */
    public function modify_password_step4()
    {
        $this->_modify_pwd_limit_check();
        output_data('1');
    }

    /**
     * 更改支付密码 第四步 - 检查是否有权修改密码
     */
    public function modify_paypwd_step4()
    {
        $this->_modify_pwd_limit_check();
        output_data('1');
    }

    private function _modify_pwd_limit_check()
    {
        //身份验证后，需要在30分钟内完成修改密码操作
        $model_member = model('member');
        $member_common_info = $model_member->getMemberCommonInfo(array('member_id' => $this->member_info['member_id']));
        if (empty($member_common_info) || !is_array($member_common_info)) {
            output_error('验证失败',['code'=>'']);
        }
        if ($member_common_info['auth_modify_pwdtime'] && TIMESTAMP - $member_common_info['auth_modify_pwdtime'] > 1800) {
            output_error('操作超时，请重新获取短信验证码',['code'=>'']);
        }
    }

    /**
     * 更改密码 第五步 - 保存新密码到数据库
     */
    public function modify_password_step5()
    {

        if (!input('post.password') || !input('post.password1') || input('post.password') != input('post.password1')) {
            output_error('提交数据错误',['code'=>'']);
        }

        //身份验证后，需要在30分钟内完成修改密码操作
        $this->_modify_pwd_limit_check();

        $model_member = model('member');

        $update = $model_member->editMember(array('member_id' => $this->member_info['member_id']), array('member_password' => md5(input('post.password'))));
        if (!$update) {
            output_error('密码修改失败',['code'=>'']);
        }
        output_data('1');
        $update = $model_member->editMemberCommon(array('auth_modify_pwdtime' => '0'), array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误',['code'=>'']);
        }
        output_data('1');
    }

    /**
     * 更改支付密码 第五步 - 保存新密码到数据库
     */
    public function modify_paypwd_step5()
    {

        if (!input('post.password') || !input('post.password1') || input('post.password') != input('post.password1')) {
            output_error('提交数据错误',['code'=>'']);
        }

        //身份验证后，需要在30分钟内完成修改密码操作
        $this->_modify_pwd_limit_check();

        $model_member = model('member');
        $update = $model_member->editMember(array('member_id' => $this->member_info['member_id']), array('member_paypwd' => md5(input('post.password'))));
        if (!$update) {
            output_error('密码修改失败',['code'=>'']);
        }

        $update = $model_member->editMemberCommon(array('auth_modify_pwdtime' => '0'), array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误',['code'=>'']);
        }
        output_data('1');
    }

    /**
     * 验证输入支付密码是否正确
     */
    public function check_paypwd()
    {
        if (!input('post.password')) {
            output_error('未输入支付密码',['code'=>'']);
        }
        /*if (!preg_match('/^\w{4}$/', $_POST['captcha']) || !captcha_check($_POST['codekey'], $_POST['captcha'])) {
            output_error('验证码错误',['code'=>'']);
        }*/
        if (md5(input('post.password')) != $this->member_info['member_paypwd']) {
            output_error('支付密码输入不正确',['code'=>'']);
        }

        $model_member = model('member');
        $data = array();
        $data['member_mobile'] = '';
        $data['member_mobilebind'] = 0;
        $update = $model_member->editMember(array('member_id' => $this->member_info['member_id']), $data);
        if (!$update) {
            output_error('系统发生错误',['code'=>'']);
        }
        //授权绑定新手机
        $update = $model_member->editMemberCommon(array('auth_modify_pwdtime' => TIMESTAMP), array('member_id' => $this->member_info['member_id']));
        if (!$update) {
            output_error('系统发生错误',['code'=>'']);
        }
        output_data('1');
    }
}