<?php

namespace app\mobile\controller;

use think\Lang;
use think\Validate;

class Selleraddress extends MobileSeller {

    public function _initialize() {
        parent::_initialize();
        $this->model_address = model('daddress');
    }

    /**
     * 地址列表
     */
    public function address_list() {
        $model_daddress = model('daddress');
        $condition = array();
        $condition['store_id'] = $this->store_info['store_id'];
        $address_list = $model_daddress->getAddressList($condition, '*', '', 20);

        output_data(array('address_list' => $address_list));
    }

    /**
     * 地址详细信息
     */
    public function address_info() {
        $address_id = intval(input('param.address_id'));
        if ($address_id <= 0) {
            output_error('地址参数错误！');
        }
        $condition = array();
        $condition['address_id'] = $address_id;
        $address_info = $this->model_address->getAddressInfo($condition);
        if (!empty($address_id) && $address_info['store_id'] == $this->store_info['store_id']) {
            output_data(array('address_info' => $address_info));
        } else {
            output_error('地址不存在');
        }
    }

    /**
     * 删除地址
     */
    public function address_del() {

        $address_id = intval(input('param.address_id'));
        if ($address_id <= 0) {
            output_error('删除地址失败！');
        }
        $condition = array();
        $condition['address_id'] = $address_id;
        $condition['store_id'] = $this->store_info['store_id'];
        $delete = model('daddress')->delAddress($condition);
        if ($delete) {
            output_data('删除地址成功！');
        } else {
            output_error('删除地址失败！');
        }
    }

    /**
     * 新增地址
     */
    public function address_add() {
        $model_daddress = model('daddress');

        //保存 新增/编辑 表单
        $obj_validate = new Validate();
        $data = array(
            'store_id' => $this->store_info['store_id'],
            'seller_name' => input('post.seller_name'),
            'area_id' => input('post.area_id'),
            'city_id' => input('post.city_id'),
            'area_info' => input('post.area_info'),
            'address' => input('post.address'),
            'telphone' => input('post.telphone'),
            'company' => '',
            'is_default' => intval(input('post.is_default'))
        );
        
        $rule = [
            ['seller_name', 'require', '收件人不能为空'],
            ['area_id', 'require|number', '请选择地址|请选择地址'],
            ['city_id', 'require|number', '请选择地址|请选择地址'],
            ['area_info', 'require', '请选择地址'],
            ['address', 'require', '详细地址不能为空'],
            ['telphone', 'require', '电话不能为空'],
        ];
        $error = $obj_validate->check($data, $rule);
         if (!$error) {
            output_error($obj_validate->getError());
        }




        $address_id = intval(input('post.address_id'));
        if ($address_id > 0) {
            $condition = array();
            $condition['address_id'] = $address_id;
            $condition['store_id'] = $this->store_info['store_id'];
            $update = $model_daddress->editAddress($data, $condition);
            if (!$update) {
                output_error('修改地址失败！');
            }
            $is_default = intval(input('post.is_default'));
            if ($is_default == 1) {
                $condition = array();
                $condition['address_id'] = array('neq', $address_id);
                $condition['store_id'] = $this->store_info['store_id'];
                $update = $model_daddress->editAddress(array('is_default' => 0), $condition);
            }
        } else {
            $insert = $model_daddress->addAddress($data);
            if (!$insert) {
                output_error('增加地址失败！');
            }
            $is_default = intval(input('post.is_default'));
            if ($is_default == 1) {
                $condition = array();
                $condition['address_id'] = array('neq', $insert);
                $condition['store_id'] = $this->store_info['store_id'];
                $update = $model_daddress->editAddress(array('is_default' => 0), $condition);
            }
        }
        output_data('操作成功！');
    }
}

?>
