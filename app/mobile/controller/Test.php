<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/27
 * Time: 14:40
 */
namespace app\mobile\controller;

use app\common\logic\Order;
use app\common\model\Ppintuan;
use app\common\model\Ppintuangroup;
use app\common\model\Ppintuanorder;
use think\Db;
use think\Lang;

class Test extends MobileMall {

    // 测试1 全部拼团商品
    public function test1() {
       /* $model = new Ppintuan();
        $list = $model->order('pintuan_state asc')->select();*/

       $list = Ppintuangroup::field('*')->select();
        dump($list);
    }
    //测试二  某个拼团商品详情
    public function test2(){
        $page = $this->request->param('page', 1, 'intval');
        $perpage = $this->request->param('perpage', 20, 'intval');
        $offset = ($page - 1) * $perpage;
        $list = \app\common\model\Goods::where('goods_id','128')->page($page, $perpage)->limit($offset, $perpage)->select();
        var_dump($list);

    }
    //测试三 商品开团
    public function test3(){

        $list = Ppintuangroup::create([
            'pintuan_id'=>'',
            'pintuangroup_goods_id'=>'',
            'pintuangroup_limit_number'=>'2',
            'pintuangroup_limit_hour'=>'2',
            'pintuangroup_joined'=>'',
            'pintuangroup_headid'=>'',
            'pintuangroup_starttime'=>'',
            'pintuangroup_endtime'=>'',
            'pintuangroup_state'=>'1',
        ]);
    }
    //测试四 拼团订单
    public function test4(){
        $list = Ppintuanorder::create([
            'pintuan_id'=>'',
            'pintuangroup_id'=>'',
            'order_id'=>'',
            'order_sn'=>'',
            'pintuanorder_isfirst'=>'',
            'pintuanorder_state'=>'',
        ]);

        //订单
        $list2 = Order::create([
            'order_sn'=>'',
            'pay_sn'=>'',
            'store_id'=>'',
        ]);
        //支付

        //订单列表
        $list3 = Order::where('buyer_name','shop')->select();
        var_dump($list3);
    }
    //成功
    public function test5(){
        $list = Ppintuangroup::where('pintuangroup_id','7')->update([
          // 'pintuangroup_joined'=>2,
            'pintuangroup_state'=>2,
        ]);

        $list = Ppintuanorder::update([
            'pintuanorder_state'=>2,
        ]);

    }
    //超时
    public function test6(){
        $list = Ppintuangroup::where([
            ['exp', 'pintuangroup_starttime < '.time() .' - pintuangroup_limit_hour * 3600'],
            ['exp', 'pintuangroup_limit_number > pintuangroup_joined'],
            'pintuangroup_state' => 1,
        ])->select();
        var_dump($list);

        $list1 = Ppintuangroup::where('pintuangroup_id','7')->update([
            //'pintuangroup_joined'=>1,
            'pintuangroup_state'=>0,
        ]);

        $list2 = Ppintuanorder::update([
            'pintuanorder_state' => 0,
        ]);

        $list3 = Order::update([
            'order_state' => 0,
            'refund_state'=>2,//退款
        ]);

        //删除订单
        $list4 = Order::destroy(function($query){
            $query->where('order_sn',10);
        });
    }
    //拼团活动到期
    public function test7(){
        $list = Ppintuan::where([
           ['exp', 'pintuan_end_time <'.time()],
            'pintuan_state'=>1,
        ])->select();
        var_dump($list);

        $list2 = Ppintuan::update([
            'pintuan_state'=>0,
        ])->where([
            ['exp', 'pintuan_end_time <'.time()],
            'pintuan_state'=>1,
        ]);
    }
}
