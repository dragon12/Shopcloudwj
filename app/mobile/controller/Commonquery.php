<?php

namespace app\mobile\controller;

use app\common\model\Commission;
use app\common\model\Message;
use app\common\model\Search;
use app\common\model\TimeGoods;
use app\common\model\Vipservice;
use support\Request;
use think\Db;
use think\Lang;

class Commonquery extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\member.lang.php');
    }

    /**
     * 物流跟踪
     */
    public function search_deliver() {
        $code = $this->request->param('code', '', 'trim');

        $order_sn = $code ? sc_decrypt($code) : '';

        $model_order = model('order');
        $map = [];
        $map['order_sn'] = $order_sn;
        $order_info = $model_order->getOrderInfo($map, array('order_common', 'order_goods'));

        if (empty($order_info) || !in_array($order_info['order_state'], array(ORDER_STATE_PAY, ORDER_STATE_SEND, ORDER_STATE_SUCCESS))) {
            output_error('无效单号');
        }

        $express = rkcache('express', true);
        $e_code = $order_info['extend_order_common']['shipping_express_id'] ? $express[$order_info['extend_order_common']['shipping_express_id']]['express_code'] : '';
        $e_name = $order_info['extend_order_common']['shipping_express_id'] ? $express[$order_info['extend_order_common']['shipping_express_id']]['express_name'] : '';

        $deliver_info = Request::expressQuery($order_info['shipping_code'], $e_code, $order_info);
        output_data(array('express_code' => $e_code, 'express_name' => $e_name, 'shipping_code' => $order_info['shipping_code'], 'deliver_info' => $deliver_info));
    }

    /**
     * 订单详情
     */
    public function order_info() {
        $code = $this->request->param('code', '', 'trim');
        $order_sn = $code ? sc_decrypt($code) : '';

        $model_order = model('order');
        $map = array();
        $order_sn and $map['order_sn'] = $order_sn;

        $order_info = $model_order->getOrderInfo($map, array('order_goods', 'order_common','refundreturn'));
        $order_id = $order_info['order_id'];

        if (empty($order_info)) {
            output_error('订单不存在');
        }
        $order_info['trade_mode_format'] = config('trade_mode.'.$order_info['trade_mode']);

        //支付链接
        $order_info['pay_link'] = WAP_SITE_URL .'/member/order_detail.html?order_id='.$order_info['order_id'].'&auto=true';


        $order_info['member_id'] = $this->member_info['member_id'];

        $model_refund_return = model('refundreturn');
        $order_list = array();
        $order_list[$order_id] = $order_info;
        $order_list = $model_refund_return->getGoodsRefundList($order_list, 1); //订单商品的退款退货显示
        $order_info = $order_list[$order_id];

        $refund_all = isset($order_info['refund_list'][0])?$order_info['refund_list'][0]:'';
//        if (!empty($refund_all) && $refund_all['examine_type'] < 3) {//订单全部退款商家审核状态:1为待审核,2为同意,3为不同意
//            var_dump($refund_all);
//            output_error($refund_all);
//        }


//        $order_info['store_member_id'] = $order_info['extend_store']['member_id'];
        $order_info['voucher_price'] = $order_info['extend_order_common']['voucher_price'];


        if ($order_info['payment_time']) {
            $order_info['payment_time'] = date('Y-m-d H:i:s', $order_info['payment_time']);
        } else {
            $order_info['payment_time'] = '';
        }
        if ($order_info['finnshed_time']) {
            $order_info['finnshed_time'] = date('Y-m-d H:i:s', $order_info['finnshed_time']);
        } else {
            $order_info['finnshed_time'] = '';
        }
        if ($order_info['add_time']) {
            $order_info['order_add_time'] = $order_info['add_time'];
            $order_info['add_time'] = date('Y-m-d H:i:s', $order_info['add_time']);
        } else {
            $order_info['add_time'] = '';
        }

        if ($order_info['extend_order_common']['order_message']) {
            $order_info['order_message'] = $order_info['extend_order_common']['order_message'];
        }
        if(!empty($order_info['extend_order_common']['invoice_info'])) {
            $order_info['invoice'] = $order_info['extend_order_common']['invoice_info']['类型'] . $order_info['extend_order_common']['invoice_info']['抬头'] . $order_info['extend_order_common']['invoice_info']['内容'];
        }
        $order_info['reciver_phone'] = $order_info['extend_order_common']['reciver_info']['phone'];
        $order_info['reciver_name'] = $order_info['extend_order_common']['reciver_name'];
        $order_info['reciver_addr'] = $order_info['extend_order_common']['reciver_info']['address'];
        //$order_info['order_message'] = $order_info['extend_order_common']['reciver_info']['order_message'];

        $order_info['promotion'] = array();
        // 是否是购买 / 卖
        $order_info['if_buy'] = $order_info['member_id'] == $this->member_info['member_id'];

        //显示锁定中
        $order_info['if_lock'] = $model_order->getOrderOperateState('lock', $order_info);

        //显示取消订单
        $order_info['if_buyer_cancel'] = $model_order->getOrderOperateState('buyer_cancel', $order_info);

        //显示退款取消订单
        $order_info['if_refund_cancel'] = $model_order->getOrderOperateState('refund_cancel', $order_info);

        //显示投诉
        $order_info['if_complain'] = $model_order->getOrderOperateState('complain', $order_info);

        //显示收货
        $order_info['if_receive'] = $model_order->getOrderOperateState('receive', $order_info);

        //显示物流跟踪
        $order_info['if_deliver'] = $model_order->getOrderOperateState('deliver', $order_info);




        //显示评价
        //$order_info['if_evaluation'] = $model_order->getOrderOperateState('evaluation', $order_info);

        //显示分享
        $order_info['if_share'] = $model_order->getOrderOperateState('share', $order_info);

        $order_info['ownshop'] = $model_order->getOrderOperateState('share', $order_info);

        //显示系统自动取消订单日期
        if ($order_info['order_state'] == ORDER_STATE_NEW) {
            $order_info['order_cancel_day'] = $order_info['order_add_time'] + config('order_auto_cancel_hour') * 3600;
            $order_info['order_cancel_day_format'] = time_desc($order_info['order_cancel_day'] - time());
            $order_info['time'] = time();
        }
        $order_info['if_deliver'] = false;
        //显示快递信息
//        if ($order_info['shipping_code'] != '') {
//            $order_info['if_deliver'] = true;
//            $express = rkcache('express', true);
//            $order_info['express_info']['e_code'] = $express[$order_info['extend_order_common']['shipping_express_id']]['express_code'];
//            $order_info['express_info']['e_name'] = $express[$order_info['extend_order_common']['shipping_express_id']]['express_name'];
//            $order_info['express_info']['e_url'] = $express[$order_info['extend_order_common']['shipping_express_id']]['express_url'];
//        }

        //显示系统自动收获时间
//        if ($order_info['order_state'] == ORDER_STATE_SEND) {
//            $order_info['order_confirm_day'] = $order_info['delay_time'] + ORDER_AUTO_RECEIVE_DAY * 24 * 3600;
//        }

        //如果订单已取消，取得取消原因、时间，操作人
        if ($order_info['order_state'] == ORDER_STATE_CANCEL) {
            $close_info = $model_order->getOrderLogInfo(array('order_id' => $order_info['order_id']), 'log_id desc');
            $order_info['close_info'] = $close_info;
            $order_info['order_tips'] = $close_info['log_msg'];
        }
        foreach ($order_info['extend_order_goods'] as $value) {
            $value['image_60_url'] = goods_cthumb($value['goods_image'], 60);
            $value['image_url'] = goods_cthumb($value['goods_image'], 240);
            $value['goods_type_cn'] = get_order_goodstype($value['goods_type']);
            $value['goods_url'] = url('goods/index', array('goods_id' => $value['goods_id']));
            if ($value['goods_type'] == 5) {
                $order_info['zengpin_list'][] = $value;
            } else {
                $order_info['goods_list'][] = $value;
            }
        }

        if (empty($order_info['zengpin_list'])) {
            $order_info['goods_count'] = count($order_info['goods_list']);
        } else {
            $order_info['goods_count'] = count($order_info['goods_list']) + 1;
        }

        $order_info['real_pay_amount'] = $order_info['order_amount'];
        //取得其它订单类型的信息000--------------------------------
        //$model_order->getOrderExtendInfo($order_info);


        $order_info['zengpin_list'] = array();
        if (is_array($order_info['extend_order_goods'])) {
            foreach ($order_info['extend_order_goods'] as $val) {
                if ($val['goods_type'] == 5) {
                    $order_info['zengpin_list'][] = $val;
                }
            }
        }
        $order_info['qrcode'] = url('share/order', ['code' => sc_encrypt($order_info['order_sn'])], 'png', true);
        output_data(array('order_info' => $order_info));

        //卖家发货信息
        if (!empty($order_info['extend_order_common']['daddress_id'])) {
            $daddress_info = model('daddress')->getAddressInfo(array('address_id' => $order_info['extend_order_common']['daddress_id']));
            $this->assign('daddress_info', $daddress_info);
        }

        $order_id = intval($_GET['order_id']);
        if ($order_id <= 0) {
            output_error('订单不存在100');
        }

        $model_order = model('order');
        $condition['order_id'] = $order_id;
//        $condition['buyer_id|inviter_id'] = $this->member_info['member_id'];
        $member_id = $this->member_info['member_id'];
        $member_condition = function ($query) use($member_id) {
            $query->whereOr([
                'buyer_id' => $member_id,
                function ($query) use($member_id) {
                    $query->where([
                        'inviter_id' => $member_id,
                        'order_buy_type' => ['eq', 2],
                    ]);
                }
            ]);
        };
        $condition[] = $member_condition;
        $order_info = $model_order->getOrderInfo($condition, array('order_goods'), 'order_id', 'order_sn', 'store_id', 'store_name', 'add_time', 'payment_time', 'shipping_time', 'finnshed_time', 'order_amount', 'shipping_fee', 'real_pay_amount', 'state_desc', 'payment_name', 'order_message', 'reciver_phone', 'reciver_name', 'reciver_addr', 'order_tips','is_expired');

        $order_info['promotion'] = array();
        $order_info['if_deliver'] = false;
        $order_info['if_buyer_cancel'] = false;
        $order_info['if_refund_cancel'] = false;
        $order_info['if_receive'] = false;
        $order_info['if_evaluation'] = false;
        $order_info['if_lock'] = false;

        $order_info['goods_list'] = array();
        $order_info['zengpin_list'] = array();
        $order_info['ownshop'] = false;
        output_data(array('order_info' => $order_info));
    }
}
