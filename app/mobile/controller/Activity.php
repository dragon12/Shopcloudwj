<?php

namespace app\mobile\controller;

use think\Lang;

class Activity extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/activity.lang.php');
    }
    
    /*
     * 显示所有活动列表
     */
    function index()
    {

        // 当前页数(框架自动判断)
        $page = intval(input("get.page")) ? intval(input("get.page")) : 1;
        // 每页条数
        $page_nums = intval(input("get.page_nums")) ? intval(input("get.page_nums")) : 10;

        $condition = array();
        $activity_model = model('activity');
        $condition['activity_type'] = 1;
        $condition['activity_startdate'] = array('elt',TIMESTAMP);
        $condition['activity_enddate'] = array('egt',TIMESTAMP);
        $condition['activity_state'] = 1;

        $activity_model->getActivityList($condition, $page_nums);
        $activity_list = $activity_model->page_info;
        output_data($activity_list);
    }
    

    /**
     * 单个活动信息页
     */
    public function detail() {
        //得到导航ID
        $nav_id = intval(input('param.nav_id'));
        //查询活动信息
        $activity_id = intval(input('param.activity_id'));
        if ($activity_id <= 0) {
            $this->error(lang('param_error')); //'缺少参数:活动编号'
        }
        $activity = model('activity')->getOneActivityById($activity_id);
        if (empty($activity) || $activity['activity_type'] != '1' || $activity['activity_state'] != 1 || $activity['activity_startdate'] > time() || $activity['activity_enddate'] < time()) {
            $this->error(lang('activity_index_activity_not_exists')); //'指定活动并不存在'
        }
        $activity['image'] = UPLOAD_SITE_URL.'/'.ATTACH_ACTIVITY.'/'.$activity['activity_banner'];
        //查询活动内容信息
        $condition = array();
        $condition['activitydetail.activity_id'] = $activity_id;
        $activitydetail_list = model('activitydetail')->getActivitydetailAndGoodsList($condition);
        foreach ($activitydetail_list as $k => $val){
            $val['goods_image_url'] =  goods_cthumb($val['goods_image'], 240);
            $activitydetail_list[$k] = $val;
        }
       output_data(array('activity' => $activity,'activitydetail_list' => $activitydetail_list));
    }

}

?>
