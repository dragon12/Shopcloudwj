<?php
namespace app\mobile\controller;
class Sellerexpress extends MobileSeller {

    /**
     * 物流列表
     */
    public function get_list() {
        $express_list = rkcache('express', true);
        //快递公司
        $express_select = dsGetValueByName('storeextend', 'store_id', session('store_id'), 'express');
        if (!is_null($express_select)) {
                $express_select = explode(',', $express_select);
            }
            else {
                $express_select = array();
            }

        output_data(array('express_array' => $express_list,'express_select'=>$express_select));
    }

    public function get_defaultexpress() {
        $order_id = intval(input('post.order_id'));
        if ($order_id <= 0) {
            output_error('参数错误');
        }
        $model_order = model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['store_id'] = $this->store_info['store_id'];

        $order_info = $model_order->getOrderInfo($condition, array('order_common', 'order_goods'));
        $if_allow_send = intval($order_info['lock_state']) || !in_array($order_info['order_state'], array(ORDER_STATE_PAY, ORDER_STATE_SEND));

        if ($if_allow_send) {
            output_error('参数错误');
        }

        //取发货地址
        $model_daddress = model('daddress');
        if ($order_info['extend_order_common']['daddress_id'] > 0) {
            $daddress_info = $model_daddress->getAddressInfo(array('address_id' => $order_info['extend_order_common']['daddress_id']));
        } else {
            //取默认地址
            $daddress_info = $model_daddress->getAddressList(array('store_id' => $this->store_info['store_id']), '*', 'is_default desc', 1);
            $daddress_info = $daddress_info[0];
        }
        output_data(array('daddress_info' => $daddress_info, 'orderinfo' => $order_info));
    }

    public function get_mylist() {
        $order_id = intval(input('post.order_id'));
        if ($order_id <= 0) {
            output_error('参数错误');
        }
        
        //订单信息
        $model_order = model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['store_id'] = $this->store_info['store_id'];

        $order_info = $model_order->getOrderInfo($condition, array('order_common', 'order_goods', 'member'));
        if (empty($order_info)) {
            output_error('订单信息不存在');
        }

        foreach ($order_info['extend_order_goods'] as $value) {
            $value['image_60_url'] = goods_cthumb($value['goods_image'], 60, $value['store_id']);
            $value['image_240_url'] = goods_cthumb($value['goods_image'], 240, $value['store_id']);
            $value['goods_type_cn'] = orderGoodsType($value['goods_type']);

            $order_info['zengpin_list'] = array();
            if ($value['goods_type'] == 5) {
                $order_info['zengpin_list'][] = $value;
            } else {
                $order_info['goods_list'][] = $value;
            }
        }
        
        
        
        $express_list = rkcache('express', true);
        //如果是自提订单，只保留自提快递公司
        if (!empty($order_info['extend_order_common']['reciver_info']['dlyp'])) {
            foreach ($express_list as $k => $v) {
                if ($v['e_zt_state'] == '0')
                    unset($express_list[$k]);
            }
            $my_express_list = array_keys($express_list);
        } else {
            //快递公司
            $my_express_list = dsGetValueByName('storeextend', 'store_id', $this->store_info['store_id'], 'express');
            if (!empty($my_express_list)) {
                $my_express_list = explode(',', $my_express_list);
                foreach ($my_express_list as $val) {
                    $express_array[$val] = $express_list[$val];
                }
            }
        }
        
        output_data(array('orderinfo' => $order_info, 'express_array' => $express_array));
    }

    /**
     * 自提物流列表
     */
    public function get_zt_list() {
        $express_list = rkcache('express', true);
        foreach ($express_list as $k => $v) {
            if ($v['e_zt_state'] == '0')
                unset($express_list[$k]);
        }
        output_data(array('express_array' => $express_list));
    }

    /**
     * 物流保存
     */
    public function savedefault() {
        $storeextend_model = model('storeextend');
        $data['store_id'] = $this->store_info['store_id'];
        $data['express'] = input('post.expresslists');

        if (!$storeextend_model->getby_store_id($this->store_info['store_id'])) {
            $result = $storeextend_model->insert($data);
        } else {
            $result = $storeextend_model->where(array('store_id' => $this->store_info['store_id']))->update($data);
        }
        if ($result) {
            output_data('succ');
        } else {
            output_error('error');
        }
    }

}