<?php

namespace app\mobile\controller;

use think\Lang;

class Redpacket extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\redpacket.lang.php');
    }

    /**
     * 红包详细页
     */
    public function index() {
        $id = intval($_GET ['id']);

        // 红包详细信息
        $packet_detail = db('redpacket')->where(array('id' => $id))->find();
        if (empty($packet_detail)) {
            output_error('红包已被抢光或不存在');
        }

        //活动规则
        //$rule = explode("\n", $packet_detail['packet_descript']);
        //最近10名中奖者
        //$packet_rec = db('redpacketrec')->where(array('packet_id'=>$id))->order('id DESC')->limit(10)->select();

        output_data(array('packet_detail' => $packet_detail, 'rec' => $packet_rec, 'rule' => $rule));
    }

}

?>
