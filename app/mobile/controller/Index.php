<?php

namespace app\mobile\controller;

use app\common\model\Adv;
use app\common\model\Goodsclassnav;
use think\Db;
use think\Lang;
use app\mobile\controller\Groupbuy;

class Index extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\index.lang.php');
    }

    /**
     * 首页
     */

    public function app() {
        $condition['a.ap_id'] = array('in', array('2', '15', '14', '13'));
        $condition['a.adv_enabled'] = 1;
        $data = model('adv')->mbadvlist($condition, 'a.adv_sort asc');
        $adname = array('15' => 'nav_list', '14' => 'adv_list', '13' => 'home1', '2' => 'adv_list2'); //预留一部分扩充adv_list/home1/home2/home3/home4
        $datas = [];
        foreach ($data as $key => $val) {
            $val['adv_code'] = adv_image($val['adv_code']) . '?_'.date('YmdHis');
            $val['adv_links'] = explode(',', $val['adv_link']);
            $datas[$adname[$val['ap_id']]][] = $val;
        }

        // 时间轴
        $yesterday_time_group = dict_get('time_group_period', date('Y-m-d', strtotime('-1day')));
        $today_time_group = dict_get('time_group_period', date('Y-m-d'));
        $tomorrow_time_group = dict_get('time_group_period', date('Y-m-d', strtotime('+1day')));

        $datas['time_group_list'] = [
            'today' => $today_time_group,
            'tomorrow' => $tomorrow_time_group,
            'yesterday' => $yesterday_time_group
        ];

        //限时折扣
        $recommend_list = model('goods')->getGoodsOnlineList(array(), 'goods_id,goods_name,goods_image,goods_promotion_price, trade_mode,goods_price', 0, '',10,'');
        foreach ($recommend_list as $k => $val) {
            $recommend_list[$k]['goods_image'] = goods_thumb($val);
            $recommend_list[$k]['trade_mode_format'] = config('trade_mode.'.$val['trade_mode']);
        }

        $datas['extral'] = [
            'goodsclass_list' => (new \app\common\model\Goodsclass())->getGoodsclassListByParentId(0),
        ];
        output_data($datas);
    }

    /**
     * 首页
     * @link http://shopcloudwj.ykzx/index.php/mobile/index/index
     */

    public function index() {

        $adv_model = new Adv();

        $condition['a.ap_id'] = array('in', array('15', '14', '27'));
        $condition['a.adv_enabled'] = 1;
        $data = $adv_model->mbadvlist($condition, 'a.adv_sort desc');
        $adname = array('15' => 'nav_list', '14' => 'adv_list', '27' => 'adv_list2'); //预留一部分扩充adv_list/home1/home2/home3/home4

        $datas = [];
        foreach ($data as $key => $val) {
            $val['adv_code'] = adv_image($val['adv_code']);
            $val['adv_links'] = $val['adv_link'] && is_string($val['adv_link']) ? explode(',', $val['adv_link']) : [];
            $datas[$adname[$val['ap_id']]][] = $val;
        }

        $datas['mobile_body'] = $data = $adv_model->getAdvpositionList([
            'ap_isuse'  => 1,
            'ap_position' => 1,
        ], '', 'ap_sort asc', true);

        //获取随机推荐的商品
        $recommend_list = model('goods')->getGoodsOnlineList(array('goods_commend' => 1), 'goods_id,goods_name,goods_image,goods_promotion_price, trade_mode,goods_price,goods_price_1,goods_price_2,goods_price_3', 0, '',10,'goods_commonid');
        foreach ($recommend_list as $k => $val) {
            $recommend_list[$k]['inviter_buying_price'] = inviter_buying_price($val, $this->member_info);
            $recommend_list[$k]['goods_image'] = goods_thumb($val);
            $recommend_list[$k]['trade_mode_format'] = config('trade_mode.'.$val['trade_mode']);
        }
        $datas['recommend_list'] = $recommend_list;
        //限时折扣

        $promotion_list = model('pxianshigoods')->getXianshiGoodsCommendList(6);
        foreach ($promotion_list as $k => $val) {
            $promotion_list[$k]['goods_image'] = goods_thumb($val);
            $promotion_list[$k]['trade_mode_format'] = config('trade_mode.'.$val['trade_mode']);
        }
        $datas['promotion_list'] = $promotion_list;
        $datas['extral'] = [
            'goodsclass_list' => (new \app\common\model\Goodsclass())->getGoodsclassListByParentId(0),
        ];

        // 获取秒杀列表
        $groupbuy = new Groupbuy();
        $groupbuy_model = model('groupbuy');
        $res_list = $groupbuy->show_groupbuy_list($groupbuy_model,'getGroupbuyOnlineList',null);

        foreach ($res_list as $k => $v){
            $res_list[$k]["count_down_day"] = bcdiv($v["count_down"],86400,0);
            $res_list[$k]["count_down_hour"] = bcdiv($v["count_down"]%86400,3600,0);
            $res_list[$k]["count_down_minute"] = bcdiv($v["count_down"]%3600,60,0);
            $res_list[$k]["count_down_second"] = $v["count_down"]%60;
            $res_list[$k]["goods_image"] = goods_cthumb($res_list[$k]["goods_image"]);
        }

        $datas["seckill_list"]["title"] = "用于测试，后期修改";
        $datas["seckill_list"]["list"] = $res_list;


        output_data($datas);
        //output_error('错误：',$datas);
    }
    /**
     * 弹出广告
     */
    public function adv(){
        $condition['ap_ispopup'] = 1;
        $adv = Db::name('advposition')->where($condition)->order('ap_id desc')->find();

        if($adv){
            $data = Db::name('adv')->where(['ap_id' => $adv['ap_id'],'adv_device' => 'APP'])->order('adv_id desc')->find();
            if($data){
                $data['adv_code'] = adv_image($data['adv_code']);
                if(time() >= $data['adv_startdate'] && time() <= $data['adv_enddate']){
                    output_data($data);
                }else{
                    output_data([]);
                }
            }else{
                output_data([]);
            }

        }else{
            output_data([]);
        }

    }

    /**
     * android客户端版本号
     */
    public function apk_version() {
        $version = config('mobile_apk_version');
        $url = config('mobile_apk');
        if (empty($version)) {
            $version = '';
        }
        if (empty($url)) {
            $url = '';
        }

        output_data(array('version' => $version, 'url' => $url));
    }

    /**
     * 默认搜索词列表
     */
    public function search_key_list() {
        $list = @explode(',', config('hot_search'));
        if (!$list || !is_array($list)) {
            $list = array();
        }
        if (cookie('hisSearch') != '') {
            $his_search_list = explode('~', cookie('hisSearch'));
        }else{
            $his_search_list = array();
        }
        if (!is_array($his_search_list)) {
            $his_search_list = array();
        }
        output_data(array('list' => $list, 'his_list' => $his_search_list));
    }

    /**
     * 热门搜索列表
     */
    public function search_hot_info() {
        if (config('rec_search') != '') {
            $rec_search_list = @unserialize(config('rec_search'));
        }else{
            $rec_search_list = array();
        }
        $rec_search_list = is_array($rec_search_list) ? $rec_search_list : array();
        output_data(array('hot_info' => $rec_search_list ? $rec_search_list : array()));
    }

    /**
     * 高级搜索
     */
    public function search_adv() {
        $gc_id = intval(input('get.gc_id'));
        $area_list = model('area')->getAreaList(array('area_deep' => 1), 'area_id,area_name');
        output_data(array('area_list' => $area_list ? $area_list : array()));
    }
    /**
     * 获取营业执照
     */
    public function license (){
        $upload_file = UPLOAD_SITE_URL.'/'.ATTACH_COMMON;
        $business_license1 = $upload_file.'/'.config('business_license_1');
        $business_license2 = $upload_file.'/'.config('business_license_2');
        output_data(array('business_license1' => $business_license1,'business_license2' => $business_license2));
    }
}

?>
