<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/8
 * Time: 16:20
 */
namespace app\mobile\controller;


use app\common\model\Ppintuangroup;
use app\common\model\Ppintuanorder;
use think\Lang;

class Memberpintuan extends MobileMember {

    public function _initialize(){
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile/lang/zh-cn/member_pintuan.lang.php');
    }

    public function getPpintuangroupList(){
        //$state = $this->request->param('state_type', '', 'trim');
        $condition = $this->pintuan_type_no(input('post.state_type'));
        $condition['pintuangroup_headid'] = $this->member_info['member_id'];

        $memberpintuan_list = [];
        $memberpintuan_list['list'] = model('ppintuangroup')->getPintuanOrderList($condition, '', 'pintuangroup_starttime desc');

        output_data($memberpintuan_list);
    }
    //ƴ��״̬��
    private function pintuan_type_no($state) {
        $condition = array();
        switch ($state) {
            case 'state_wait':
                $condition['pintuangroup_state'] = '1';
                break;
            case 'state_success':
                $condition['pintuangroup_state'] = '2';
                break;
            case 'state_fail':
                $condition['pintuangroup_state'] = '0';
                break;
        }
        return $condition;
    }

}