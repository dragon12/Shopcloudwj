<?php


namespace app\mobile\controller;


use think\Lang;
use think\Db;
use think\Request;

class Groupbuy extends MobileMall{

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'home/lang/'.config('default_lang').'/groupbuy.lang.php');
    }


    /**
     * 秒杀列表
     **/
    public function groupbuy_list() {
        $type_id = intval(input("param.type_id"));
        $type_id = empty($type_id) ? 1 : $type_id;

        // 当前页数(框架自动判断)
        $page = intval(input("get.page")) ? intval(input("get.page")) : 1;
        // 每页条数
        $page_nums = intval(input("get.page_nums")) ? intval(input("get.page_nums")) : 10;

        $groupbuy_model = model('groupbuy');
        switch ($type_id){
            case "1":
                $res_list = $this->show_groupbuy_list($groupbuy_model,'getGroupbuyOnlineList',$page_nums);
                break;
            case "2":
                // 即将开始
                $res_list = $this->show_groupbuy_list($groupbuy_model,'getGroupbuySoonList',$page_nums);
                break;
            case "3":
                // 往期抢购
                $res_list = $this->show_groupbuy_list($groupbuy_model,'getGroupbuyHistoryList',$page_nums);
                break;
            default:
                $res_list = [];
                break;
        }
        output_data($groupbuy_model->page_info);
    }

    /**
     * 秒杀详细信息
     **/
    public function groupbuy_detail() {
        if(request()->isPost()){
            $group_id = intval(input("post.group_id"));
            $goods_id = intval(input("post.id"));

            if(empty($group_id) || empty($goods_id)){
                output_data(["error"=>"确少参数"]);
            }
            $groupbuy_model = model("groupbuy");

            //获取抢购详细信息
            $groupbuy_info = $groupbuy_model->getGroupbuyInfoByID($group_id);
            if(empty($groupbuy_info)) {
                output_data(["error"=>lang('param_error')]);
            }

            $url = $_SERVER['SERVER_NAME']."/mobile/goods/goods_detail?goods_id=".$goods_id;
            $res_data = json_decode(http_request($url),true);

            $groupbuy_info["goods_detail"] = $res_data["result"]["goods_detail"];
            $groupbuy_info["goods_detail"]["goods_info"]["goods_attr"] = $res_data["result"]["goods_attr"];
            $groupbuy_info["groupbuy_image"] = goods_cthumb($groupbuy_info["groupbuy_image"],240);
            // if ($groupbuy_info['groupbuy_is_vr']) {
            //     $goods_info = model('goods')->getGoodsInfoByID($groupbuy_info['goods_id']);
            //     $buy_limit = max(0, (int) $goods_info['virtual_limit']);
            //     $upper_limit = max(0, (int) $groupbuy_info['groupbuy_upper_limit']);
            //     if ($buy_limit < 1 || ($buy_limit > 0 && $upper_limit > 0 && $buy_limit > $upper_limit)) {
            //         $buy_limit = $upper_limit;
            //     }
            //
            //     $this->assign('goods_info', $goods_info);
            // } else {
            //     $buy_limit = $groupbuy_info['groupbuy_upper_limit'];
            // }
            // $groupbuy_info['groupbuy_upper_limit'] = $buy_limit;

            // 浏览数加1
            $update_array = array();
            $update_array['groupbuy_views'] = Db::raw('groupbuy_views+1');
            $groupbuy_model->editGroupbuy($update_array, array('groupbuy_id'=>$group_id));

            // // 好评率
            // $evaluategoods_model = model('evaluategoods');
            // $evaluate_info = $evaluategoods_model->getEvaluategoodsInfoByCommonidID($groupbuy_info['goods_commonid']);
            // $this->assign('evaluate_info', $evaluate_info);

            output_data($groupbuy_info);
        }

    }


    /**
     * 获取秒杀列表
     **/
    public function show_groupbuy_list($model,$function_name,$page_nums) {
        $groupbuy_model = $model;

        $condition = array(
            'groupbuy_is_vr' => 0,
        );
        $order = '';

        // 分类筛选条件
        if (($gclass_id = (int) input('class')) > 0) {
            $condition['gclass_id'] = $gclass_id;

            if (($s_gclass_id = (int) input('s_class')) > 0)
                $condition['s_gclass_id'] = $s_gclass_id;
        }

        // 价格区间筛选条件
        if (($price_id = intval(input('groupbuy_price'))) > 0
            && isset($this->groupbuy_price[$price_id])) {
            $p = $this->groupbuy_price[$price_id];
            $condition['groupbuy_price'] = array('between', array($p['gprange_start'], $p['gprange_end']));
        }

        // 排序
        $groupbuy_order_key = trim(input('groupbuy_order_key'));
        $groupbuy_order = input('groupbuy_order') == '2'?'desc':'asc';
        if(!empty($groupbuy_order_key)) {
            switch ($groupbuy_order_key) {
                case '1':
                    $order = 'groupbuy_price '.$groupbuy_order;
                    break;
                case '2':
                    $order = 'groupbuy_rebate '.$groupbuy_order;
                    break;
                case '3':
                    $order = 'groupbuy_buyer_count '.$groupbuy_order;
                    break;
            }
        }

        $groupbuy_list = $groupbuy_model->$function_name($condition, $page_nums, $order);
        return $groupbuy_list;

    }


}