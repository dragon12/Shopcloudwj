<?php

namespace app\mobile\controller;

use think\Lang;

class Pointprod extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\pointprod.lang.php');
        //判断系统是否开启积分兑换功能
        if (config('pointprod_isuse') != 1) {
            output_error('积分兑换功能为开启');
        }
    }

    public function index() {
        $this->plist();
    }

    /**
     * 积分服务列表
     */
    public function plist() {

        $model_pointprod = model('pointprod');

        //展示状态
        $pgoodsshowstate_arr = $model_pointprod->getPgoodsShowState();
        //开启状态
        $pgoodsopenstate_arr = $model_pointprod->getPgoodsOpenState();

        $model_member = model('member');
        //查询会员等级
        $membergrade_arr = $model_member->getMemberGradeArr();

        //查询兑换服务列表
        $where = array();
        $where['pgoods_show'] = $pgoodsshowstate_arr['show'][0];
        $where['pgoods_state'] = $pgoodsopenstate_arr['open'][0];

        $keyword = input('keyword', '', 'trim');
        $level = input('level', 0, 'intval');
        $isable = input('isable', 0, 'intval');
        $points_min = input('points_min', 0, 'floatval');
        $points_max = input('points_max', 0, 'floatval');

        if (!empty($keyword)) {
            $where['pgoods_name|pgoods_keywords'] = array('like', '%' . $keyword . '%');
            if ($_COOKIE['hisSearch2'] == '') {
                $his_sh_list = array();
            } else {
                $his_sh_list = explode('~', $_COOKIE['hisSearch2']);
            }
            if (strlen($keyword) <= 20 && !in_array($keyword, $his_sh_list)) {
                if (array_unshift($his_sh_list, $keyword) > 8) {
                    array_pop($his_sh_list);
                }
            }
            setcookie('hisSearch2', implode('~', $his_sh_list), time() + 2592000, '/', SUBDOMAIN_SUFFIX ? SUBDOMAIN_SUFFIX : '', false);
        }
        //会员级别
        $level_filter = array();
        if (isset($level)) {
            $level_filter['search'] = intval($level);
        }
        if (intval($isable) == 1) {
            if ($memberid = $this->getMemberIdIfExists()) {
                $member_infotmp = model('member')->getMemberInfoByID($memberid);
                //当前登录会员等级信息
                $membergrade_info = $model_member->getOneMemberGrade($member_infotmp['member_exppoints'], true);
                $this->member_info = array_merge($member_infotmp, $membergrade_info);
            }
        }
        if (intval($isable) == 1 && isset($this->member_info['level'])) {
            $level_filter['isable'] = intval($this->member_info['level']);
        }
        if (count($level_filter) > 0) {
            if (isset($level_filter['search']) && isset($level_filter['isable'])) {
//                $where['pgoods_limitmgrade'] = array(array('eq', $level_filter['search']), array('elt', $level_filter['isable']), 'and');
            } elseif (isset($level_filter['search'])) {
//                $where['pgoods_limitmgrade'] = $level_filter['search'];
            } elseif (isset($level_filter['isable'])) {
//                $where['pgoods_limitmgrade'] = array('elt', $level_filter['isable']);
            }
        }


        //查询仅我能兑换和所需积分
        $points_filter = array();
        if (intval($isable) == 1 && isset($this->member_info['level'])) {
            $points_filter['isable'] = $this->member_info['member_points'];
        }
        if (intval($points_min) > 0) {
            $points_filter['min'] = intval($_GET['points_min']);
        }
        if (intval($points_max) > 0) {
            $points_filter['max'] = intval($_GET['points_max']);
        }
        if (count($points_filter) > 0) {
            asort($points_filter);
            if (count($points_filter) > 1) {
                $points_filter = array_values($points_filter);
                $where['pgoods_points'] = array('between', array($points_filter[0], $points_filter[1]));
            } else {
                if (isset($points_filter['min']) && $points_filter['min']) {
                    $where['pgoods_points'] = array('egt', $points_filter['min']);
                } elseif (isset($points_filter['max']) && $points_filter['max']) {
                    $where['pgoods_points'] = array('elt', $points_filter['max']);
                } elseif (isset($points_filter['isable']) && $points_filter['isable']) {
                    $where['pgoods_points'] = array('elt', $points_filter['isable']);
                }
            }
        }


        //排序
        $orderby = input('orderby', '', 'trim');
        switch ($orderby) {
            case 'stimedesc':
                $orderby = 'pgoods_starttime desc,';
                break;
            case 'stimeasc':
                $orderby = 'pgoods_starttime asc,';
                break;
            case 'pointsdesc':
                $orderby = 'pgoods_points desc,';
                break;
            case 'pointsasc':
                $orderby = 'pgoods_points asc,';
                break;
            default:
                $orderby = '';
        }
        $orderby .= 'pgoods_sort asc,pgoods_id desc';
        $filed = '*';
        $pageSize = 10;
        $pointprod_list = $model_pointprod->getPointProdList($where, $filed, $orderby, '', $pageSize);
        $page_count = $model_pointprod->page_info;

        output_data(array('goods_list' => $pointprod_list, 'grade_list' => $membergrade_arr, 'ww' => json_encode($where)), mobile_page($page_count));
    }

    /**
     * 积分礼品详细
     */
    public function pinfo() {
        $pid = intval($_GET['id']);
        if (!$pid) {
            output_error('参数错误!');
        }
        $model_pointprod = model('pointprod');
        //查询兑换礼品详细
        $prodinfo = $model_pointprod->getOnlinePointProdInfo(array('pgoods_id' => $pid));

        $goods_attr = $prodinfo['goods_attr'];
        if (empty($prodinfo)) {
            output_error('商品参数错误!');
        }
        $prodinfo['pgoods_body'] = html_entity_decode($prodinfo['pgoods_body']);
        //更新礼品浏览次数
        $tm_tm_visite_pgoods = cookie('tm_visite_pgoods');
        $tm_tm_visite_pgoods = $tm_tm_visite_pgoods ? explode(',', $tm_tm_visite_pgoods) : array();
        if (!in_array($pid, $tm_tm_visite_pgoods)) {//如果已经浏览过该服务则不重复累计浏览次数 
            $result = $model_pointprod->editPointProdViewnum($pid);
            if ($result['state'] == true) {//累加成功则cookie中增加该服务ID
                $tm_tm_visite_pgoods[] = $pid;
                cookie('tm_visite_pgoods', implode(',', $tm_tm_visite_pgoods));
            }
        }

        //查询兑换信息
        $model_pointorder = model('pointorder');
        $pointorderstate_arr = $model_pointorder->getPointOrderStateBySign();
        $where = array();
        $where['point_orderstate'] = array('neq', $pointorderstate_arr['canceled'][0]);
        $where['pointog_goodsid'] = $pid;
        $orderprod_list = $model_pointorder->getPointOrderAndGoodsList($where, '*',  'pointsordergoods.pointog_recid desc');
        if ($orderprod_list) {
            $buyerid_arr = array();
            foreach ($orderprod_list as $k => $v) {
                $buyerid_arr[] = $v['point_buyerid'];
            }
            $memberlist_tmp = model('member')->getMemberList(array('member_id' => array('in', $buyerid_arr)), 'member_id,member_avatar');
            $memberlist = array();
            if ($memberlist_tmp) {
                foreach ($memberlist_tmp as $v) {
                    $memberlist[$v['member_id']] = $v;
                }
            }
            foreach ($orderprod_list as $k => $v) {
//                $v['member_avatar'] = ($t = $memberlist[$v['point_buyerid']]['member_avatar']) ? UPLOAD_SITE_URL . DS . ATTACH_AVATAR . DS . $t : UPLOAD_SITE_URL . DS . ATTACH_COMMON . DS . config('default_user_portrait');
                $v['member_avatar'] = get_member_avatar_for_id($v['point_buyerid']);
                $orderprod_list[$k] = $v;
            }
        }

        $goods_attrs = [];
        foreach ($goods_attr as $key => $item){
            $goods_attrs[] = array_values($item);
        }
        $prodinfo['goods_attr'] = $goods_attrs;
        //热门积分兑换服务
        $recommend_pointsprod = $model_pointprod->getRecommendPointProd(5);

        output_data(array('goods_commend_list' => $orderprod_list, 'goods_info' => $prodinfo));
    }

}

?>
