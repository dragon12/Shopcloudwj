<?php

namespace app\mobile\controller;

use think\Lang;

class Memberpaymentrecharge extends MobileMember {

    private $payment_code;
    private $payment_config;

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\memberorder.lang.php');
        if (!input('post.payment_code')) {

            $payment_code = 'alipay_h5';
            if (input('payment_code')) {

                $payment_code = input('payment_code');
            }



            $model_mb_payment = model('mbpayment');

            $condition = array();

            $condition['payment_code'] = $payment_code;

            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);

            if (!$mb_payment_info) {
                output_error('支付方式未开启');
            }



            $this->payment_code = $payment_code;

            $this->payment_config = $mb_payment_info['payment_config'];
        }
    }

    /**
     * 账户充值
     */
    public function pd_pay() {
        $pay_sn = input('pay_sn');
        $model_mb_payment = model('mbpayment');
        $condition = array();
        $condition['payment_code'] = $this->payment_code;
        $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
//        $mb_payment_info['payment_config'] = unserialize($mb_payment_info['payment_config']);
//        dump($mb_payment_info);die;
        if (!$mb_payment_info) {
            output_error('系统不支持选定的支付方式');
            exit();
        }
        $logic_payment = model('payment', 'logic');
    	$result = $logic_payment->getPdOrderInfo($pay_sn, session('member_id'));
        if (!$result['code']) {
            output_error($result['msg']);
            exit();
        }
        if ($result['data']['pdr_payment_state'] || empty($result['data']['api_pay_amount'])) {
            output_error('该充值单不需要支付');
            exit();
        }

        $this->_api_pay_pd($result['data'], $mb_payment_info);
    }

    /**
     * 第三方在线支付接口
     *
     */
    private function _api_pay_pd($order_pay_info, $mb_payment_info) {

        $inc_file = APP_PATH . DS . DIR_MOBILE . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';
        if (!is_file($inc_file)) {
            output_error('支付接口不存在');
            exit();
        }
        require($inc_file);
        $param = array();
        $param = $mb_payment_info['payment_config'];

        // wxpay_jsapi

        if ($this->payment_code == 'wxpay_jsapi') {

            $param['orderSn'] = $order_pay_info['pay_sn'];

            $param['orderFee'] = (int) (100 * $order_pay_info['api_pay_amount']);

            $param['orderInfo'] = config('site_name') . '订单' . $order_pay_info['pay_sn'];

            $param['orderAttach'] = $order_pay_info['order_type'];

            $api = new \wxpay_jsapi();

            $api->setConfigs($param);

            try {

                echo $api->paymentHtml($this);
            } catch (\Exception $ex) {
                
            }

            exit;
        }

        $payment_api = new $this->payment_code(array_merge($param,$order_pay_info));
        $return = $payment_api->submit($param);
        echo $return;
        exit;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
