<?php

namespace app\mobile\controller;


class Qrcode extends MobileHome {

    public function index() {
        import('qrcode.phpqrcode', EXTEND_PATH);
        $value = input('param.url');

        $errorCorrectionLevel = "L";
        $matrixPointSize = input('param.size', 4, 'intval');
        \QRcode::png($value, false, $errorCorrectionLevel, $matrixPointSize,2);
        exit;
    }
}
