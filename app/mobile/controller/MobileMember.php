<?php

namespace app\mobile\controller;

class MobileMember extends MobileHome {

    protected $member_info = [];
    protected $key = '';

    public function _initialize() {
        parent::_initialize();
        $agent = $_SERVER['HTTP_USER_AGENT'];

        $this->key = $this->token;
        if (strpos($agent, "MicroMessenger") && request()->controller() == 'Wxauto') {
            $this->wxconfig = db('wxconfig')->find();
            $this->appId = $this->wxconfig['appid'];
            $this->appSecret =$this->wxconfig['appsecret'];

            /**
             * 当前登录人信息
             */
            $model_mb_user_token = model('mbusertoken');
            $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($this->token);

            if($mb_user_token_info) {
                $model_member = model('member');
                $this->member_info = $model_member->getMemberInfoByID($mb_user_token_info['member_id']);
            }
        } else {
            $model_mb_user_token = model('mbusertoken');
            $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($this->token);
            if (empty($mb_user_token_info)) {
                output_error('请登录', array('login' => '0'));
            }
            $model_member = model('member');
            $this->member_info = $model_member->getMemberInfoByID($mb_user_token_info['member_id']);

            if (empty($this->member_info)) {
                output_error('请登录', array('login' => '0'));
            } else {
                $this->member_info['client_type'] = $mb_user_token_info['member_clienttype'];
                $this->member_info['openid'] = $mb_user_token_info['member_openid'];
                $this->member_info['token'] = $mb_user_token_info['member_token'];
                $level_name = $model_member->getOneMemberGrade($mb_user_token_info['member_id']);
                $this->member_info['level_name'] = $level_name['level_name'];
                //考虑到模型中session
                if(session('member_id')!=$this->member_info['member_id']){
                    //避免重复查询数据库
                    $model_member->createSession(array_merge($this->member_info,$level_name));
                }

            }
        }
    }

    protected function getOpenidBySessionkey() {
        $sessionkey = $this->request->param('sessionkey', '', 'trim');
        $this->wxconfig_load('allinpay_minipro');

        // $res['openid'].'|'.$res['session_key'].'|'.$res['unionid'];
        $sessionkey = explode('|', sc_decrypt($sessionkey, $this->appId));

        return $sessionkey[0];
    }
    public function getOpenId() {
        return $this->member_info['member_wxopenid'];
    }

    public function setOpenId($openId) {
        $this->member_info['member_wxopenid'] = $openId;
        model('mbusertoken')->editMemberOpenId($this->member_info['token'], $openId);
    }

    /**
     * 设置 代理商对销售订单的管理的搜索条件
     * @param int member_id
     * @return Object Closure
     */
    public function build_member_condition($member_id)
    {
        $closure = function ($query) use($member_id) {
            $query->whereOr([
                'buyer_id' => $member_id,
                function ($query) use($member_id) {
                    $query->where([
                        'inviter_id' => $member_id,
                        'order_buy_type' => ['eq', 2],
                    ]);
                }
            ]);
        };
        return $closure;
    }
}

?>
