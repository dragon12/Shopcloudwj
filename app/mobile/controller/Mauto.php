<?php
/**
 * 非微信接口
 */

namespace app\mobile\controller;


class Mauto extends MobileHome
{
    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    // 自动注册
    public function register()
    {
        $nickname = system_no('account');
        $member_name = $nickname;
        $model_member = model('member');
        $password = rand(100000, 999999);
        $member = array();
        $member['member_password'] = $password;
        $member['member_email'] = '';

        $member['member_name'] = $member_name;
        $member['member_truename'] = $member_name;
        $model_member->addMember($member);

        $member = $model_member->getMemberInfo(array('member_name' => $member_name));
        if($member) {
            $token = $this->_get_token($member['member_id'], $member['member_name'], 'wap');
            if($token) {
                output_data(array('username' => $member['member_name'], 'key' => $token));
            }
        } else {
            output_data(array('username' => false, 'key' => false));
        }
    }
}