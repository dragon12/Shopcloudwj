<?php

namespace app\mobile\controller;

use think\Controller;

/*
 * 基类
 */

class MobileHome extends Controller
{
    public $appId;
    public $appSecret;

    //客户端类型
    protected $client_type_array = array('pc', 'mobile', 'app', 'android', 'wap', 'wechat', 'ios', 'windows', 'jswechat');
    //列表默认分页数
    protected $pagesize = 5;
    protected $token = '';
    protected $client_type;

    public function _initialize()
    {
        parent::_initialize();


        // 记录日志
        if(true || config('app_debug')) {
            config('request_log_id', \think\Db::name('api_log')->insertGetId([
                'request_uri'   => $_SERVER['REQUEST_URI'],
                'file_string'   => json_encode($_FILES, JSON_UNESCAPED_UNICODE),
                'input'         => file_get_contents('php://input'),
                'post_string'   => json_encode($_POST, JSON_UNESCAPED_UNICODE),
                'update_time'   => date('Y-m-d H:i:s'),
                'create_time'   => date('Y-m-d H:i:s'),
            ]));
        }


        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Headers: Origin, *');

        if($this->request->method() === 'OPTIONS') {
            exit();
        }

        $this->token      = $this->request->header('XX-Token') ?: $this->request->param('token', $this->request->param('key'));
        $this->client_type = $this->request->header('XX-Device-Type') ?: 'pc';

        //分页数处理
        $pagesize = intval(input('get.pagesize'));
        if ($pagesize > 0) {
            $this->pagesize = $pagesize;
        }
        else {
            $this->pagesize = 10;
        }
        /*加入配置信息*/
        $config_list = rkcache('config', true);
        config($config_list);
    }

    public function httpGet($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 50);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);

        $log = date('Y-m-d H:i:s').' | GET '.$url."\r\n";
        $log .= 'Result: '.$res."\r\n";
        file_rput_contents(RUNTIME_PATH.'http_log/'.date('Y-m/d').'.log', $log, FILE_APPEND);
        return $res;
    }

    /**
     * 登录生成token
     */
    protected function _get_token($member_id, $member_name, $client, $openid = '')
    {
        $model_mb_user_token = model('mbusertoken');
        //生成新的token
        $mb_user_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0, 999999)));
        $mb_user_token_info['member_id'] = $member_id;
        $mb_user_token_info['member_name'] = $member_name;
        $mb_user_token_info['member_token'] = $token;
        $mb_user_token_info['member_openid'] = $openid;
        $mb_user_token_info['member_logintime'] = TIMESTAMP;
        $mb_user_token_info['member_clienttype'] = $client;

        $result = $model_mb_user_token->addMbUserToken($mb_user_token_info);
        if ($result) {
            return $token;
        }
        else {
            return null;
        }

    }

    // 配置加载
    protected function wxconfig_load($payment_code) {

        $logic_payment = model('payment', 'logic');
        $result = $logic_payment->getPaymentInfo($payment_code);   // 微信相关支付，这里用实际支付方式获取对应支付配置，同一类支付统一经由一个类处理
        $payment_info = $result['data'];

        if(in_array($payment_code, ['allinpay_minipro'])) {
            $this->appId = $payment_info['payment_config']['allinpay_sub_appid2'];
            $this->appSecret = $payment_info['payment_config']['allinpay_sub_appsecret2'];
        } else {
            $this->appId = $payment_info['payment_config']['wx_appid'];
            $this->appSecret = $payment_info['payment_config']['wx_appsecret'];
        }
    }
}
