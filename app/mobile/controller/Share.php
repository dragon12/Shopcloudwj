<?php

namespace app\mobile\controller;

use app\common\model\Order;
use think\Image;
use think\Lang;

class Share extends MobileMall {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\member.lang.php');
    }
    
    
    /**
     * 产品分享
     */
    public function goods() {
        $goods_id = $this->request->param('goods_id', 0, 'intval');
        $member_id = $this->request->param('member_id', 0, 'intval');
        $pintuangroup_share_id = $this->request->param('pintuangroup_share_id', 0, 'intval');

        $goods_model = new \app\common\model\Goods();
        $goods = $goods_model->getGoodsInfoByID($goods_id);
        $goods_name = str_cut($goods['goods_name'], 40, '...');
        $image_path = goods_thumb_path($goods);

        $bg = PUBLIC_PATH.'/static/home/images/qrcode/goods_bg.png';
        $fonts_path = PUBLIC_PATH.'/font/msyh.ttf';
        // 二维码临时缓存
        $qrcode_path = RUNTIME_PATH.'qrcode/'.date('Ym/d_His').'_'.mt_rand(100000, 999999).'.png';
        // 产品图片临时缓存
        $goods_thumb = RUNTIME_PATH.'qrcode/'.date('Ym/d_His').'_goods_'.mt_rand(100000, 999999).'.png';
        is_dir(dirname($qrcode_path)) or mkdir(dirname($qrcode_path), 0777, true);

        import('qrcode.phpqrcode', EXTEND_PATH);
        $params = ['goods_id' => $goods_id];
        $member_id and $params['inviter_id'] = $member_id;
        $pintuangroup_share_id and $params['pintuangroup_share_id'] = $pintuangroup_share_id;

        $share_url = \think\Request::instance()->domain().'/wap/mall/share_product_detail.html?'.http_build_query($params);

        $errorCorrectionLevel = "L";
        $matrixPointSize = 8;
        \QRcode::png($share_url, $qrcode_path, $errorCorrectionLevel, $matrixPointSize, 2);
        Image::open($qrcode_path)->thumb(260, 260, Image::THUMB_CENTER)->save($qrcode_path);

        Image::open($image_path)->thumb(560, 560, Image::THUMB_CENTER)->save($goods_thumb);
        Image::open($bg)
            ->water($qrcode_path, array(60, 780), 100)
            ->water($goods_thumb, array(40, 40), 100)
            ->text($goods_name, $fonts_path, 20, '#999999', array(40, 620))
            ->text('￥'.$goods['goods_price'], $fonts_path, 30, '#FF0000', array(150, 685))
            ->text('优惠价：', $fonts_path, 20, '#999999', array(40, 690))
            ->save(null);
    }


    /**
     * 分享订单
     */
    public function order() {
        $code = $this->request->param('code', '', 'trim');
        $code = str_replace('.png', '', $code);
        $download = $this->request->param('download', 0, 'intval');

        $order_sn = sc_decrypt($code);

        $share_url = \think\Request::instance()->domain().'/wap/public/order.html?'.http_build_query([
                'code' => $code
            ]);

        $order_model = new Order();
        $inviter_model = new \app\common\model\Inviter();
        $order_info = $order_model->getOrderInfo(['order_sn' => $order_sn]);

        $inviter_info = $order_info['inviter_id'] ? $inviter_model->getInviterInfoByID($order_info['inviter_id']) : [];

        $errorCorrectionLevel = "L";
        $matrixPointSize = 8;

        $fonts_path = PUBLIC_PATH.'/font/msyh.ttf';
        $bg = PUBLIC_PATH.'/static/home/images/qrcode/common.png';
        // 二维码临时缓存
        $qrcode_path = RUNTIME_PATH.'qrcode/'.date('Ym/d_His').'_'.mt_rand(100000, 999999).'.png';
        is_dir(dirname($qrcode_path)) or mkdir(dirname($qrcode_path), 0777, true);

        import('qrcode.phpqrcode', EXTEND_PATH);
        \QRcode::png($share_url, $qrcode_path, $errorCorrectionLevel, $matrixPointSize, 2);

        header("Content-type: image/png");
        Image::open($qrcode_path)->thumb(400, 400, Image::THUMB_CENTER)->save($qrcode_path);

        $image = Image::open($bg);
//        if($inviter_info) {
//            $image->text($inviter_info['inviter_name'] . '', $fonts_path, 24, '#3c3c3c', array(80, 42));
//        }
        $image->water($qrcode_path, array(50, 100), 100)
            ->text('订单号：'.$order_sn, $fonts_path, 18, '#3c3c3c', array(65, 500))
            ->save(null);
        if ($download) {
            $download = sprintf('%s_%s.png', '订单', $order_sn);
            header('Content-Disposition:attachment;filename=' . $download);
        }
        die;
    }
}
