<?php

namespace app\mobile\controller;

class MobileSeller extends MobileHome {

    protected $seller_info = array();
    protected $seller_group_info = array();
    protected $member_info = array();
    protected $store_info = array();
    protected $store_grade = array();

    public function _initialize() {
        parent::_initialize();

        $model_mb_seller_token = model('mbsellertoken');

        $key = input('post.key');
        if (empty($key)) {
            $key = input('get.key');
        }
        if (empty($key)) {
            output_error('请登录', array('login' => '0'));
        }

        $mb_seller_token_info = $model_mb_seller_token->getMbSellerTokenInfoByToken($key);
        if (empty($mb_seller_token_info)) {
            output_error('请登录', array('login' => '0'));
        }

        $model_seller = model('seller');
        $model_member = model('member');
        $model_store = model('store');
        $model_seller_group = model('sellergroup');

        $this->seller_info = $model_seller->getSellerInfo(array('seller_id' => $mb_seller_token_info['seller_id']));
        $this->member_info = $model_member->getMemberInfoByID($this->seller_info['member_id']);
        $this->store_info = $model_store->getStoreInfoByID($this->seller_info['store_id']);
        $this->seller_group_info = $model_seller_group->getSellerGroupInfo(array('group_id' => $this->seller_info['seller_group_id']));
        
        // 店铺等级
        if (intval($this->store_info['is_platform_store']) === 1) {
            $this->store_grade = array(
                'sg_id' => '0',
                'sg_name' => '自营店铺专属等级',
                'sg_goods_limit' => '0',
                'sg_album_limit' => '0',
                'sg_space_limit' => '999999999',
                'sg_template_number' => '6',
                'sg_price' => '0.00',
                'sg_description' => '',
                'sg_function' => 'editor_multimedia',
                'sg_sort' => '0',
            );
        } else {
            $store_grade = rkcache('store_grade', true);
            $this->store_grade = $store_grade[$this->store_info['grade_id']];
        }

        if (empty($this->member_info)) {
            output_error('请登录', array('login' => '0'));
        } else {
            $this->seller_info['client_type'] = $mb_seller_token_info['client_type'];
        }
        $model_seller->createSellerSession(array_merge($this->member_info,$this->store_info,$this->seller_info, is_array($this->seller_group_info)?$this->seller_group_info:array()));
    }
    /**
     * 记录卖家日志
     *
     * @param $content 日志内容
     * @param $state 1成功 0失败
     */
    protected function recordSellerLog($content = '', $state = 1) {
        $seller_info = array();
        $seller_info['log_content'] = $content;
        $seller_info['log_time'] = TIMESTAMP;
        $seller_info['log_seller_id'] =$this->seller_info['seller_id'];
        $seller_info['log_seller_name'] = $this->seller_info['seller_name'];
        $seller_info['log_store_id'] = $this->store_info['store_id'];
        $seller_info['log_seller_ip'] = request()->ip();
        $seller_info['log_url'] = request()->module() . '/' . request()->controller() . '/' . request()->action();
        $seller_info['log_state'] = $state;
        $model_seller_log = model('sellerlog');
        $model_seller_log->addSellerLog($seller_info);
    }
}

?>
