<?php

namespace app\mobile\controller;


use app\common\logic\Buy;
use app\common\model\Address;
use app\common\model\Cart;
use app\common\model\Order;

class Memberbuy extends MobileMember
{
    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    /**
     * 购物车、直接购买第一步:选择收获地址和配置方式
     */
    public function buy_step1()
    {

        $cart_id = explode(',', $_POST['cart_id']);

        $logic_buy = model('buy','logic');

        //得到会员等级
        $model_member = model('member');
        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);


       if (!$member_info['is_buylimit']) {
            output_error('您没有商品购买的权限,如有疑问请联系客服人员');
        }
        /*
        if ($member_info) {
            $member_gradeinfo = $model_member->getOneMemberGrade(intval($member_info['member_exppoints']));
            $member_discount = $member_gradeinfo['orderdiscount'];
            $member_level = $member_gradeinfo['level'];
        }
        else {
            $member_discount = $member_level = 0;
        }*/

        $param_data = $this->request->post();
        $param_data["pintuan_id"] = empty($param_data["pintuan_id"]) ? "" : $param_data["pintuan_id"];
        $param_data["pintuangroup_id"] = empty($param_data["pintuangroup_id"]) ? "" : $param_data["pintuangroup_id"];
        //得到购买数据
        $ifcart=!empty(input('post.ifcart'))?true:false;
        $result = $logic_buy->buyStep1($cart_id, $ifcart, $this->member_info['member_id'], $param_data);

        if (!$result['code']) {
            output_error($result['msg']);
        }
        else {
            $result = $result['data'];
        }

        if (intval(input('post.address_id')) > 0)
        {
            $result['address_info'] = model('address')->getDefaultAddressInfo(array('address_id' => intval(input('post.address_id')), 'member_id' => $this->member_info['member_id']));
        }
        if ($result['address_info']) {
            $data_area = $logic_buy->changeAddr($result['freight_list'], $result['address_info']['city_id'], $result['address_info']['area_id'], $this->member_info['member_id']);
            if (!empty($data_area) && $data_area['state'] == 'success') {
                if (is_array($data_area['content'])) {
                    foreach ($data_area['content'] as $store_id => $value) {
                        $data_area['content'][$store_id] = sc_price_format($value);
                    }
                }
            }
            else {
                output_error('地区请求失败');
            }
        }

        //整理数据
        $cart_list = array();

        $cart_list['goods_list'] = $result['cart_list'];

        //根据商品类型进行分组处理
        $trade_group = [];
        foreach ($cart_list['goods_list'] as $item){
            isset($trade_group[$item['trade_mode']]) or $trade_group[$item['trade_mode']] = [
                'trade_mode' => $item['trade_mode'],
                'goods_info' => [],
                'trade_mode_format' => config('trade_mode.'.$item['trade_mode']),
                'total_price' => 0,
                'total' => 0,
                'tax' => 0,
                'total_tax' => 0
            ];
            $trade_group[$item['trade_mode']]['goods_info'][] = $item;
            $trade_group[$item['trade_mode']]['total_price'] += $item['goods_price'] * $item['goods_num'];

            $trade_group[$item['trade_mode']]['total'] += $item['goods_num'];
            //税额和总税额
            if(isset($item['tax_rate'])){
                $trade_group[$item['trade_mode']]['tax'] = round($item['tax_rate']/100 * $item['goods_price'] * $item['goods_num'],2);
                $trade_group[$item['trade_mode']]['total_tax'] += $trade_group[$item['trade_mode']]['tax'];
            }
        }
        $cart_list['goods_list'] = $trade_group;

        $cart_list['goods_total'] = $result['goods_total'];

        $cart_list['mansong_rule_list'] = isset($result['mansong_rule_list'])?$result['mansong_rule_list']:'';
        if (is_array($result['voucher_list']) && count($result['voucher_list']) > 0) {
            current($result['voucher_list']);
            $cart_list['voucher_info'] = reset($result['voucher_list']);
            $cart_list['voucher_info']['voucher_price'] = sc_price_format($cart_list['voucher_info']['voucher_price']);
            $cart_list['voucher_info']['voucher_end_date_text']=date('Y年m月d日',$cart_list['voucher_info']['voucher_enddate']);
            $total_list = $cart_list['voucher_info']['voucher_price'];
        }
        else {
            $cart_list['voucher_info'] = array();
        }

        $cart_list['voucher_list'] = $result['voucher_list'];
        if (!empty($result['cancel_calc_sid_list'])) {
            $cart_list['freight'] = '0';
            $cart_list['freight_message'] = $result['cancel_calc_sid_list']['desc'];
        }

        $buy_list = array();
        $buy_list['cart_list'] = $cart_list;
        $buy_list['cart_list_api']=array_values($cart_list);
        $buy_list['trade_mode'] = $result['trade_mode'];
        $buy_list['freight_hash'] = $result['freight_list'];
        $buy_list['address_info'] = $result['address_info'];
        $buy_list['ifshow_offpay'] = $result['ifshow_offpay'];
        $buy_list['vat_hash'] = $result['vat_hash'];
        $buy_list['vat_deny'] = $result['vat_deny'];
        $buy_list['inv_info'] = $result['inv_info'];
        $buy_list['available_predeposit'] = isset($result['available_predeposit'])?$result['available_predeposit']:array();
        $buy_list['available_rc_balance'] = isset($result['available_rc_balance'])?$result['available_rc_balance']:array();
        if (isset($result['rpt_list']) && !empty($result['rpt_list'])) {
            foreach ($result['rpt_list'] as $k => $v) {
                unset($result['rpt_list'][$k]['rpacket_id']);
                unset($result['rpt_list'][$k]['rpacket_end_date']);
                unset($result['rpt_list'][$k]['rpacket_owner_id']);
                unset($result['rpt_list'][$k]['rpacket_code']);
            }
        }
        $buy_list['rpt_list'] = isset($result['rpt_list']) ? $result['rpt_list'] : array();
        $buy_list['zk_list'] = isset($result['zk_list'])?$result['zk_list']:array();

        $data_area = $logic_buy->changeAddr($result['freight_list'], $result['address_info']['city_id'], $result['address_info']['area_id'], $this->member_info['member_id']);
        $rpacket_price = 0;
        $buy_list['order_amount'] = sc_price_format($cart_list['goods_total'] - $rpacket_price);
        $buy_list['address_api'] = $data_area ? $data_area : '';
        if ($data_area['content']) {

            $total_list = model('buy_1','logic')->reCalcGoodsTotal($cart_list['goods_total'], $data_area['content'], 'freight');
            $buy_list['final_total'] = $total_list;
            //返回可用平台红包
//            $result['rpt_list'] = model('buy_1','logic')->getStoreAvailableRptList($this->member_info['member_id'], array_sum($total_list), 'rpacket_limit desc');
//            reset($result['rpt_list']);
//            if (is_array($result['rpt_list']) && count($result['rpt_list']) > 0) {
//                $result['rpt_info'] = current($result['rpt_list']);
//                unset($result['rpt_info']['rpacket_id']);
//                unset($result['rpt_info']['rpacket_end_date']);
//                unset($result['rpt_info']['rpacket_owner_id']);
//                unset($result['rpt_info']['rpacket_code']);
//            }
        }else{
            $buy_list['final_total'] = $buy_list['order_amount'];
        }
//        $rpacket_price=isset($result['rpt_info']['rpacket_price']) ? $result['rpt_info']['rpacket_price']:'';
//        $rpacket_price = 0; 暂时注释
//        $buy_list['order_amount'] = sc_price_format($cart_list['goods_total'] - $rpacket_price);暂时注释
//        $buy_list['rpt_info'] = isset($result['rpt_info']) ? $result['rpt_info'] : array();
//          $buy_list['address_api'] = $data_area ? $data_area : '';暂时注释

//        $buy_list['final_total'] = $buy_list['order_amount'];

        output_data($buy_list);
    }

    protected function getDeliveryTypeByCartId($cart_id) {
        $cart = Cart::get($cart_id);

        $goods = \app\common\model\Goods::get($cart['goods_id']);
        return $goods['trade_mode'];
    }
    protected function getDeliveryTypeByGoodsId($goods_id) {

        $goods = \app\common\model\Goods::get($goods_id);
        return $goods['trade_mode'];
    }
    /**
     * 购物车、直接购买第二步:保存订单入库，产生订单号，开始选择支付方式
     *
     */
    public function buy_step2()
    {
        $_cart_ids = explode(',', $_POST['cart_id']);
        $ifchat = input('post.ifcart');
        $group = [];
        $need_valid = false;
        foreach($_cart_ids as $item) {
            $_data = explode('|', $item);
            if($ifchat ==''){
                $trade_mode = $this->getDeliveryTypeByGoodsId($_data[0]);
            }else{
                $trade_mode = $this->getDeliveryTypeByCartId($_data[0]);
            }
            $group[$trade_mode][] = $item;
            if($trade_mode > TRADE_NORMAL) $need_valid = true;
        }
        $address_id = input('post.address_id');
        $address = Address::get($address_id);
        if($need_valid && !$address['real_name']) {
            output_error('收货地址，必须提交实名信息');
        }

        $order_list = [];
        foreach ($group as  $val) {
            $param = array();
            $param['inviter_id'] = $this->request->param('inviter_id', 0, 'intval');
            $param['ifcart'] = input('post.ifcart');
            $param['cart_id'] = $val;
            $param['address_id'] = input('post.address_id');
            $param['vat_hash'] = input('post.vat_hash');
            $param['offpay_hash'] = input('post.offpay_hash');
            $param['offpay_hash_batch'] = input('post.offpay_hash_batch');
            $param['pay_name'] = input('post.pay_name');
            $param['invoice_id'] = input('post.invoice_id');
            $param['inviter_id'] = input('post.inviter_id');
            $param['rpt'] = input('post.rpt');
            $param['pintuan_id']=input('post.pintuan_id');
            $param['pintuangroup_id']=input('post.pintuangroup_id');
            $param['trade_mode']=input('post.trade_mode');


            //处理代金券
            $voucher = array();
            $_POST['voucher'] = isset($_POST['voucher']) ? $_POST['voucher'] : null;
            if($_POST['voucher']){
                $post_voucher = explode(',', $_POST['voucher']);
                if (!empty($post_voucher)) {
                    foreach ($post_voucher as $value) {
                        list($voucher_t_id, $voucher_price) = explode('|', $value);
                        $voucher = $value;
                    }
                }
            }
            $param['voucher'] = $voucher;

            $_POST['pay_message'] = input('post.pay_message', '');
//            $_POST['pay_message'] = explode(',', $_POST['pay_message']);
//            $param['pay_message'] = array();
//            if (is_array($_POST['pay_message']) && $_POST['pay_message']) {
//                foreach ($_POST['pay_message'] as $v) {
//                    if (strpos($v, '|') !== false) {
//                        $v = explode('|', $v);
//                        $param['pay_message'][$v[0]] = $v[1];
//                    }
//                }
//            }
            $param['pay_message'] =  $_POST['pay_message'];
            $param['pd_pay'] = input('post.pd_pay');
            $param['rcb_pay'] = input('post.rcb_pay');
            $param['password'] = input('post.password');
            $param['fcode'] = input('post.fcode');
            $param['order_from'] = 2;
            $logic_buy = new Buy();

            //得到会员等级
            /* $model_member = model('member');
             $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
             if ($member_info) {
                 $member_gradeinfo = $model_member->getOneMemberGrade(intval($member_info['member_exppoints']));
                 $member_discount = $member_gradeinfo['orderdiscount'];
                 $member_level = $member_gradeinfo['level'];
             }
             else {
                 $member_discount = $member_level = 0;
             }*/
            $result = $logic_buy->buyStep2($param, $this->member_info['member_id'], $this->member_info['member_name'], $this->member_info['member_email']);
            if (!$result['code']) {
                output_error($result['msg']);
            }

            $order_info = current($result['data']['order_list']);
            $order_list[] = array('pay_sn' => $result['data']['pay_sn'], 'payment_code' => $order_info['payment_code']);
        }
        output_data($order_list);
    }

    /**
     * 验证密码
     */
    public function check_password()
    {
        if (empty(input('post.password'))) {
            output_error('参数错误');
        }

        $model_member = model('member');

        $member_info = $model_member->getMemberInfoByID($this->member_info['member_id']);
        if ($member_info['member_paypwd'] == md5(input('post.password'))) {
            output_data('1');
        }
        else {
            output_error('密码错误');
        }
    }

    /**
     * 更换收货地址
     */
    public function change_address()
    {
        $logic_buy = model('buy','logic');
        $city_id = input('post.city_id');
        $area_id = input('post.area_id');
        if (empty($city_id)) {
            $city_id = $area_id;
        }

        $data = $logic_buy->changeAddr(input('post.freight_hash'), $city_id, $area_id, $this->member_info['member_id']);
        if (!empty($data) && $data['state'] == 'success') {
            output_data($data);
        }
        else {
            output_error('地址修改失败');
        }
    }

    /**
     * 获取支付状态
     *
     */
    public function pay_status() {
        $pay_sn = $this->request->param('pay_sn', '', 'trim');
        //取子订单列表
        $condition = array();
        $condition['pay_sn'] = $pay_sn;
        $condition['order_state'] = array('egt', ORDER_STATE_PAY);
        $order_list = (new Order())->getOrderList($condition, '', '*', '', '', array(), true);

        output_data($order_list ? 'success' : 'fail');
    }

    /**
     * 实物订单支付(新接口)
     */
    public function pay()
    {
        $pay_sn = input('post.pay_sn');

        //查询支付单信息
        $model_order = model('order');
        $pay_info = $model_order->getOrderPayInfo(array(
                                                      'pay_sn' => $pay_sn, 'buyer_id' => $this->member_info['member_id']
                                                  ), true);
        if (empty($pay_info)) {
            output_error('该订单不存在');
        }

        //取子订单列表
        $condition = array();
        $condition['pay_sn'] = $pay_sn;
        $condition['order_state'] = array('in', array(ORDER_STATE_NEW, ORDER_STATE_PAY));
        $order_list = $model_order->getOrderList($condition, '', '*', '', '', array(), true);
        if (empty($order_list)) {
            output_error('未找到需要支付的订单');
        }

        //定义输出数组
        $pay = array();
        //支付提示主信息
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payed_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payed_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['pay_diff_amount'] = 0;
        //账户可用金额
        $pay['member_available_pd'] = 0;
        $pay['member_available_rcb'] = 0;

        $logic_order = model('order','logic');

        //计算相关支付金额
        foreach ($order_list as $key => $order_info) {
            if (!in_array($order_info['payment_code'], array('offline', 'chain'))) {
                if ($order_info['order_state'] == ORDER_STATE_NEW) {
                    $pay['payed_rcb_amount'] += $order_info['rcb_amount'];
                    $pay['payed_pd_amount'] += $order_info['pd_amount'];
                    $pay['pay_diff_amount'] += $order_info['order_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'];
                }
            }
        }
        if (isset($order_info['chain_id']) && $order_info['payment_code'] == 'chain') {
            $order_list[0]['order_remind'] = '下单成功，请在' . CHAIN_ORDER_PAYPUT_DAY . '日内前往门店提货，逾期订单将自动取消。';
            $flag_chain = 1;
        }

        //如果线上线下支付金额都为0，转到支付成功页
        if (empty($pay['pay_diff_amount'])) {
            output_error('订单重复支付');
        }

        $payment_list = model('mbpayment')->getMbPaymentOpenList();

        if (!empty($payment_list)) {
            foreach ($payment_list as $k => $value) {
                unset($payment_list[$k]['payment_id']);
                unset($payment_list[$k]['payment_config']);
                unset($payment_list[$k]['payment_state']);
                unset($payment_list[$k]['payment_state_text']);
            }
        }
        if(in_array($this->member_info['client_type'],array('ios','android'))){
            foreach ($payment_list as $k => $value) {
               if(!strpos($payment_list[$k]['payment_code'],'app')){
                   unset($payment_list[$k]);
               }
            }
        }
        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['pay_sn'] = $pay_sn;
        $pay['payed_amount'] = sc_price_format($pay['payed_rcb_amount'] + $pay['payed_pd_amount']);
        unset($pay['payed_pd_amount']);
        unset($pay['payed_rcb_amount']);
        $pay['pay_amount'] = sc_price_format($pay['pay_diff_amount']);
        unset($pay['pay_diff_amount']);
        $pay['member_available_pd'] = sc_price_format($pay['member_available_pd']);
        $pay['member_available_rcb'] = sc_price_format($pay['member_available_rcb']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        output_data(array('pay_info' => $pay));
    }

    /**
     * AJAX验证支付密码
     */
    public function check_pd_pwd()
    {
        if (empty(input('post.password'))) {
            output_error('支付密码格式不正确');
        }
        $buyer_info = model('member')->getMemberInfoByID($this->member_info['member_id'], 'member_paypwd');
        if ($buyer_info['member_paypwd'] != '') {
            if ($buyer_info['member_paypwd'] === md5(input('post.password'))) {
                output_data('1');
            }
        }
        output_error('支付密码验证失败');
    }

    /**
     * F码验证
     */
    public function check_fcode()
    {
        $goods_id = intval(input('post.goods_id'));
        if ($goods_id <= 0) {
            output_error('商品ID格式不正确');
        }
        if (input('post.fcode') == '') {
            output_error('F码格式不正确');
        }
        $result = model('buy','logic')->checkFcode($goods_id, trim(input('post.fcode')));
        if ($result['code']) {
            output_data('1');
        }
        else {
            output_error('F码验证失败');
        }
    }
}