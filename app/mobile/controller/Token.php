<?php

namespace app\mobile\controller;

use think\Lang;

class Token extends MobileMall {
    
    public function _initialize() {
        parent::_initialize();
    }
    
    public function index() {
        $token_id=input('id');
		$model_mb_user_token = model('mbusertoken');
		$mb_user_token=$model_mb_user_token->where('token_id',$token_id)->find(); 

		cookie('username', $mb_user_token['member_name']);
        cookie('key', $mb_user_token['token']);
		
		$this->redirect(WAP_SITE_URL.'/member/member.html?key='.$mb_user_token['token'].'&username='.$mb_user_token['member_name']);return;
    }

}
