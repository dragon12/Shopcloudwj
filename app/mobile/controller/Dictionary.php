<?php
namespace app\mobile\controller;

use support\Dictionary as SupportDictionary;

class Dictionary extends MobileMall
{
    public function query()
    {
        $dict_type = $this->request->param('dict_type', '', 'trim');
        $list = SupportDictionary::get($dict_type);
        output_data($list);
    }
}
