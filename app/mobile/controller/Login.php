<?php

namespace app\mobile\controller;

use think\Lang;
use process\Process;

if (!defined('ABSPATHMDL')) 
{
	define('ABSPATHMDL',dirname(__FILE__).'/');
}

class Login extends MobileMall
{

    public function _initialize()
    {
        parent::_initialize();
        Lang::load(APP_PATH . 'mobile\lang\zh-cn\login.lang.php');
    }

    /**
     * 登录
     */
    public function index()
    {
        $username = input('param.username');
        $password = input('param.password');
        $client = input('param.client');

        if (empty($username) || empty($password) || !in_array($client, $this->client_type_array)) {
            output_error('用户名/密码不能为空');
        }

        $model_member = new \app\common\model\Member();

        $array = array();
        $array["member_name"] = $username;
        $array["member_password"] = md5($password);

        // $array['user'] = $username;
        // $array['pwd'] = $password;

        // // 用户第三方登录
        // try{
        //     $member_info = $model_member->erpLogin($array);
        //     $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $client);
        //     $logindata = array(
        //         'username' => $member_info['member_name'],
        //         'userid' => $member_info['member_id'],
        //         'key' => $token
        //     );
        //     output_data($logindata);
        // } catch (\Exception $e) {
        //     output_error($e->getMessage());
        // }

        $member_info = $model_member->getMemberInfo($array);

        if (empty($member_info) && preg_match('/^0?(13|15|17|18|14|19)[0-9]{9}$/i', $username)) {//根据会员名没找到时查手机号
            $array = array();
            $array['member_mobile'] = $username;
            $array['member_password'] = md5($password);
            $member_info = $model_member->getMemberInfo($array);
        }

        if (empty($member_info) && (strpos($username, '@') > 0)) {//按邮箱和密码查询会员
            $array = array();
            $array['member_email'] = $username;
            $array['member_password'] = md5($password);
            $member_info = $model_member->getMemberInfo($array);
        }

        if (is_array($member_info) && !empty($member_info)) {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $client);
            if ($token) {
                $logindata = array(
                    'username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token
                );
                session('wap_member_info', $logindata);

                /*
                //demotree.vip 登录Wp和OA平台用户 (此处为将服务器作为客户端登录，非客户端用户自身登录)
                $GLOBALS['MlUrl']='http://demo001.demotree.vip';
		            $GLOBALS['WpUrl']='http://demo001.demotree.vip/wp';
		            $GLOBALS['OAUrl']='http://oa.demo001.demotree.vip/roadflow/MApi';
		            //$regjson = '{"username":"'.$username.'","password":"'.$password.'","email":"","client":"'.get_clientip().'"}';
		            //$wprst = get_post_json($GLOBALS['WpUrl'].'/api-login.php',$regjson);
		            //$oarst = get_post_json($GLOBALS['OAUrl'].'/api_login.aspx',$regjson);
		            $rurl = $GLOBALS['OAUrl'].'/api_loginrd.aspx?username='.urlencode($username).'&password='.$password.'&rdurl='.urlencode($GLOBALS['WpUrl'].'/api-loginrd.php').'&rdurl2='.urlencode($GLOBALS['MlUrl']);
		            header('Location:'.$rurl);
		            //$this->redirect($rurl);
                exit();
                */

                output_data($logindata);
            }
            else {
                output_error('登录失败le');
            }
        }
        else {
            output_error('用户名密码错误');
        }
    }
    public function get_inviter(){
        $inviter_id=intval(input('get.inviter_id'));
        $member=db('member')->where('member_id',$inviter_id)->field('member_id,member_name')->find();
        
        output_data(array('member' => $member));
    }

    /**
     * 注册 重复注册验证
     */
    public function register()
    {
        if (Process::islock('reg')) {
            output_error('您的操作过于频繁，请稍后再试');
        }
        $username = input('param.username');
        
        //demotree.vip 核验手机号
        if (is_numeric($username) && !preg_match('/^0?(13|15|17|18|14|19)[0-9]{9}$/i', $username))
        {
        	output_error('账号格式错误！');
        }
        
        $password = input('param.password');
        dump($password);
        exit;
        $password_confirm = input('param.password_confirm');
//        $email = input('param.email');
        $client = input('param.client');
        $inviter_id = intval(input('param.inviter_id'));

        $model_member = new \app\common\model\Member();
        $register_info = array();
        $register_info['member_name'] = $username;
        $register_info['member_password'] = $password;
        $register_info['password_confirm'] = $password_confirm;
//        $register_info['email'] = $email;
        //添加奖励积分 v3-b12
        if($inviter_id){
            $register_info['inviter_id'] = $inviter_id;
        }else{
            $register_info['inviter_id'] = intval(base64_decode(cookie('uid'))) / 1;
        }

        $member_info = $model_member->register($register_info);
        if (!isset($member_info['error'])) {
            process::addprocess('reg');
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $client);
            if ($token) {
            	  /*
            	  //demotree.vip 登录Wp和OA平台用户
                $GLOBALS['MlUrl']='http://demo001.demotree.vip';
		            $GLOBALS['WpUrl']='http://demo001.demotree.vip/wp';
		            $GLOBALS['OAUrl']='http://oa.demo001.demotree.vip/roadflow/MApi';
		            //$regjson = '{"username":"'.$username.'","password":"'.$password.'","email":"","client":"'.get_clientip().'"}';
		            //$wprst = get_post_json($GLOBALS['WpUrl'].'/api-login.php',$regjson);
		            //$oarst = get_post_json($GLOBALS['OAUrl'].'/api_login.aspx',$regjson);
		            $rurl = $GLOBALS['OAUrl'].'/api_loginrd.aspx?username='.urlencode($username).'&password='.$password.'&rdurl='.urlencode($GLOBALS['WpUrl'].'/api-loginrd.php').'&rdurl2='.urlencode($GLOBALS['MlUrl']);
		            header('Location:'.$rurl);
		            //$this->redirect($rurl);
                exit();
            	  */
                output_data(array(
                                'username' => $member_info['member_name'], 'userid' => $member_info['member_id'],
                                'key' => $token
                            ));
            }
            else {
                output_error('注册失败');
            }
        }
        else {
            output_error($member_info['error']);
        }
    }

    /**
     * 会员名称检测
     *
     * @param
     * @return
     */
    public function check_member() {
        $member_name = input('param.member_name');
        $member_mobile = input('param.member_mobile');
        $member_model = model('member');
        if(!$member_mobile && !$member_mobile){
            output_error('请输入用户名 或 手机号');
        }
        if($member_name) {
            $check_member_name = $member_model->getMemberInfo(array('member_name' => $member_name));
            if($check_member_name) output_error('用户名已被占用');
        }
        if($member_mobile) {
            $check_member_mobile = $member_model->getMemberInfo(array('member_mobile' => $member_mobile));
            if($check_member_mobile) output_error('手机号已被占用');
        }

        output_data('success');
    }
}