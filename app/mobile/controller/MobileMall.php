<?php

namespace app\mobile\controller;

class MobileMall extends MobileHome {

    protected $member_info = [];

    public function _initialize() {
        parent::_initialize();
        $this->getMemberIdIfExists();
    }

    protected function getMemberIdIfExists() {
        $key = $this->token;

        $model_mb_user_token = model('mbusertoken');
        $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($key);
        if (empty($mb_user_token_info)) {
            return 0;
        }

        $this->member_info or $this->member_info = (new \app\common\model\Member())->getMemberInfoByID($mb_user_token_info['member_id']);

        return $mb_user_token_info['member_id'];
    }

}

?>
