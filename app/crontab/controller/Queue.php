<?php
/**
 * 队列
 */
namespace app\crontab\controller;

use support\Request;
use think\Db;

class Queue extends BaseCron {

    /**
     * 默认方法
     */
    public function index(){
        // 查询队列
        $list = Db::name('queue')->where(['state' => 0])->limit(50)->order('id asc')->select();

        if(!$list) return;

        //当前缓存类型为本地文件,则直接执行
        $QueueLogic = new \app\common\logic\Queue();
        // 队列执行
        foreach ($list as $item) {
            try {
                $QueueLogic->{$item['action']}(unserialize($item['params']));
                Db::name('queue')->where(['id' => $item['id']])->update(['state' => 2]);
            } catch (\Exception $e) {
                Db::name('queue')->where(['id' => $item['id']])->update(['state' => 1]);
                // 记录日志
                log_write('queue', $item['id'].' - '.$e->getMessage());
            }
        }

        $this->index();
    }

    public function test() {

        $list = Db::name('order')->where('order_sn', '111')->select();
    }
    
}