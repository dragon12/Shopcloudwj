<?php
//获取URL访问的ROOT地址 网站的相对路径
define('BASE_SITE_ROOT', str_replace('/index.php', '', \think\Request::instance()->root()));
define('PLUGINS_SITE_ROOT', BASE_SITE_ROOT.'/static/plugins');
define('ADMIN_SITE_ROOT', BASE_SITE_ROOT.'/static/admin');
define('HOME_SITE_ROOT', BASE_SITE_ROOT.'/static/home');

define("REWRITE_MODEL", true); // 设置伪静态
if (!REWRITE_MODEL) {
    define('BASE_SITE_URL', \think\Request::instance()->domain() . \think\Request::instance()->baseFile());
} else {
    // 系统开启伪静态
    if (!defined('BASE_SITE_ROOT')) {
        define('BASE_SITE_URL', \think\Request::instance()->domain());
    } else {
        define('BASE_SITE_URL', \think\Request::instance()->domain() . \think\Request::instance()->root());
    }
}

//检测是否安装 DSSHOP 系统
if(file_exists("install/") && !file_exists("install/install.lock")){
    header('Location: '.BASE_SITE_ROOT.'/install/install.php');
    exit();
}
//error_reporting(E_ALL ^ E_NOTICE);//显示除去 E_NOTICE 之外的所有错误信息


//引用语言包的类型 针对于前端模板 中文\英文
if (in_array(think\Cookie::get('sc_home_lang'), array('zh-cn', 'en-us'))) {
    think\Config::set('default_lang', think\Cookie::get('sc_home_lang'));
}


//define('BASE_SITE_URL', BASE_SITE_URL);
define('HOME_SITE_URL', BASE_SITE_URL.'/home');
define('ADMIN_SITE_URL', BASE_SITE_URL.'/admin');
define('PUB_SITE_URL', BASE_SITE_URL.'/pub');
define('MOBILE_SITE_URL', BASE_SITE_URL.'/mobile');
define('WAP_SITE_URL', str_replace('/index.php', '', BASE_SITE_URL).'/wap');
define('UPLOAD_SITE_URL',str_replace('/index.php', '', BASE_SITE_URL).'/uploads');
define('EXAMPLES_SITE_URL',str_replace('/index.php', '', BASE_SITE_URL).'/examples');
define('SESSION_EXPIRE',3600);

define('PUBLIC_PATH',ROOT_PATH.'public');
define('PLUGINS_PATH',ROOT_PATH.'plugins');
define('BASE_DATA_PATH',PUBLIC_PATH.'/data');
define('TEMP_DOWNLOAD_PATH',RUNTIME_PATH.'download');
define('BASE_UPLOAD_PATH',PUBLIC_PATH.'/uploads');

define('TIMESTAMP',time());
define('DIR_HOME','home');
define('DIR_ADMIN','admin');
define('DIR_MOBILE','mobile');
define('DIR_WAP','wap');

define('DIR_UPLOAD','public/uploads');

define('ATTACH_PATH','home');
define('ATTACH_COMMON',ATTACH_PATH.'/common');
define('ATTACH_AVATAR',ATTACH_PATH.'/avatar');
define('ATTACH_INVITER',ATTACH_PATH.'/inviter');
define('ATTACH_EDITOR',ATTACH_PATH.'/editor');
define('ATTACH_MEMBERTAG',ATTACH_PATH.'/membertag');
define('ATTACH_GOODS',ATTACH_PATH.'/store/goods');
define('ATTACH_LOGIN',ATTACH_PATH.'/login');
define('ATTACH_WAYBILL',ATTACH_PATH.'/waybill');
define('ATTACH_ARTICLE',ATTACH_PATH.'/article');
define('ATTACH_BRAND',ATTACH_PATH.'/brand');
define('ATTACH_COUNTRY',ATTACH_PATH.'/country');
define('ATTACH_GOODS_CLASS',ATTACH_PATH.'/goods_class');
define('ATTACH_ADV',ATTACH_PATH.'/adv');
define('ATTACH_APPADV',ATTACH_PATH.'/appadv');
define('ATTACH_ACTIVITY',ATTACH_PATH.'/activity');
define('ATTACH_WATERMARK',ATTACH_PATH.'/watermark');
define('ATTACH_POINTPROD',ATTACH_PATH.'/pointprod');
define('ATTACH_GROUPBUY',ATTACH_PATH.'/groupbuy');
define('ATTACH_VOUCHER',ATTACH_PATH.'/voucher');
define('ATTACH_MOBILE','mobile');
define('ATTACH_MALBUM',ATTACH_PATH.'/member');
define('TPL_SHOP_NAME','default');
define('TPL_ADMIN_NAME', 'default');
define('TPL_DELIVERY_NAME', 'default');
define('TPL_MEMBER_NAME', 'default');

define('DEFAULT_CONNECT_SMS_TIME', 60);//倒计时时间

define('MD5_KEY', 'a2382918dbb49c8643f19bc3ab90ecf9');
define('CHARSET','UTF-8');
define('ALLOW_IMG_EXT','jpg,png,gif,bmp,jpeg');#上传图片后缀
define('ALLOW_VIDEO_EXT','avi,wmv,mpeg,mp4,m4v,mov,asf,flv,f4v,rmvb,rm,3gp,vob');#上传图片后缀

define('HTTP_TYPE',  \think\Request::instance()->isSsl() ? 'https://' : 'http://');#是否为SSL


//默认颜色规格id(前台显示图片的规格)
define('DEFAULT_SPEC_COLOR_ID', 1);


/**
 * 店铺相册图片规格形式, 处理的图片包含 商品图片以
 */
define('GOODS_IMAGES_WIDTH', '240,480,1280');
define('GOODS_IMAGES_HEIGHT', '240,480,1280');
define('GOODS_IMAGES_EXT', '_240,_480,_1280');

/**
 * 通用图片生成规格形式
 */
define('COMMON_IMAGES_EXT', '_small,_normal,_big');

/**
 * 发货类型配置
 */
define('TRADE_NORMAL', 1); // 国内发货
define('TRADE_BBC', 2); // 保税区发货
define('TRADE_BC', 3); // 国外发货

// 非国内发货类型
\think\Config::set('trade_not_internal', [
    TRADE_BBC,
    TRADE_BC
]);

\think\Config::set('trade_mode', [
    TRADE_NORMAL => '一般贸易',
    TRADE_BBC => '保税仓发货',
    TRADE_BC => '跨境直邮',
]);

//贸易方式编码
\think\Config::set('trade_mode_code', [
    TRADE_BBC => '1210',
    TRADE_BC=> '9610',
]);

/**
 *  订单状态
 */
//已取消
define('ORDER_STATE_CANCEL', 0);
//已产生但未支付
define('ORDER_STATE_NEW', 10);
//已支付
define('ORDER_STATE_PAY', 20);
//已发货
define('ORDER_STATE_SEND', 30);
//已收货，交易成功
define('ORDER_STATE_SUCCESS', 40);
//默认未删除
define('ORDER_DEL_STATE_DEFAULT', 0);
//已删除
define('ORDER_DEL_STATE_DELETE', 1);
//彻底删除
define('ORDER_DEL_STATE_DROP', 2);
//订单结束后可评论时间，15天，60*60*24*15
define('ORDER_EVALUATE_TIME', 1296000);
//抢购订单状态
define('OFFLINE_ORDER_CANCEL_TIME', 3);//单位为天


// 退款退货类型
define('REFUND_REFUND', 1);  // 退款
define('REFUND_RETURN', 2);  // 退货

// 退款退货处理状态
define('REFUND_PROCESS_SUBMIT', 1);  // 待处理
define('REFUND_PROCESS_RETURN', 2);  // 退货中
define('REFUND_PROCESS_PROCESS', 3);  // 处理中
define('REFUND_PROCESS_SUCCESS', 4);  // 已完成

define('REFUND_STATE_SUBMIT', 1);  // 待处理
define('REFUND_STATE_PASS', 2);  // 同意
define('REFUND_STATE_REJECT', 3);  // 拒绝

/**
 * 订单购买类型
 */
define('ORDER_BUY_SELF', 1);       // 销售
define('ORDER_BUY_SALE', 2);       // 自购

// 订单佣金类型
define('COMMISSION_SHARE', 1);  // 销售佣金
define('COMMISSION_RETURN', 2);  // 自购返还
define('COMMISSION_INVITER', 3);  // 代理佣金

// 订单佣金结算状态
define('COMMISSION_STATE_NEW', 10);  // 未支付
define('COMMISSION_STATE_CALC', 20);  // 已支付
define('COMMISSION_STATE_ACTIVE', 30);  // 待结算
define('COMMISSION_STATE_SUCCESS', 40);  // 已结算
define('COMMISSION_STATE_INVALID', 50);  // 失效


/**
 * 用户类型
 */
define('MEMBER_TYPE_1', 1); // 一级代理商
define('MEMBER_TYPE_2', 2); // 二级代理商
define('MEMBER_TYPE_3', 3); // 三级代理商

/**
 * 默认店铺
 */
define('DEFAULT_AGENT', 10);
define('DEFAULT_STORE', 10112);

/**
 * 会员组
 */
define('MEMBER_AGENT_GROUP', 1);    // 代理商
define('MEMBER_AREA_GROUP', 2);
define('MEMBER_DOCTOR_GROUP', 3);
define('MEMBER_CUSTOMER_GROUP', 9); // 客户

/**
 * 会员组级别
 */
define('MEMBER_GROUP_LEVEL_1', 1); // 一级用户
define('MEMBER_GROUP_LEVEL_2', 2); // 二级用户

/**
 * 会员启用状态
 */
define('MEMBER_REVIEW_STATE_0', 0); // 未审核
define('MEMBER_REVIEW_STATE_1', 1); // 待审核
define('MEMBER_REVIEW_STATE_2', 2); // 未通过审核
define('MEMBER_REVIEW_STATE_3', 3); // 已通过审核

/**
 * 会员的开启状态
 */
define('MEMBER_STATE_0', 0); // 0为关闭
define('MEMBER_STATE_1', 1); // 1为开启

/**
 * 店铺状态
 */
define('INVITER_STATE_REVIEW', 0);  // 未审核
define('INVITER_STATE_PASS', 1);    // 通过
define('INVITER_STATE_REJECT', 2);  // 未通过

/**
 * 仓库下发状态
 */
define('WMS_STATE_NOT_ISSUED', 1);  // 未下发
define('WMS_STATE_ISSUED', 2);    // 已下发
define('WMS_STATE_FAIL', 3);  // 下发失败
define('WMS_STATE_QUERY', 4);    // 状态回执获取中
define('WMS_STATE_SEND', 5);    // 已发货

/**
 * 单一窗口下发状态
 */
define('CUSTOMS_STATE_NOT_ISSUED', 1);  // 未下发
define('CUSTOMS_STATE_ISSUED', 2);    // 已下发
define('CUSTOMS_STATE_FAIL', 3);  // 下发失败
define('CUSTOMS_STATE_ISSUE_SUCCESS', 4);  // 下发成功
define('CUSTOMS_STATE_SUCCESS', 5);  // 新增申报成功

// 通联支付单推送状态
define('ALLINPAY_STATE_NOT_ISSUED', 1);  // 未下发
define('ALLINPAY_STATE_ISSUED', 2);  // 已下发
define('ALLINPAY_STATE_FAIL', 3);  // 下发失败
define('ALLINPAY_STATE_NULL', 4);  // 无需下发

// 179号文推送状态
define('PLATDATA_STATE_NEW', 0);  // 推送数据未整理
define('PLATDATA_STATE_UNSIGNED', 1);  // 未加签
define('PLATDATA_STATE_NOT_ISSUED', 2);  // 已加签
define('PLATDATA_STATE_ISSUED', 3);  // 已下发
define('PLATDATA_STATE_FAIL', 4);  // 下发失败

// 短信类型配置
define('SMS_SIGN_UP', 1);  // 注册认证短信
define('SMS_SIGN_IN', 2);  // 登录认证短信
define('SMS_RESET_PASSWD', 3);  // 找回密码认证短信
define('SMS_BIND', 4);  // 绑定手机认证短信
define('SMS_AUTH', 5);  // 认证短信

// 已支付订单状态
\think\Config::set('order_paid_states', [ORDER_STATE_PAY, ORDER_STATE_SEND, ORDER_STATE_SUCCESS]);

\think\Config::set('inviter_state', [
    INVITER_STATE_REVIEW    => '待审核',
    INVITER_STATE_PASS      => '通过',
    INVITER_STATE_REJECT    => '未通过',
]);

\think\Config::set('kuaidi100', [
    'key'    => '',
    'customer' => '',
]);

// 加载万嘉仓库相关配置
\think\Config::set('wms_config', [
    'PRODUCT_RECORD_NO'     => '100000000',       // 总局商品备案号
    'COMPANY_NAME'          => '',
    'COMPANY_CODE'          => '',
    'EBC_NAME'              => '',
    'EBC_CODE'              => '',
    'LOGIS_COMPANY_NAME'    => '青岛万嘉通商外贸综合服务有限公司',
    'LOGIS_COMPANY_CODE'    => '370210190000018359',
]);

// 加载单一窗口相关配置
\think\Config::set('customs_config', [
    'uploadCerNo' => '',          // 179号文上传订单证书编号
    'payCode'     => '312228034T',        // 支付企业备案代码
    'payName'     => '通联支付网络服务股份有限公司',    //  支付企业备案名称

    'recpCode'     => '',        // 应填写收款企业代码（境内企业为统一社会信用代码；境外企业可不填写）
    'recpName'     => '',        // 应填写收款企业名称

    'ebpCode'     => '',        // 电商平台的海关注册登记编号或统一社会信用代码。
    'ebpName'     => '',  // 电商平台的登记名称。
    'ebcCode'     => '',        // 电商企业的海关注册登记编号或统一社会信用代码，对应清单的收发货人。
    'ebcName'     => '',  // 电商企业的登记名称，对应清单的收发货人。
    'copCode'     => '',        // 报文传输的企业代码（需要与接入客户端的企业身份一致）
    'copName'     => '',  // 报文传输的企业名称
    'dxpId'       => '',       // 向中国电子口岸数据中心申请数据交换平台的用户编号
    'loginName'   => '',                // 登录单一窗口用户名
    'loginPassword' => '',              // 登录单一窗口密码
    'pfxPath'       => EXTEND_PATH.'ordersync/cert/pro.pfx',              // 单一窗口密钥（测试与正式不同，上线时也需要替换）
    'pfxPassword'   => "",          // 私钥密码为123456

    /************ 测试 ***********/
//    'loginName'   => 'songwen',                // 登录单一窗口用户名
//    'loginPassword' => 'songwen_123',              // 登录单一窗口密码
//    'pfxPath'       => EXTEND_PATH.'ordersync/cert/test.pfx',              // 单一窗口密钥（测试与正式不同，上线时也需要替换）
]);
