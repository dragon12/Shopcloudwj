<?php


namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use app\crontab\controller\Minutes;

class Minutescommand extends Command{

    protected function configure()
    {
        $this->setName('Minutescommand')->setDescription('分计划任务');
    }

    protected function execute(Input $input, Output $output)
    {
        // $output->writeln("已经执行计划任务");
        $minutes = new Minutes();
        $minutes->index();
    }


}