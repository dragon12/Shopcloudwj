<?php

namespace app\admin\controller;

use app\common\model\Platdataopen;
use think\Lang;

class Chinaport extends AdminControl {

    public function _initialize() {
        parent::_initialize();
    }

    /**
     * 管理员列表
     */
    public function index() {

        $this->setAdminCurItem('index');
        return $this->fetch('index');
    }

    /**
     * 查询待签名订单数据
     */
    public function orderlist() {
        $list = \think\Db::name('platdataopen')->where([
            'upload_state' => PLATDATA_STATE_UNSIGNED,
        ])->order('create_time asc')->limit('20')->select();

        foreach ($list as $key => $item) {
            $upload_data = json_decode($item['upload_data'], true);
            $data = [];
            $data[] = '"sessionID":"'.$item['session_id'].'"';
            $data[] = '"payExchangeInfoHead":"'.json_encode($upload_data['payExchangeInfoHead'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES). '"';
            $data[] = '"payExchangeInfoLists":"'.json_encode($upload_data['payExchangeInfoLists'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES). '"';
            $list[$key]['sign_data'] = $data;
        }

        $this->success($list);
    }

    // 保存签名信息
    public function save() {
        $id = $this->request->param('id', 0, 'intval');
        $sign_data = $this->request->param('sign_data', '', 'trim');
        $service_time = $this->request->param('service_time', '', 'trim');

        $platdataopen = Platdataopen::get($id);
        $platdataopen->save([
            'service_time' => $service_time,
            'sign_data' => $sign_data,
            'upload_state' => PLATDATA_STATE_NOT_ISSUED,
        ]);

        $this->success('success');
    }

    /**
     * 获取卖家栏目列表,针对控制器下的栏目
     */
    protected function getAdminItemList() {
        $menu_array = array(
            array(
                'name' => 'index',
                'text' => '数据签名',
                'url' => url('chinaport/index')
            ),
        );
        return $menu_array;
    }

}

?>
