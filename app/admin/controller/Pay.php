<?php

namespace app\admin\controller;

use think\Controller;


// 第三方支付请求类
class Pay extends Controller {

    // 配置信息
    private $config = [];
    // 用户数据
    private $paydata = [];


    public function test(){
        $this->pay(["amount"=>100,"order"=>"123","authcode"=>"qwer"],"allinpay");
    }


    // 请求第三方支付公共方法，根据传入值寻找具体的方法
    // $payData 用户交易数据(支付订单ID、金额、单号、支付授权码如微信,支付宝,银联的付款二维码)
    // $payTypeCode 支付方式代码
    public function pay($payData,$payTypeCode){
        // 是否存在此支付方式
        $pay_info = model("payment")->getPaymentOpenInfo(["payment_code"=>$payTypeCode]);
        if(!$pay_info){
            return sc_callback(10001,"支付方式不存在");
        }
        // 是否存在配置信息
        if(!unserialize($pay_info["payment_config"])){
            return sc_callback(10001,"支付方式未配置");
        }
        $this->config = unserialize($pay_info["payment_config"]);
        $this->paydata = $payData;
        return $this->$payTypeCode();
    }


    // 通联支付
    public function allinpay(){
        // 参数
        $data = [
            "cusid" => $this->config["allinpay_mch_id"],   // 商户号
            "appid" => $this->config["allinpay_appid"],    // appid
            "version" => "11",                             // 版本号
            "randomstr" => $this->getNonceStr(16),
            "trxamt" => bcmul($this->paydata["amount"],100),  // 金额(分)
            "reqsn" => $this->paydata["order"],            // 单号
            "authcode" => $this->paydata["authcode"],      // 支付授权码
            "key" => $this->config["allinpay_key"],        // key
        ];

        // 参数组装
        ksort($data);
        $data["sign"] = strtoupper(md5(http_build_query($data)));

        // 请求url,以后换成正式地址
        $url = "https://test.allinpaygd.com/apiweb/unitorder/scanqrpay";

        // 记录支付请求
        \think\Db::name('orderpay')->where(['pay_id' => $this->paydata["pay_id"]])->update([
            'request_data' => json_encode([
                'url'   => $url,
                'params_str' => $data
            ])
        ]);

        // 请求
        $res = $this->curlPost($url,$data);
        $res_sort = json_decode($res,true);
        if($res_sort && $res_sort["retcode"]=="SUCCESS"){
            if($res_sort["trxstatus"]=="0000"){
                // 因为没有异步回调，所以成功之后直接修改orderpay
                \think\Db::name('orderpay')->where(['pay_id' => $this->paydata["pay_id"]])->update([
                    'response_data' => $res,"api_paystate"=>1]);
                return sc_callback(10000,"支付成功");
            }else{
                return sc_callback(10001,$res_sort["errmsg"]);
            }
            return sc_callback(10001,$res_sort["retmsg"]);
        }else{
            return sc_callback(10001,$res_sort["retmsg"]);
        }

    }


    // 随机字符串
    public function getNonceStr($length = 32){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }


    // curl post请求
    public function curlPost($url,$post){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,[]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


}


?>