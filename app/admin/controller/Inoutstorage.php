<?php


namespace app\admin\controller;


class Inoutstorage extends AdminControl {

    public function index(){
        $list_rows = "10";
        $page = input("get.page");
        $list = db("goods_storage_log")->where(["log_type"=>["in","1,2"]])->field("admin_name,log_type,goods_name,erp_storage,goods_storage,order_sn,content,create_time")->order("create_time desc")->paginate($list_rows);
        $this->assign([
            "list" => $list,
        ]);
        return $this->fetch();
    }

}