<?php


namespace app\admin\controller;


class Storage extends AdminControl{

    public function index(){

        $list_rows = "10";
        $page = input("get.page");
        $where = [];
        !empty(input("param.order_sn")) && $where["order_sn"] = input("param.order_sn");
        $list = db("goods_storage_log")->where($where)->field("admin_name,log_type,goods_name,erp_storage,goods_storage,order_sn,content,create_time")->order("create_time desc,log_id desc")->paginate($list_rows);
        $this->assign([
            "list" => $list,
        ]);
        return $this->fetch();
    }


}