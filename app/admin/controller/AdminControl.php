<?php

namespace app\admin\controller;

use think\Controller;

class AdminControl extends Controller {

    /**
     * 管理员资料 name id group
     */
    protected $admin_info;

    protected $permission;
    public function _initialize() {
        $this->admin_info = $this->systemLogin();
        $config_list = rkcache('config', true);
        // 将配置加载到config中
        config($config_list);

        //引用语言包的类型 针对于前端模板 中文\英文
//        if (in_array(cookie('sc_admin_lang'), array('zh-cn', 'en-us'))) {
//            config('default_lang', cookie('sc_admin_lang'));
//        }

        //引用语言包的类型 针对于数据库读写类型 中文\英文
        if (in_array(cookie('sc_admin_sql_lang'), array('zh-cn', 'en-us'))) {
            config('default_sql_lang', cookie('sc_admin_sql_lang'));
        }else{
            config('default_sql_lang', 'zh-cn');
        }


        if ($this->admin_info['admin_id'] != 1) {
            // 验证权限
            $this->checkPermission();
        }

        $this->setMenuList();
    }

    /**
     *  排序 排序字段为list_orders数组 POST 排序字段为：list_order
     */
    protected function listOrders($model)
    {
        if (!is_object($model)) {
            return false;
        }

        $pk  = $model->getPk(); //获取主键名称
        $ids = $this->request->post("list_orders/a");

        if (!empty($ids)) {
            foreach ($ids as $key => $r) {
                $data['list_order'] = $r;
                $model->where([$pk => $key])->update($data);
            }

        }

        return true;
    }

    /**
     * 取得当前管理员信息
     *
     * @param
     * @return 数组类型的返回结果
     */
    protected final function getAdminInfo() {
        return $this->admin_info;
    }

    /**
     * 系统后台登录验证
     *
     * @param
     * @return array 数组类型的返回结果
     */
    protected final function systemLogin() {
        $admin_info = array(
            'admin_id' => session('admin_id'),
            'admin_name' => session('admin_name'),
            'admin_gid' => session('admin_gid'),
            'admin_is_super' => session('admin_is_super'),
        );
        if (empty($admin_info['admin_id']) || empty($admin_info['admin_name']) || !isset($admin_info['admin_gid']) || !isset($admin_info['admin_is_super'])) {
            session(null);
            $this->redirect('Admin/Login/index');
        }

        return $admin_info;
    }

    public function setMenuList() {
        $menu_list = $this->menuList();

        $menu_list=$this->parseMenu($menu_list);

        $this->assign('menu_list', $menu_list);
    }

    /**
     * 验证当前管理员权限是否可以进行操作
     *
     * @param string $link_nav
     * @return
     */
    protected final function checkPermission($link_nav = null){
        if ($this->admin_info['admin_is_super'] == 1) return true;

        $controller = request()->controller();
        $action = request()->action();

        if (empty($this->permission)){

            $admin_model=model('admin');
            $gadmin = $admin_model->getOneGadmin(array('gid'=>$this->admin_info['admin_gid']));

            $permission = sc_decrypt($gadmin['glimits'],MD5_KEY.md5($gadmin['gname']));
            $this->permission = $permission = explode('|',$permission);
        }else{
            $permission = $this->permission;
        }

        //显示隐藏小导航，成功与否都直接返回
        if (is_array($link_nav)){
            if (!in_array("{$link_nav['controller']}.{$link_nav['action']}",$permission) && !in_array($link_nav['controller'],$permission)){
                return false;
            }else{
                return true;
            }
        }
        //以下几项不需要验证
        $tmp = array('Index','Dashboard','Login');
        if (in_array($controller,$tmp)){
            return true;
        }
        if (in_array($controller,$permission) || in_array("$controller.$action",$permission)){
            return true;
        }else{
            $extlimit = array('ajax','export_step1');
            if (in_array($action,$extlimit) && (in_array($controller,$permission) || strpos(serialize($permission),'"'.$controller.'.'))){
                return true;
            }
            //带前缀的都通过
            foreach ($permission as $v) {
                if (!empty($v) && strpos("$controller.$action",$v.'_') !== false) {
                    return true;break;
                }
            }
        }
        $this->error(lang('sc_assign_right'),'Dashboard/welcome');
    }

    /**
     * 过滤掉无权查看的菜单
     *
     * @param array $menu
     * @return array
     */
    private final function parseMenu($menu = array()) {
        if ($this->admin_info['admin_is_super'] == 1) {
            return $menu;
        }
        foreach ($menu as $k => $v) {
            foreach ($v['children'] as $ck => $cv) {
                $tmp = explode(',', $cv['args']);
                // 以下几项不需要验证
                $except = array('Index', 'Dashboard', 'Login');
                if (in_array($tmp[1], $except))
                    continue;
                if (!in_array($tmp[1], array_values($this->permission))) {
                    unset($menu[$k]['children'][$ck]);
                }
            }
            if (empty($menu[$k]['children'])) {
                unset($menu[$k]);
                unset($menu[$k]['children']);
            }
        }
        return $menu;
    }

    /**
     * 记录系统日志
     *
     * @param $lang 日志语言包
     * @param $state 1成功0失败null不出现成功失败提示
     * @param $admin_name
     * @param $admin_id
     */
    protected final function log($lang = '', $state = 1, $admin_name = '', $admin_id = 0) {
        if ($admin_name == '') {
            $admin_name = session('admin_name');
            $admin_id = session('admin_id');
        }
        $data = array();
        if (is_null($state)) {
            $state = null;
        } else {
            $state = $state ? '' : lang('sc_fail');
        }
        $data['adminlog_content'] = $lang . $state;
        $data['adminlog_time'] = TIMESTAMP;
        $data['admin_name'] = $admin_name;
        $data['admin_id'] = $admin_id;
        $data['adminlog_ip'] = request()->ip();
        $data['adminlog_url'] = request()->controller() . '&' . request()->action();

        $adminlog_model=model('adminlog');
        return $adminlog_model->addAdminlog($data);
    }

    /**
     * 添加到任务队列
     *
     * @param array $goods_array
     * @param boolean $ifdel 是否删除以原记录
     */
    protected function addcron($data = array(), $ifdel = false) {
        $cron_model = model('cron');
        if (isset($data[0])) { // 批量插入
            $where = array();
            foreach ($data as $k => $v) {
                dump($v);
                if (isset($v['content'])) {
                    $data[$k]['content'] = serialize($v['content']);
                }
                // 删除原纪录条件
                if ($ifdel) {
                    $where[] = '(type = ' . $data['type'] . ' and exeid = ' . $data['exeid'] . ')';
                }
            }
            // 删除原纪录
            if ($ifdel) {
                $cron_model->delCron(implode(',', $where));
            }
            $cron_model->addCronAll($data);
        } else { // 单条插入
            if (isset($data['content'])) {
                $data['content'] = serialize($data['content']);
            }
            // 删除原纪录
            if ($ifdel) {
                $cron_model->delCron(array('type' => $data['type'], 'exeid' => $data['exeid']));
            }
            $cron_model->addCron($data);
        }
    }

    /**
     * 当前选中的栏目
     */
    protected function setAdminCurItem($curitem = '') {
        $this->assign('admin_item', $this->getAdminItemList());
        $this->assign('curitem', $curitem);
    }

    /**
     * 获取卖家栏目列表,针对控制器下的栏目
     */
    protected function getAdminItemList() {
        return array();
    }

    /*
     * 侧边栏列表
     */

    function menuList() {
        return array(
            'dashboard' => array(
                'name' => 'dashboard',
                'text' => lang('sc_dashboard'),

                'children' => array(
                    'welcome' => array(
                        'ico'=>"&#xe649;",
                        'text' => lang('sc_welcome'),
                        'args' => 'welcome,Dashboard,dashboard',
                    ),
                    /*
                    'aboutus' => array(
                        'text' => lang('sc_aboutus'),
                        'args' => 'aboutus,dashboard,dashboard',
                    ),
                     */
                    'config' => array(
                        'ico'=>'&#xe632;',
                        'text' => lang('sc_base'),
                        'args' => 'base,Config,dashboard',
                    ),
                    'member' => array(
                        'ico'=>'&#xe609;',
                        'text' => lang('sc_member_manage'),
                        'args' => 'member,Member,dashboard',
                    ),
                ),
            ),
            'setting' => array(
                'name' => 'setting',
                'text' => lang('sc_set'),
                'children' => array(
                    'config' => array(
                        'ico'=>'&#xe632;',
                        'text' => lang('sc_base'),
                        'args' => 'base,Config,setting',
                    ),
                    //                    'account' => array(
                    //                        'ico'=>'&#xe613;',
                    //                        'text' => lang('sc_account'),
                    //                        'args' => 'qq,Account,setting',
                    //                    ),
                    'upload_set' => array(
                        'ico'=>'&#xe65b;',
                        'text' => lang('sc_upload_set'),
                        'args' => 'default_thumb,Upload,setting',
                    ),
                    'seo' => array(
                        'ico'=>'&#xe632;',
                        'text' => lang('sc_seo_set'),
                        'args' => 'index,Seo,setting',
                    ),
                    'message' => array(
                        'ico'=>'&#xe652;',
                        'text' => lang('sc_message'),
                        'args' => 'email,Message,setting',
                    ),
                    'payment' => array(
                        'ico'=>'&#xe674;',
                        'text' => lang('sc_payment'),
                        'args' => 'index,Payment,setting',
                    ),
                    'admin' => array(
                        'ico'=>'&#xe615;',
                        'text' => lang('sc_admin'),
                        'args' => 'admin,Admin,setting',
                    ),
                    'express' => array(
                        'ico'=>'&#xe640;',
                        'text' => lang('sc_express'),
                        'args' => 'index,Express,setting',
                    ),
                    'waybill' => array(
                        'ico'=>'&#xe654;',
                        'text' => lang('sc_waybill'),
                        'args' => 'index,Waybill,setting',
                    ),
                    'dictionary' => array(
                        'ico'=>'&#xe640;',
                        'text' => '数据字典',
                        'args' => 'index,Dictionary,setting',
                    ),
                    // 'Country' => array(
                    //     'ico'=>'&#xe640;',
                    //     'text' => '国家管理',
                    //     'args' => 'index,Country,setting',
                    // ),
                    'Region' => array(
                        'ico'=>'&#xe640;',
                        'text' => lang('sc_region'),
                        'args' => 'index,Region,setting',
                    ),
                    'db' => array(
                        'ico'=>'&#xe644;',
                        'text' => lang('sc_db'),
                        'args' => 'db,Db,setting',
                    ),
                    'admin_log' => array(
                        'ico'=>'&#xe654;',
                        'text' => lang('sc_adminlog'),
                        'args' => 'loglist,Adminlog,setting',
                    ),
                ),
            ),
            'member' => array(
                'name' => 'member',
                'text' => lang('sc_member'),
                'children' => array(
                    'member' => array(
                        'ico'=>'&#xe609;',
                        'text' => lang('sc_member_manage'),
                        'args' => 'member,Member,member',
                    ),
                    //                    'membergrade' => array(
                    //                        'ico'=>'&#xe61d;',
                    //                        'text' => lang('sc_membergrade'),
                    //                        'args' => 'index,Membergrade,member',
                    //                    ),
                    //                    'exppoints' => array(
                    //                        'ico'=>'&#xe645;',
                    //                        'text' => lang('sc_exppoints'),
                    //                        'args' => 'index,Exppoints,member',
                    //                    ),
                    'notice' => array(
                        'ico'=>'&#xe652;',
                        'text' => lang('sc_notice'),
                        'args' => 'index,Notice,member',
                    ),
                    'member_review' => array( // 待启用会员
                         'ico'=>'&#xe654;',
                         'text' => '待启用会员',
                         'args' => 'member_review,Member,member',
                    ),
                    'member_group_setting' => array( // 会员组设置
                         'ico'=>'&#xe654;',
                         'text' => lang('sc_member_group_setting'),
                         'args' => 'member_group,Membergrade,member',
                    ),
//                    'points' => array(
//                        'ico'=>'&#xe644;',
//                        'text' => lang('sc_points'),
//                        'args' => 'index,Points,member',
//                    ),
                    //                    'predeposit' => array(
                    //                        'ico'=>'&#xe671;',
                    //                        'text' => lang('sc_predeposit'),
                    //                        'args' => 'pdrecharge_list,Predeposit,member',
                    //                    ),
                    //                    'test' => array(
                    //                        'ico'=>'&#xe671;',
                    //                        'text' => '计划任务测试',
                    //                        'args' => 'test,Predeposit,member',
                    //                    ),
                ),
            ),
//             'shop' => array(          // 商家管理
//                                       'name' => 'merchant',
//                                       'text' => '代理商管理',
//                                       'children' => array(
//                                           'shop_list' => array(
//                                               'text' => '代理商管理',
//                                               'args' => 'index,Shop,merchant',
//                                           ),
//                                           'message' => array(
//                                               'ico'=>'&#xe652;',
//                                               'text' => lang('sc_message'),
//                                               'args' => 'index,Notice,merchant',
//                                           ),
// //                                          'shop_review' => array(
// //                                              'text' => '未激活代理商',
// //                                              'args' => 'member_inactive,Shop,merchant',
// //                                          ),
//                                           'points_log' => array(
//                                               'text' => '佣金记录',
//                                               'args' => 'pointsLog,Shop,merchant',
//                                           ),
//                                           'predeposit' => array(
//                                               'ico'=>'&#xe671;',
//                                               'text' => '提现/资金明细',
//                                               'args' => 'pdcash_list,Predeposit,merchant',
//                                           ),
//                                       ),
//             ),
            'goods' => array(
                'name' => 'goods',
                'text' => lang('sc_goods'),
                'children' => array(
                    'goodsclass' => array(
                        'ico'=>'&#xe661;',
                        'text' => lang('sc_goodsclass'),
                        'args' => 'goods_class,Goodsclass,goods',
                    ),
                    'Brand' => array(
                        'ico'=>'&#xe66c;',
                        'text' => lang('sc_brand'),
                        'args' => 'index,Brand,goods',
                    ),
                    'Goods' => array(
                        'ico'=>'&#xe661;',
                        'text' => lang('sc_goods_manage'),
                        'args' => 'index,Goods,goods',
                    ),
                    'Storage' => array(
                        'ico'=>'&#xe661;',
                        'text' => lang('sc_goods_storage'),
                        'args' => 'index,Storage,goods',
                    ),
                    'Inoutstorage' => array(
                        'ico'=>'&#xe661;',
                        'text' => lang('sc_goods_inoutstorage'),
                        'args' => 'index,Inoutstorage,goods',
                    ),
                    'Type' => array(
                        'ico'=>'&#xe659;',
                        'text' => lang('sc_type'),
                        'args' => 'index,Type,goods',
                    ),
                    'Spec' => array(
                        'ico'=>'&#xe653;',
                        'text' => lang('sc_spec'),
                        'args' => 'index,Spec,goods',
                    ),
                    'Arrivalnotice' => array(
                        'ico'=>'&#xe661;',
                        'text' => lang('sc_arrivalnotice'),
                        'args' => 'index,Arrivalnotice,goods',
                    ),
                    'transport' => array(
                        'ico'=>'&#xe655;',
                        'text' => lang('sc_transport'),
                        'args' => 'index,Transport,goods',
                    ),
                    'album' => array(
                        'ico'=>'&#xe65b;',
                        'text' => lang('sc_album'),
                        'args' => 'index,GoodsAlbum,goods',
                    ),
                ),
            ),
            'trade' => array(
                'name' => 'trade',
                'text' => lang('sc_trade'),
                'children' => array(
                    'deliver' => array(
                        'ico'=>'&#xe640;',
                        'text' => lang('sc_deliver'),
                        'args' => 'index,Deliver,trade',
                    ),
                    'order' => array(
                        'ico'=>'&#xe631;',
                        'text' => lang('sc_order'),
                        'args' => 'index,Order,trade',
                    ),
                    'offline_order' => array(
                        'ico'=>'&#xe607;',
                        'text' => '线下订单',
                        'args' => 'offline_order,Order,trade',
                    ),
                    //                    'vrorder' => array(
                    //                        'ico'=>'&#xe654;',
                    //                        'text' => lang('sc_vrorder'),
                    //                        'args' => 'index,Vrorder,trade',
                    //                    ),
                    'refund' => array(
                        'ico'=>'&#xe624;',
                        'text' => lang('sc_refund'),
                        'args' => 'refund_manage,Refund,trade',
                    ),
                    'return' => array(
                        'ico'=>'&#xe642;',
                        'text' => lang('sc_return'),
                        'args' => 'return_manage,Returnmanage,trade',
                    ),
                    //                    'vrrefund' => array(
                    //                        'ico'=>'&#xe624;',
                    //                        'text' => lang('sc_vrrefund'),
                    //                        'args' => 'refund_manage,Vrrefund,trade',
                    //                    ),
                    //                    'consulting' => array(
                    //                        'ico'=>'&#xe670;',
                    //                        'text' => lang('sc_consulting'),
                    //                        'args' => 'Consulting,Consulting,trade',
                    //                    ),
                    //                    'inform' => array(
                    //                        'ico'=>'&#xe66f;',
                    //                        'text' => lang('sc_inform'),
                    //                        'args' => 'inform_list,Inform,trade',
                    //                    ),
                    //                    'evaluate' => array(
                    //                        'ico'=>'&#xe641;',
                    //                        'text' => lang('sc_evaluate'),
                    //                        'args' => 'evalgoods_list,Evaluate,trade',
                    //                    ),
                    'deliverset' => array(
                        'ico'=>'&#xe640;',
                        'text' => '发货设置',
                        'args' => 'index,Deliverset,trade',
                    ),
                    //                    'complaint' => array(
                    //                        'ico'=>'&#xe655;',
                    //                        'text' => '订单投诉',
                    //                        'args' => 'index,complaint,trade',
                    //                    ),
                    //                    'transport' => array(
                    //                        'ico'=>'&#xe655;',
                    //                        'text' => '售卖区域',
                    //                        'args' => 'index,Transport,trade',
                    //                    ),
                ),
            ),
            'website' => array(
                'name' => 'website',
                'text' => lang('sc_website'),
                'children' => array(
//                    'Articleclass' => array(
//                        'ico'=>'&#xe604;',
//                        'text' => lang('sc_articleclass'),
//                        'args' => 'index,Articleclass,website',
//                    ),
//                    'Article' => array(
//                        'ico'=>'&#xe65b;',
//                        'text' => lang('sc_article'),
//                        'args' => 'index,Article,website',
//                    ),
//                    'Document' => array(
//                        'text' => lang('sc_document'),
//                        'args' => 'index,Document,website',
//                    ),
//                    'Navigation' => array(
//                        'text' => lang('sc_navigation'),
//                        'args' => 'index,Navigation,website',
//                    ),
                    'Adv' => array(
                        'text' => lang('sc_adv'),
                        'args' => 'ap_manage,Adv,website',
                    ),
//                    'Link' => array(
//                        'ico'=>'&#xe617;',
//                        'text' => lang('sc_link'),
//                        'args' => 'index,Link,website',
//                    ),
//                    'Footlink' => array(
//                        'ico'=>'&#xe617;',
//                        'text' => 'PC底部链接',
//                        'args' => 'index,Footlink,website',
//                    ),
//                    'Mallconsult' => array(
//                        'ico'=>'&#xe66d;',
//                        'text' => lang('sc_shop_consult'),
//                        'args' => 'index,Mallconsult,website',
//                    ),
//                    'mb_feedback' => array(
//                        'ico'=>'&#xe60f;',
//                        'text' => lang('mb_feedback'),
//                        'args' => 'flist,mbfeedback,website',
//                    ),
                ),
            ),
           'operation' => array(
               'name' => 'operation',
               'text' => lang('sc_operation'),
               'children' => array(
                   'Operation' => array(
                       'ico'=>'&#xe662;',
                       'text' => lang('sc_operation_set'),
                       'args' => 'setting,Operation,operation',
                   ),
//                                        'Inviter' => array(
//                                            'ico'=>'&#xe662;',
//                                            'text' => lang('sc_inviter_set'),
//                                            'args' => 'setting,Inviter,operation',
//                                        ),
                   'Groupbuy' => array(
                       'ico'=>'&#xe668;',
                       'text' => lang('sc_groupbuy'),
                       'args' => 'index,Groupbuy,operation',
                   ),
                   //                    'Vrgroupbuy' => array(
                   //                        'ico'=>'&#xe668;',
                   //                        'text' => lang('sc_groupbuy_vr'),
                   //                        'args' => 'index,Vrgroupbuy,operation',
                   //                    ),
                   'Pintuan' => array(
                       'text' => lang('sc_promotion_pintuan'),
                       'args' => 'index,Promotionpintuan,operation',
                   ),
                   //                    'Xianshi' => array(
                   //                        'text' => lang('sc_promotion_xianshi'),
                   //                        'args' => 'index,Promotionxianshi,operation',
                   //                    ),
                   //                    'Mansong' => array(
                   //                        'text' => lang('sc_mansong'),
                   //                        'args' => 'index,Promotionmansong,operation',
                   //                    ),
                   //                    'Bundling' => array(
                   //                        'text' => lang('sc_promotion_bundling'),
                   //                        'args' => 'index,Promotionbundling,operation',
                   //                    ),
                   //                    'Voucher' => array(
                   //                        'ico'=>'&#xe658;',
                   //                        'text' => lang('sc_voucher_price_manage'),
                   //                        'args' => 'index,Voucher,operation',
                   //                    ),
                   'Activity' => array(
                       'text' => lang('sc_activity_manage'),
                       'args' => 'index,Activity,operation',
                   ),
                   'Pointprod' => array(
                       'ico'=>'&#xe646;',
                       'text' => lang('sc_pointprod'),
                       'args' => 'index,Pointprod,operation',
                   ),
                                      'rechargecard' => array(
                                          'ico'=>'&#xe643;',
                                          'text' => lang('sc_rechargecard'),
                                          'args' => 'index,Rechargecard,operation',
                                      ),
               ),
           ),
//            'stat' => array(
//                'name' => 'stat',
//                'text' => lang('sc_stat'),
//                'children' => array(
//                    'stat_general' => array(
//                        'ico'=>'&#xe662;',
//                        'text' => lang('sc_statgeneral'),
//                        'args' => 'general,Statgeneral,stat',
//                    ),
//                    'stat_industry' => array(
//                        'text' => lang('sc_statindustry'),
//                        'args' => 'scale,Statindustry,stat',
//                    ),
//                    'stat_member' => array(
//                        'ico'=>'&#xe665;',
//                        'text' => lang('sc_statmember'),
//                        'args' => 'newmember,Statmember,stat',
//                    ),
//                    'stat_trade' => array(
//                        'text' => lang('sc_stattrade'),
//                        'args' => 'predeposit,Stattrade,stat',
//                    ),
//                    'stat_goods' => array(
//                        'ico'=>'&#xe661;',
//                        'text' => lang('sc_statgoods'),
//                        'args' => 'pricerange,Statgoods,stat',
//                    ),
//                    'stat_marketing' => array(
//                        'text' => lang('sc_statmarketing'),
//                        'args' => 'promotion,Statmarketing,stat',
//                    ),
//                    'stat_stataftersale' => array(
//                        'text' => lang('sc_stataftersale'),
//                        'args' => 'refund,Stataftersale,stat',
//                    ),
//                ),
//            ),
//                        'mobile' => array(
//                            'name' => 'mobile',
//                            'text' => lang('mobile'),
//                            'children' => array(
//                                'mb_category_list' => array(
//                                    'text' => lang('mb_category_list'),
//                                    'args' => 'mb_category_list,mbcategorypic,mobile',
//                                ),
//                                'mb_feedback' => array(
//                                    'ico'=>'&#xe60f;',
//                                    'text' => lang('mb_feedback'),
//                                    'args' => 'flist,mbfeedback,mobile',
//                                ),
//                                'app_appadv' => array(
//                                    'text' => lang('appadv'),
//                                    'args' => 'index,Appadv,mobile',
//                                ),
//                            ),
//                        ),
            'wechat' => array(
                'name' => 'wechat',
                'text' => lang('wechat'),
                'children' => array(
                    'wechat_setting' => array(
                        'ico'=>'&#xe632;',
                        'text' => lang('wechat'),
                        'args' => 'setting,Wechat,wechat',
                    ),
                    'wechat_menu' => array(
                        'ico'=>'&#xe614;',
                        'text' => lang('wechat_menu'),
                        'args' => 'menu,Wechat,wechat',
                    ),
//                    'wechat_keywords' => array(
//                        'ico'=>'&#xe610;',
//                        'text' => lang('wechat_keywords'),
//                        'args' => 'k_text,Wechat,wechat',
//                    ),
//                    'wechat_member' => array(
//                        'ico'=>'&#xe65a;',
//                        'text' => lang('wechat_member'),
//                        'args' => 'member,Wechat,wechat',
//                    ),
//                    'wechat_push' => array(
//                        'ico'=>'&#xe652;',
//                        'text' => lang('wechat_push'),
//                        'args' => 'SendList,Wechat,wechat',
//                    ),
                ),
            ),

            'customs' => array(
                'name' => 'customs',
                'text' => '关务',
                'children' => array(
                    'chinaport' => array(
                        'text' => '数据签名',
                        'args' => 'index,chinaport,customs',
                    ),
                ),
            ),
            //
            // 'cron' => array(
            //     'name' => 'crontab',
            //     'text' => '计划任务',
            //     'children' => array(
            //         'daycron' => array(
            //             'text' => '日计划任务',
            //             'args' => 'index,Crontab,cron',
            //         ),
            //         'hourcron' => array(
            //             'text' => '时计划任务',
            //             'args' => 'hourcron,Crontab,cron',
            //         ),
            //         'minutecron' => array(
            //             'text' => '分计划任务',
            //             'args' => 'minutecron,Crontab,cron',
            //         ),
            //     ),
            // ),
        );
    }

    /*
     * 权限选择列表
     */

    function limitList() {
        $_limit = array(
            array('name' => lang('sc_set'), 'child' => array(
                array('name' => lang('sc_base'), 'action' => null, 'controller' => 'Config'),
                array('name' => lang('sc_account'), 'action' => null, 'controller' => 'Account'),
                array('name' => lang('sc_upload_set'), 'action' => null, 'controller' => 'Upload'),
                array('name' => lang('sc_seo_set'), 'action' => null, 'controller' => 'Seo'),
                array('name' => lang('sc_payment'), 'action' => null, 'controller' => 'Payment'),
                array('name' => lang('sc_message'), 'action' => null, 'controller' => 'Message'),
                array('name' => lang('sc_express'), 'action' => null, 'controller' => 'Express'),
                array('name' => lang('sc_waybill'), 'action' => null, 'controller' => 'Waybill'),
                array('name' => lang('sc_region'), 'action' => null, 'controller' => 'Region'),
                array('name' => lang('sc_adminlog'), 'action' => null, 'controller' => 'Adminlog'),
            )),
            array('name' => lang('sc_goods'), 'child' => array(
                array('name' => lang('sc_goods_manage'), 'action' => null, 'controller' => 'Goods'),
                array('name' => lang('sc_goodsclass'), 'action' => null, 'controller' => 'Goodsclass'),
                array('name' => lang('sc_goods_storage'), 'action' => null, 'controller' => 'Storage'),
                array('name' => lang('sc_goods_inoutstorage'), 'action' => null, 'controller' => 'Inoutstorage'),
                array('name' => lang('sc_brand'), 'action' => null, 'controller' => 'Brand'),
                array('name' => lang('sc_type'), 'action' => null, 'controller' => 'Type'),
                array('name' => lang('sc_spec'), 'action' => null, 'controller' => 'Spec'),
                array('name' => lang('sc_album'), 'action' => null, 'controller' => 'GoodsAlbum'),
            )),
            array('name' => lang('sc_member'), 'child' => array(
                array('name' => lang('sc_member_manage'), 'action' => null, 'controller' => 'Member'),
                array('name' => lang('sc_membergrade'), 'action' => null, 'controller' => 'Membergrade'),
                array('name' => lang('sc_exppoints'), 'action' => null, 'controller' => 'Exppoints'),
                array('name' => lang('sc_points'), 'action' => null, 'controller' => 'Points'),
                array('name' => lang('sc_predeposit'), 'action' => null, 'controller' => 'Predeposit'),
                array('name' => lang('sc_chatlog'), 'action' => null, 'controller' => 'Chatlog'),
            )),
            array('name' => lang('sc_trade'), 'child' => array(
                array('name' => lang('sc_order'), 'action' => null, 'controller' => 'Order'),
                array('name' => lang('sc_vrorder'), 'action' => null, 'controller' => 'Vrorder'),
                array('name' => lang('sc_refund'), 'action' => null, 'controller' => 'Refund'),
                array('name' => lang('sc_return'), 'action' => null, 'controller' => 'Returnmanage'),
                array('name' => lang('sc_vrrefund'), 'action' => null, 'controller' => 'Vrrefund'),
                array('name' => lang('sc_consulting'), 'action' => null, 'controller' => 'Consulting'),
                array('name' => lang('sc_inform'), 'action' => null, 'controller' => 'Inform'),
                array('name' => lang('sc_evaluate'), 'action' => null, 'controller' => 'Evaluate'),
                array('name' => '发货设置', 'action' => null, 'controller' => 'Deliverset'),
            )),
            array('name' => lang('sc_website'), 'child' => array(
                array('name' => lang('sc_articleclass'), 'action' => null, 'controller' => 'Articleclass'),
                array('name' => lang('sc_article'), 'action' => null, 'controller' => 'Article'),
                array('name' => lang('sc_document'), 'action' => null, 'controller' => 'Document'),
                array('name' => lang('sc_navigation'), 'action' => null, 'controller' => 'Navigation'),
                array('name' => lang('sc_adv'), 'action' => null, 'controller' => 'Adv'),
                array('name' => lang('sc_link'), 'action' => null, 'controller' => 'Link'),
                array('name' => 'PC底部链接', 'action' => null, 'controller' => 'Footlink'),
            )),
            array('name' => lang('sc_operation'), 'child' => array(
                array('name' => lang('sc_operation_set'), 'action' => null, 'controller' => 'Operation'),
                array('name' => lang('sc_groupbuy'), 'action' => null, 'controller' => 'Groupbuy'),
                array('name' => lang('sc_groupbuy_vr'), 'action' => null, 'controller' => 'Vrgroupbuy'),
                array('name' => lang('sc_activity_manage'), 'action' => null, 'controller' => 'Activity'),
                array('name' => lang('sc_promotion_xianshi'), 'action' => null, 'controller' => 'Promotionxianshi'),
                array('name' => lang('sc_mansong'), 'action' => null, 'controller' => 'Promotionmansong'),
                array('name' => lang('sc_promotion_bundling'), 'action' => null, 'controller' => 'Promotionbundling'),
                array('name' => lang('sc_pointprod'), 'action' => null, 'controller' => 'Pointprod|Pointorder'),
                array('name' => lang('sc_voucher_price_manage'), 'action' => null, 'controller' => 'Voucher'),
                array('name' => lang('sc_activity_manage'), 'action' => null, 'controller' => 'Vrbill'),
                array('name' => lang('sc_shop_consult'), 'action' => null, 'controller' => 'Mallconsult'),
                array('name' => lang('sc_rechargecard'), 'action' => null, 'controller' => 'Rechargecard'),
            )),
            array('name' => lang('sc_stat'), 'child' => array(
                array('name' => lang('sc_statgeneral'), 'action' => null, 'controller' => 'Statgeneral'),
                array('name' => lang('sc_statindustry'), 'action' => null, 'controller' => 'Statindustry'),
                array('name' => lang('sc_statmember'), 'action' => null, 'controller' => 'Statmember'),
                array('name' => lang('sc_stattrade'), 'action' => null, 'controller' => 'Stattrade'),
                array('name' => lang('sc_statgoods'), 'action' => null, 'controller' => 'Statgoods'),
                array('name' => lang('sc_statmarketing'), 'action' => null, 'controller' => 'Statmarketing'),
                array('name' => lang('sc_stataftersale'), 'action' => null, 'controller' => 'Stataftersale'),
            )),
            // array('name' => "计划任务", 'child' => array(
            //     array('name' => lang('sc_statgeneral'), 'action' => null, 'controller' => 'Crontab'),
            //
            // )),
        );

        return $_limit;
    }

}

?>
