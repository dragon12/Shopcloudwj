<?php

/**
 * 地区设置
 */

namespace app\admin\controller;

use think\Lang;

class Country extends AdminControl {

    public function _initialize() {
        parent::_initialize();
        Lang::load(APP_PATH . 'admin/lang/' . config('default_lang') . '/country.lang.php');
        $this->_Country_mod = model('Country');
        define('MAX_LAYER', 4);
    }

    public function index() {
        $region_list = $this->_Country_mod->getCountryList(0);
        $this->assign('region_list', $region_list);
        $this->setAdminCurItem('index');
        return $this->fetch();
    }

//    function ajax_cate() {
//        $cate_id = input('param.id');
//        if (empty($cate_id)) {
//            return;
//        }
//
//        $cate = $this->_Country_mod->get_list($cate_id);
//        foreach ($cate as $key => $val) {
//            $child = $this->_Country_mod->get_list($val['area_id']);
//
//            if (!$child || empty($child)) {
//                $cate[$key]['switchs'] = 0;
//            } else {
//                $cate[$key]['switchs'] = 1;
//            }
//        }
//        echo json_encode(array_values($cate));
//        return;
//    }

    /**
     * ajax操作
     */
    public function ajax() {
        switch (input('param.branch')) {
            /**
             * 更新国家
             */
            case 'country_name':
                $area_model = model('country');
                $where = array('country_id' => intval(input('get.id')));
                $update_array = array();
                $update_array['country_name'] = trim(input('get.value'));
                $area_model->editCountry($update_array, $where);
                echo 'true';
                exit;

                break;
            /**
             *  排序 编码 设置
             */
            case 'country_order':
                $area_model = model('country');
                $where = array('country_id' => intval(input('get.id')));
                $update_array = array();
                $update_array['country_order'] = trim(input('get.value'));
                $area_model->editCountry($update_array, $where);

                \areacache::deleteCacheFile();
                \areacache::updateAreaPhp();
                \areacache::updateAreaArrayJs();
                echo 'true';
                exit;

            case 'country_code':
                $area_model = model('country');
                $where = array('country_id' => intval(input('get.id')));
                $update_array = array();
                $update_array['country_code'] = trim(input('get.value'));
                $area_model->editCountry($update_array, $where);

                \areacache::deleteCacheFile();
                \areacache::updateAreaArrayJs();
                \areacache::updateAreaPhp();
                echo 'true';
                exit;

//            case 'area_index_show':
//                $area_model = model('area');
//                $where = array('area_id' => intval(input('get.id')));
//                $update_array = array();
//                $update_array[input('get.column')] = input('get.value');
//                $area_model->editArea($update_array, $where);
//
//                \areacache::deleteCacheFile();
//                \areacache::updateAreaArrayJs();
//                \areacache::updateAreaPhp();
//                echo 'true';
//                exit;
//                break;
            /**
             * 添加、修改操作中 检测类别名称是否有重复
             */
            case 'check_class_name':
                $area_model = model('country');
                $condition['country_name'] = trim(input('param.country_name'));
//                $condition['area_parent_id'] = intval(input('param.area_parent_id'));
                $condition['country_id'] = array('neq', intval(input('param.country_id')));
                $class_list = $area_model->getCountryList($condition);
                if (empty($class_list)) {
                    echo 'true';
                    exit;
                } else {
                    echo 'false';
                    exit;
                }
                break;
        }
    }

    public function add() {
        if (!request()->isPost()) {
            $country = array(
                'country_id' => input('param.country_id'),
            );
            $this->assign('country', $country);
            return $this->fetch('form');
        } else {
            $country_mod = model('country');
            $data = array(
                'country_name' => input('post.country_name'),
                'country_code' => input('post.country_code'),
                'country_order' => input('post.country_order'),
            );
            //上传图片
            if ($_FILES['country_icon']['name'] != '') {
                $file = request()->file('country_icon');
                $file_name = date('YmdHis') . rand(10000, 99999);
                $upload_file = BASE_UPLOAD_PATH . DS . DIR_ADMIN . DS . 'country';
                $result = $file->validate(['ext' => ALLOW_IMG_EXT])->move($upload_file, $file_name);
                if ($result) {
                    $data['country_icon'] = $result->getFilename();
                }
            }
            //验证数据  BEGIN
            $region_validate = validate('country');
            if (!$region_validate->scene('add')->check($data)) {
                $this->error($region_validate->getError());
            }
            //验证数据  END

            $result = $country_mod->addCountry($data);
            if ($result) {
                \areacache::deleteCacheFile();
                \areacache::updateAreaArrayJs();
                \areacache::updateAreaPhp();
                scLayerOpenSuccess(lang('sc_common_save_succ'));
            } else {
                $this->error(lang('sc_common_save_fail'));
            }
        }
    }

    public function edit() {
        $country_id = intval(input('param.country_id'));
        if ($country_id <= 0) {
            $this->error(lang('param_error'));
        }
        $country_mod = model('country');
        if (!request()->isPost()) {
            $country = $country_mod->getCountryInfo(array('country_id' => $country_id));
            $this->assign('country', $country);
            return $this->fetch('form');
        } else {
            $country = $country_mod->getCountryInfo(array('country_id' => $country_id));
            $data = array(
                'country_name' => input('post.country_name'),
                'country_code' => input('post.country_code'),
                'country_order'=>  input('post.country_order'),
                'country_icon' => input('post.country_icon'),
            );
            //上传图片
            if ($_FILES['country_icon']['name'] != '') {
                $file = request()->file('country_icon');
                $file_name = date('YmdHis') . rand(10000, 99999);
                $upload_file = BASE_UPLOAD_PATH . DS . DIR_ADMIN . DS . 'country';
                $result = $file->validate(['ext' => ALLOW_IMG_EXT])->move($upload_file, $file_name);
                if ($result) {
                    $data['country_icon'] = $result->getFilename();
                    //删除原有友情链接图片
                    @unlink($upload_file . DS . $country['country_icon']);
                }
            }

            //验证数据  BEGIN
            $region_validate = validate('country');
            if (!$region_validate->scene('edit')->check($data)) {
                $this->error($region_validate->getError());
            }
            //验证数据  END
            $result = $country_mod->editCountry($data, array('country_id' => $country_id));
            if ($result >= 0) {
                \areacache::deleteCacheFile();
                \areacache::updateAreaArrayJs();
                \areacache::updateAreaPhp();
                scLayerOpenSuccess(lang('sc_common_op_succ'));
            } else {
                $this->error(lang('sc_common_op_fail'));
            }
        }
    }

    //国家删除
    public function drop() {
        $area_id = input('param.country_id');
        if (empty($area_id)) {
            $this->error(lang('param_error'));
        }
        //判断此分类下是否有子分类
        $area_mod = model('country');
        $result = $area_mod->delCountry(array('country_id'=>$area_id));
        if ($result) {
            sc_json_encode(10000, lang('sc_common_op_succ'));
        } else {
            sc_json_encode(10001, lang('error'));
        }
    }

    /* 取得可以作为上级的地区分类数据 */

    function _get_options($except = NULL) {
        $area = $this->_Region_mod->get_list();
        if (empty($area)) {
            return;
        }
        $tree = new \mall\Tree();
        $tree->setTree($area, 'area_id', 'area_parent_id', 'area_name');
        return $tree->getOptions(MAX_LAYER - 1, 0, $except);
    }

    protected function getAdminItemList() {
        $menu_array = array(
            array(
                'name' => 'index',
                'text' => '管理',
                'url' => url('Country/index')
            ),
        );

        if (request()->action() == 'add' || request()->action() == 'index') {
            $menu_array[] = array(
                'name' => 'add',
                'text' => '新增',
                'url' =>"javascript:scLayerOpen('".url('Country/add')."','".lang('sc_add')."')",
            );
        }
        if (request()->action() == 'edit') {
            $menu_array[] = array(
                'name' => 'edit',
                'text' => '编辑',
                'url' => 'javascript:void(0)'
            );
        }
        return $menu_array;
    }

}
