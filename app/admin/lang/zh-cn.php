<?php

/* 左侧按钮 BEGIN */
//设置
$lang['sc_dashboard'] = '控制台';
$lang['sc_welcome'] = '欢迎界面';
$lang['sc_aboutus'] = '关于我们';
$lang['sc_base'] = '站点设置';
$lang['sc_account'] = '账号同步';
$lang['sc_seo_set'] = 'SEO设置';
$lang['sc_upload_set'] = '上传设置';
$lang['sc_message'] = '邮箱短信';
$lang['sc_payment'] = '支付方式';
$lang['sc_admin'] = '权限设置';
$lang['sc_express'] = '快递公司';
$lang['sc_waybill'] = '运单模板';
$lang['sc_region'] = '地区管理';
$lang['sc_cache'] = '清理缓存';
$lang['sc_db'] = '数据备份';
$lang['sc_adminlog'] = '操作日志';
$lang['sc_brand_manage'] = '品牌管理';
$lang['sc_member_group_setting'] = '会员组设置';

//会员
$lang['sc_member'] = '会员';
$lang['sc_member_manage'] = '会员管理';
$lang['sc_membergrade'] = '会员级别';
$lang['sc_exppoints'] = '经验值管理';
$lang['sc_notice'] = '会员通知';
$lang['sc_points'] = '积分管理';
$lang['sc_predeposit'] = '预存款';
$lang['sc_chatlog'] = '聊天记录';
$lang['points_unit'] = '积分';
$lang['sc_active'] = '激活';

//商品
$lang['sc_goods'] = '商品';
$lang['sc_goodsclass'] = '商品分类';
$lang['sc_brand'] = '品牌管理';
$lang['sc_goods_manage'] = '商品管理';
$lang['sc_goods_storage'] = '库存记录';
$lang['sc_goods_inoutstorage'] = '出库入库';
$lang['sc_type'] = '类型管理';
$lang['sc_spec'] = '规格管理';
$lang['sc_arrivalnotice'] = '到货通知';
$lang['sc_transport'] = '地区管理';
$lang['sc_album'] = '空间管理';

//交易
$lang['sc_trade'] = '订单管理';
$lang['sc_order'] = '实物订单';
$lang['sc_deliver'] = '发货管理';
$lang['sc_vrorder'] = '虚拟订单';
$lang['sc_refund'] = '退款管理';
$lang['sc_return'] = '退货管理';
$lang['sc_vrrefund'] = '虚拟订单退款';
$lang['sc_consulting'] = '咨询管理';
$lang['sc_inform'] = '举报管理';
$lang['sc_evaluate'] = '评价管理';

//
$lang['sc_website'] = '网站';
$lang['sc_articleclass'] = '文章分类';
$lang['sc_article'] = '文章管理';
$lang['sc_document'] = '会员协议';
$lang['sc_navigation'] = '导航管理';
$lang['sc_adv'] = '广告管理';
$lang['sc_link'] = '友情链接';
$lang['sc_shop_consult'] = '平台客服';

//营销
$lang['sc_operation'] = '活动';
$lang['sc_operation_set'] = '基本设置';
$lang['sc_inviter_set'] = '分销设置';
$lang['sc_groupbuy'] = '秒杀管理';
$lang['sc_groupbuy_vr'] = '虚拟抢购设置';
$lang['sc_promotion_pintuan'] = '团购管理';
$lang['sc_promotion_mansong'] = '满即送';
$lang['sc_voucher_price_manage'] = '代金券';
$lang['sc_activity_manage'] = '专题管理';
$lang['sc_pointprod'] = '兑换礼品';
$lang['sc_promotion_xianshi'] = '限时折扣';
$lang['sc_promotion_bundling'] = '优惠套装';
$lang['sc_rechargecard'] = '平台充值卡';

//统计
$lang['sc_stat'] = '统计';
$lang['sc_statgeneral'] = '概述及设置';
$lang['sc_statindustry'] = '行业分析';
$lang['sc_statmember'] = '会员统计';
$lang['sc_stattrade'] = '销量分析';
$lang['sc_statgoods'] = '商品分析';
$lang['sc_statmarketing'] = '市场分析';
$lang['sc_stataftersale'] = '售后分析';

//手机端/APP
$lang['mobile'] = '手机端';
$lang['mb_index'] = '首页编辑';
$lang['special_list'] = '专题设置';
$lang['mb_category_list'] = '分类图片设置';
$lang['mb_app'] = '下载设置';
$lang['mb_feedback'] = '意见反馈';
$lang['appadv'] = 'APP广告';
$lang['appadvposition'] = 'APP广告位';

//微信端
$lang['wechat'] = '公众号配置';
$lang['wechat_menu'] = '微信菜单';
$lang['wechat_keywords'] = '关键字回复';
$lang['wechat_member'] = '绑定列表';
$lang['wechat_push'] = '消息推送';

$lang['adv_manage'] = '闲置幻灯片';

//订单管理
$lang['order_manage'] = '订单管理';
$lang['buyer_name'] = '买家';
$lang['buyer_name_mobile'] = '买家联系方式';
$lang['payment'] = '支付方式';
$lang['sc_orders'] = '订单';
$lang['order_number'] = '订单号';
$lang['order_state'] = '订单状态';
$lang['order_state_new'] = '待付款';
$lang['order_state_pay'] = '待发货';
$lang['order_state_send'] = '待收货';
$lang['sc_processing'] = '处理中';
$lang['sc_processed'] = '待处理';
$lang['order_state_success'] = '已完成';
$lang['type'] = '类型';
$lang['pended_payment'] = '已提交，待确认'; //增加
$lang['order_price_from'] = '订单金额';
$lang['cancel_search'] = '撤销检索';
$lang['order_time'] = '下单时间';
$lang['order_total_price'] = '订单总额';
$lang['order_total_transport'] = '运费';
$lang['miss_order_number'] = '缺少订单编号';

$lang['order_state_paid'] = '已付款';
$lang['order_admin_operator'] = '系统管理员';
$lang['order_state_null'] = '无';
$lang['order_handle_history'] = '操作历史';
$lang['order_admin_cancel'] = '未付款，系统管理员取消订单。';
$lang['order_admin_pay'] = '系统管理员确认收款完成。';
$lang['order_confirm_cancel'] = '您确实要取消该订单吗？';
$lang['order_confirm_received'] = '您确定已经收到货款了吗？';
$lang['order_change_received'] = '收到货款';
$lang['order_revise_price'] = '修改价格';
$lang['order_log_cancel'] = '取消订单';
$lang['max_price'] = '单笔最大限额';
$lang['order_quantity_limit'] = '最大购买量';
$lang['goods_max_limit'] = '商品最大购买量';

//订单详情
$lang['order_detail'] = '订单详情';
$lang['order_info'] = '订单信息';
$lang['payment_time'] = '支付时间';
$lang['ship_time'] = '发货时间';
$lang['complate_time'] = '完成时间';
$lang['buyer_message'] = '买家附言';
$lang['consignee_ship_order_info'] = '收货人及发货信息';
$lang['consignee_name'] = '收货人姓名';
$lang['region'] = '所在地区';
$lang['tel_phone'] = '电话号码';
$lang['mob_phone'] = '手机号码';
$lang['address'] = '详细地址';
$lang['ship_method'] = '配送方式';
$lang['ship_code'] = '发货单号';
$lang['product_info'] = '商品信息';
$lang['product_price'] = '单价';
$lang['product_num'] = '数量';
$lang['product_shipping_mfee'] = '免运费';
$lang['sc_promotion'] = '促销活动';
$lang['sc_groupbuy_flag'] = '抢';
$lang['sc_mansong_flag'] = '满';
$lang['sc_mansong'] = '满即送';
$lang['sc_xianshi_flag'] = '折';
$lang['sc_xianshi'] = '限时折扣';
$lang['sc_bundling_flag'] = '组';
$lang['sc_bundling'] = '优惠套装';

$lang['order_refund'] = '退款';
$lang['order_return'] = '退货';
$lang['order_show_at'] = '于';

/**
 * 页面中的常用文字
 */
$lang['sc_select_all'] = '全选';
$lang['sc_ensure_del'] = '您确定要删除吗?';
$lang['sc_ensure_install'] = '您确定要安装吗?';
$lang['sc_ensure_cancel'] = '您确定要取消吗?';
$lang['sc_ensure_disable'] = '您确定要禁用吗?';
$lang['sc_ensure_enable'] = '您确定要启用吗?';
$lang['sc_please_select_item'] = '请选择要操作的对象';
$lang['sc_del_all'] = '批量删除';
$lang['sc_no_record'] = '没有符合条件的记录';
$lang['sc_handle'] = '操作';
$lang['sc_edit_all'] = '批量编辑';
$lang['sc_ok'] = '确定';
$lang['sc_cancel'] = '取消';
$lang['sc_verify'] = '审核';
$lang['sc_display'] = '显示';
$lang['sc_search'] = '搜索';
$lang['sc_submit'] = '提交';
$lang['sc_save'] = '保存';
$lang['sc_confirm_submit'] = '确认提交';
$lang['sc_explain'] = '说明';

$lang['sc_please_choose'] = '请选择';
$lang['sc_class'] = '分类';
$lang['sc_list'] = '列表';
$lang['sc_all'] = '全部';
$lang['sc_sort'] = '排序';
$lang['sc_query'] = '查询';
$lang['sc_view'] = '查看';
$lang['sc_to'] = '至';
$lang['sc_add_sub_class'] = '新增下级';
$lang['sc_manage'] = '管理';
$lang['sc_editable'] = '可编辑';
$lang['sc_year'] = '年';
$lang['sc_month'] = '月';
$lang['sc_day'] = '日';
$lang['sc_hour'] = "小时";
$lang['sc_yuan'] = '元';
$lang['sc_reset'] = '重置';
$lang['sc_set'] = '设置';
$lang['sc_cancel_search'] = '撤销检索';
$lang['sc_recommend'] = '推荐';
$lang['sc_refuse'] = '拒绝';
$lang['sc_pass'] = '通过';
$lang['sc_back'] = '返回';
$lang['sc_recommendclose'] = '取消推荐';
$lang['sc_applylog'] = '申请日志';
$lang['sc_backlist'] = '返回列表';
$lang['sc_detail'] = '详细';
$lang['sc_open'] = '开启';
$lang['sc_close'] = '关闭';
$lang['sc_end'] = '结束';
$lang['sc_explanation'] = '操作提示';
$lang['sc_explanation_tip'] = '提示相关设置操作时应注意的要点';
$lang['sc_explanation_close'] = '收起提示';
$lang['sc_state'] = '状态';
$lang['sc_normal'] = '正常';
$lang['sc_invalidation'] = '失效';
$lang['sc_export'] = '导出';
$lang['class_sort_explain'] = '数字范围为0~255，数字越小越靠前';
$lang['sc_sort_number'] = '请输入有效的数字';
$lang['sc_url_error'] = '请输入有效的链接';
$lang['sc_quickly_targeted'] = '快捷定位';

$lang['error'] = '在处理您的请求时出现了问题:<br />';
$lang['url_error'] = '必须输入正确格式的网址';

$lang['start_time'] = '开始时间';
$lang['end_time'] = '结束时间';
$lang['homepage'] = '首页';
$lang['miss_argument'] = '缺少参数';
$lang['invalid_request'] = '非法访问';
$lang['param_error'] = '参数错误';
$lang['wrong_checkcode'] = '验证码错误';
$lang['no_record'] = '没有符合条件的记录';
$lang['sc_record'] = '记录';
$lang['currency'] = '￥';
$lang['sc_colon'] = '：';
$lang['piece'] = '件';
$lang['sc_yes'] = '是';
$lang['sc_no'] = '否';
$lang['sc_more'] = '更多';
$lang['sc_default'] = '默认';
$lang['sc_download'] = '下载';
$lang['sc_apply'] = '立即申请';
$lang['sc_documents'] = '相关文档';
$lang['sc_song_typeface'] = '宋体';

//操作提示
$lang['sc_common_op_succ'] = '操作成功';
$lang['sc_common_op_fail'] = '操作失败';
$lang['sc_common_del_succ'] = '删除成功';
$lang['sc_common_del_fail'] = '删除失败';
$lang['sc_common_save_succ'] = '保存成功';
$lang['sc_common_save_fail'] = '保存失败';
$lang['sc_succ'] = '成功';
$lang['sc_fail'] = '失败';
$lang['sc_common_button_save'] = '保存';
$lang['sc_common_loading'] = '加载中...';
$lang['sc_common_button_upload'] = '上传';


$lang['sc_none_input'] = '请输入';
$lang['sc_current_edit'] = '正在编辑';

$lang['sc_add'] = '添加';
$lang['sc_new'] = '新增';
$lang['sc_update'] = '更新';
$lang['sc_edit'] = '编辑';
$lang['sc_del'] = '删除';
$lang['sc_disable'] = '禁用';
$lang['sc_install'] = '安装';
$lang['sc_enable'] = '启用';

$lang['sc_goods_name'] = '商品名称';
$lang['download_lang'] = '数据分段下载';
$lang['sc_assign_right'] = '您不具备进行该操作的权限';
$lang['sc_common_button_confirm'] = '确认';
$lang['sale_price'] = '销售价';
$lang['refund_buyer_add_time'] = '申请时间';
$lang['refund_buyer_message']	= '退款原因';
$lang['return_order_return'] = '退货数量';
$lang['refund_order_refund'] = '退款金额';
$lang['refund_state']	= '审核状态';

$lang['sc_examine'] = '待审核';
$lang['order_state_cancel'] = '已取消';
$lang['state_new'] = '新申请';

//refund
$lang['refund_message'] = '备注信息';
$lang['refund_message_null'] = '备注信息不能为空';
$lang['refund_order_ordersn'] = '订单编号';
$lang['refund_order_refundsn'] = '退款编号';
$lang['refund_order_buyer'] = '买家会员名';

$lang['buyer_message'] = '买家留言';
$lang['tax_rate'] = '税率';
