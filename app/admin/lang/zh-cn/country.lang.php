<?php
$lang['country_name'] = '国家名称';
$lang['country_code'] = '国家编码';
$lang['country_order'] = '国家排序';
$lang['country_name_error'] = '国家名称必填';
$lang['country_order_error'] = '排序必须为数字';
$lang['country_icon'] = '国家图标';


$lang['country_index_help1'] = '对国家作任何更改后，都会直接更新国家缓存';
$lang['country_index_help2'] = '在“国家管理”模块中删除数据时同时将“商家配送范围”关联的数据删除';
