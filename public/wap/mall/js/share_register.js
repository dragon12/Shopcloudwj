var curpage = 1;
var hasmore = true;
var footer = false;
var keyword = getQueryString("keyword");
var gc_id = getQueryString("gc_id");
var myDate = new Date;
var searchTimes = myDate.getTime();
var key = getQueryString("key");
var inviter_id = getQueryString('inviter_id');

// 标记为分享注册
setCookie('share_register', 1);
$(function () {
    if (keyword != "") {
        $("#keyword").html(keyword)
    }
    adv();
    get_list();
});
function get_list() {
    $(".loading").remove();
    if (!hasmore) {
        return false
    }
    hasmore = false;
    param = {};
    param.page = curpage;
    param.pagesize = pagesize;
    if (key != "") {
        param.key = key
    }
    // 加载推荐商品信息
    $.getJSON(ApiUrl + "/goods/goods_list.html", {
        key: getCookie('key'),
        promotion_type_id:1
    }, function (data) {
        // data.result.member_type = member_type;
        console.log(data.result);
        var html = template('goods_list', data);
        $('.item-goods .goods-list').html(html);
    });
}
$(document).on('click','.sctouch-register-bottom',function () {
    pageChange("../member/register_mobile.html?inviter_id=" + inviter_id);
});
function adv() {
    $.ajax({
        url: ApiUrl + "/Openinggiftbag/register_adv",
        type: 'get',
        dataType: 'json',
        success: function(result) {
            console.log();
            var data = result.result;
            var html = '';
            $.each(data,function(k, v) {
                switch (k) {
                    case 'adv_list':
                        $("#main-container1").html(template(k, data));
                        break;
                    default:
                        html += template(k, data);
                }
            });
        }
    });
}