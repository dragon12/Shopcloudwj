function ncScrollLoad() {
    var page,curpage,hasmore,footer,isloading;

    pagesize = 10;
    ncScrollLoad.prototype.loadInit = function(options) {
        var defaults = {
                data:{},
                callback :function(){},
                resulthandle:''
            }
        var options = $.extend({}, defaults, options);
        if (options.iIntervalId) {
            pagesize = options.page>0?options.page : pagesize;
            curpage = 1;
            hasmore = true;
            footer = false;
            isloading = false;
        }
        ncScrollLoad.prototype.getList(options);
        $(window).scroll(function(){
            if (isloading) {//防止scroll重复执行
                return false;
            }
            if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
                isloading = true;
                options.iIntervalId = false;
                ncScrollLoad.prototype.getList(options);
            }
        });
    }

        ncScrollLoad.prototype.getList = function(options){
        console.log(options);
        if (!hasmore) {
            $('.loading').remove();
            ncScrollLoad.prototype.getLoadEnding(options);
            return false;
        }
        param = {};
        //参数
        if(options.getparam){
            param = options.getparam;
        }
        //初始化时延时分页为1
        if(options.iIntervalId){
            param.curpage = 1;
        }
        param.page = curpage;
        param.pagesize = pagesize;
        $.getJSON(options.url, param, function(result){
            checkLogin(result.login);
            $('.loading').remove();
            curpage++;
            var data = result.result;
            //处理返回数据
            if(options.resulthandle){
                eval('data = '+options.resulthandle+'(data);');
            }

            if (!$.isEmptyObject(options.data)) {
                data = $.extend({}, options.data, data);
            }
            var html = template(options.tmplid, data);
            if(options.iIntervalId === false){
                $(options.containerobj).append(html);
            }else{
                $(options.containerobj).html(html);
            }
            hasmore = result.hasmore;
            if (!hasmore) {
                $('.loading').remove();
                //加载底部
                if ($('#footer').length > 0) {
                    ncScrollLoad.prototype.getLoadEnding(options);
                    if (result.page_total == 0) {
                        $('#footer').addClass('posa');
                    }else{
                        $('#footer').removeClass('posa');
                    }
                }
            }
            if (options.callback) {
                options.callback.call('callback');
            }
            isloading = false;
        });
    }

    ncScrollLoad.prototype.getLoadEnding = function(options) {
	    var moreObj = $('.list-more');
	    if(moreObj.length == 0) {
		    moreObj = $('<div class="list-more"></div>');
		    $(options.containerobj).append(moreObj);
	    }
	    moreObj.html('已经没有更多了');
        // if (!footer) {
        //     footer = true;
        //     $.ajax({
        //         url: WapSiteUrl+'/js/footer.js?v2.0.11',
        //         dataType: "script"
        //     });
        // }
    }
}