if(getCookie('sc_mobile_lang')=='en'){
    sc_lang=sc_lang_en;
    if(typeof(sc_lang_en['title'])!='undefined'){
        document.title=sc_lang_en.title;
    }
}
$(function(){
    $('lang').each(function(){
        $(this).replaceWith(sc_lang[$(this).attr('data-id')]);
    });

})
if(typeof(template)!='undefined'){
template.helper("lang",
    function(e) {
        return sc_lang[e];
    });
}
