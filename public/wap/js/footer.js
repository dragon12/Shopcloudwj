$(function() {
    var a = getCookie("key");
    str = '<div class="footer-blank"></div>'
    str += '<div class="footer-nav"><ul>';
    str += '<li class="'+(/mall\/index/.test(location.href) ? 'active' : '')+'"><span data-url="../mall/index.html" class="open-url"><i class="iconfont">&#xe906;</i><span>首页</span></span></li>';
    str += '<li class="'+(/mall\/product_first_categroy/.test(location.href) ? 'active' : '')+'"><span data-url="../mall/product_first_categroy.html" class="open-url" data-name="2"><i class="iconfont">&#xe909;</i><span>分类</span></span></li>';
    str += '<li class="'+(/mall\/detection/.test(location.href) ? 'active' : '')+'"><span data-url="../mall/detection.html?detection=1" class="open-url" data-name="2"><i class="iconfont">&#xe907;</i><span>发现</span></span></li>';
    str += '<li class="'+(/mall\/cart_list/.test(location.href) ? 'active' : '')+'"><span data-url="../mall/cart_list.html" class="open-url"><i class="iconfont">&#xe73e;</i><span>购物车</span><i id="foot_nav_cart"></i></span></li>';
    str += '<li class="'+(/member\/member/.test(location.href) ? 'active' : '')+'"><span data-url="../member/member.html" class="open-url"><i class="iconfont">&#xe905;</i><span>我的</span></span></li></ul></div>';

    $("#footer").html(str);
});