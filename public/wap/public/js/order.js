$(function() {
    window.pageLoad = function () {
        baseApi.get("Commonquery/order_info", {
            code: getQueryString("code"),
        }, function (t) {
            console.log(t);
                t.result = t;
                t.result.order_info.WapSiteUrl = WapSiteUrl;
                $("#order-info-container").html(template("order-info-tmpl", t.result.order_info));
                $('body').on('tap','.goods-refund',function () {
                    c($(this));
                });
                $('body').on('tap','.goods-return',function () {
                    _($(this));
                });
            });
		baseApi.post("/Commonquery/search_deliver.html",  {
			code: getQueryString("code"),
		}, function(r) {
			if (!r) {
				r = {};
				r.err = "商品暂未发货"
			}
			var temp = template("order-delivery-tmpl", r);
			$("#order-delivery").html(temp);
		})
    }

    pageLoad();
    window.pageReload = function () {
        pageLoad();
    };
	
    function e(Obj) {
        var r = Obj.attr("order_id");
        layer.open({
            shadeClose:false,
            content: '确定取消订单？'
            , btn: ['确定','取消']
            , yes: function (index) {
                t(r)
                layer.close(index);
            }
        });
    }
    function t(e) {
        baseApi.post("Memberorder/order_cancel.html", {
            order_id: e
        }, function(r) {
            window.location.reload();
        })
    }
    function o(Obj) {
        var r = Obj.attr("order_id");
        layer.open({
            shadeClose:false,
            content: '确定收到了货物吗？'
            , btn: ['确定','取消']
            , yes: function (index) {
                i(r)
                layer.close(index);
            }
        });
    }
    function i(e) {
        baseApi.post("Memberorder/order_receive.html", {
            order_id: e
        }, function(r) {
            window.location.reload()
        })
    }
    function d(Obj) {
        var r = Obj.attr("order_id");
        pageChange("../member/member_evaluation.html?order_id=" + r)
    }
    function a(Obj) {
        var r = Obj.attr("order_id");
        pageChange("../member/member_evaluation_again.html?order_id=" + r)
    }
    function n(Obj) {
        var r = Obj.attr("order_id");
        pageChange("../member/refund_all.html?order_id=" + r)
    }
    function l(Obj) {
        var r = Obj.attr("order_id");
        pageChange("../member/order_delivery.html?order_id=" + r)
    }
    function c(Obj) {
        var r = Obj.attr("order_id");
        var e = Obj.attr("order_goods_id");
        pageChange("../member/refund.html?order_id=" + r + "&order_goods_id=" + e)
    }
    function _(Obj) {
        var r = Obj.attr("order_id");
        var e = Obj.attr("order_goods_id");
        pageChange("../member/return.html?order_id=" + r + "&order_goods_id=" + e)
    }

    function show_time(timediff){
        timediff = timediff < 0 ? 0 : timediff;
        //获取时间差

        //获取还剩多少小时
        var hour = add0(parseInt(timediff/(60*60)));
        //获取还剩多少分钟
        var minute = add0(parseInt((timediff - hour*60*60)/60));
        //获取还剩多少秒
        var second = add0(timediff - hour*60*60 - minute*60);
        //输出还剩多少时间
        document.getElementById('hour').innerHTML = hour;
        document.getElementById('minute').innerHTML = minute;
        document.getElementById('second').innerHTML = second;

        // 设置定时器
        if(timediff) {
            setTimeout(function () {
                show_time(timediff - 1);
            },1000);
        }
    }
});