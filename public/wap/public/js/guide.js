/**
 * 引导页
 */
!function (e, $, mui) {
    "use strict";
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.skipPage = '../mall/index.html';
		t.config = config;
		t.userInfo = null;
		t.page = {
			page: 0,
			perpage: 0,
			more: true
		};
		t.element = {
			slide:$('#slider')
		};
		// 初始化
		t.init();
		
		// 事件监听
		t.listen();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;

		t.imageList();
	};
	
	// 事件初始化
	pt.listen = function() {
		var t = this;
		$('.guide-skip').on('tap', function () {
			t.skip();
		});
	};

	// 跳过
	pt.skip = function() {
		var t = this;
		// 已引导的版本
		localCache('GUIDE_VERSION', APP_VERSION, 86400 * 30 * 1000);

		clearTimeout(t.timer || 0);
		// 引导页跳过
		if(checkLoginStatus()) {
			// 引导页跳过
			pageChange(this.skipPage, {});
		}
	};

	// 引导页倒计时
	pt.guideTimer = function() {
		var t = this;
		$('.guide-skip').show();
		var s = $('.guide-skip span').text().toInt();
		if(s > 0) {
			$('.guide-skip span').text(s - 1);

			clearTimeout(t.timer || 0);
			t.timer = setTimeout(function () {
				t.guideTimer();
			}, 1000);
		} else {
			t.skip();
		}
	};
	
	// 判断是否需要加载引导页
	pt.guideCheck = function () {
		var t = this;
		var guideVersion = localCache('GUIDE_VERSION');

		if(!guideVersion) {
			// 第一次安装，弹出服务协议和隐私
			layer.open({
				closeBtn: true,
				title: '服务协议和隐私政策',
				area: ['10rem', '10rem'],
				content: '请你务必审慎阅读、充分理解“服务协议”和“隐私政策”各项条款，包括但不限于：为了向你提供即时通讯、内容分享等服务，我们需要收集你的设备信息、操作日志等个人信息，你可以在“设置”中查看、更改、删除个人信息，并管理你的授权。你可以阅读 <a  class="open-url" data-url="../mall/article_show.html?article_id=42">《服务协议》</a> 和 <a  class="open-url" data-url="../mall/article_show.html?article_id=53">《隐私政策》</a> 了解详细信息。如果你同意，请点击“同意”开始接受我们的服务。',
				btn: ['&#12288;同意&#12288;', "暂不使用"],
				yes: function (index) {
					layer.close(index);
					t.guideTimer();
				}
				, no: function (index) {
					plus.runtime.quit();
				}
			});
		} else {
			t.guideTimer();
		}
		
		if(guideVersion && guideVersion >= APP_VERSION) {
			// 跳转到首页
			// pageChange('../mall/index.html');
		}
	}
    
	// 图片加载
	pt.imageList = function() {
		var guideVersion = localCache('GUIDE_VERSION');

		var t = this;
		baseApi.get(
			'index/guide',
			{},
			function (result) {
				// 清空
				t.element.slide.find('.mui-slider-group').html('');
				if(result.length == 0 && guideVersion) {
					t.skip();
					return;
				} else {
					t.guideCheck();
				}
				if(result.length > 0) $('.guide-cover').hide();
				result.forEach(function (item, index) {
					var html = '';
					html = '<div class="mui-slider-item"><div class="item-logo" style="background-image:url('+item.image_url+')"></div></div>';
					var itemObj = $(html);
					t.element.slide.find('.mui-slider-group').append(itemObj);
					
					if(index == result.length - 1) {
						// 添加按钮
						// itemObj.find('.item-logo').html('<span class="mui-btn open-url" data-url="../home/index.html">立即体验</span>');
					}

					html = '<div class="mui-indicator '+(index == 0 ? 'mui-active' : '')+'"></div>';
					t.element.slide.find('.mui-slider-indicator').append(html);
				});
                var gallery = mui('#slider');
				gallery.slider({
					interval:2000//自动轮播周期，若为0则不自动播放，默认为0；
				});
                mui.init({});
                
			});
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
	};

	mui.init({
		keyEventBind: {
			backbutton: true //开启back按键监听
		}
	});

	window.pageReload = function() {
		e.page.skipPage = '../member/member.html';
		e.page.guideTimer();
	};

	// 页面初始化
	mui.plusReady(function () {
		e.page = new p(config);
		var tuichu = "再按一次退出程序";
		var backButtonPress = 0;

		getCurrentWindow().id = 'public/guide';

		plus.navigator.setStatusBarBackground('#3E7FC3');
		
		mui.back = function(event) {
			backButtonPress++;
			if(backButtonPress > 1) {
				var main = plus.android.runtimeMainActivity();
				main.moveTaskToBack(false);
				return false;
			} else {
				plus.nativeUI.toast(tuichu);
			}
			setTimeout(function() {
				backButtonPress = 0;
			}, 1000);
			return false;
		};
	});


}(window, $, mui);