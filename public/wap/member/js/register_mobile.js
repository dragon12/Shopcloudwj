var mobile = 0;
var code = 0;
var wxUserInfo = localCache('reg_wxinfo');
var member_group = 1;
var share_register = getCookie('share_register');
$(function () {
	// 邀请注册隐藏邀请码选项
	if(share_register) {
		$('.inviter-code').hide();
        $('.register_code').hide();
	}

	if(wxUserInfo) {
		$('.register-btn').text('注册并绑定微信');
	}
    logoutSuccess();
    var inviter_id = getQueryString("inviter_id") || getCookie('inviter_id');
    var gb_id = getQueryString('gb_id');
    $("#inviter_id").val(inviter_id || '');
    $("#refreshcode").bind("click", function () {
        loadSeccode()
    });
    $.sValid.init({
        rules: {usermobile: {required: true, mobile: true}},
        messages: {usermobile: {required: "请填写手机号！", mobile: "手机号码不正确"}},
        callback: function (e, i, r) {
            if (e.length > 0) {
                var l = "";
                $.map(i, function (e, i) {
                    l += "<p>" + e + "</p>"
                });
                layer.open({content: l, skin: 'msg', time: 2});
            }
        }
    });

	// // 加载分类
	// dictGet('gc_top', function (result) {
	// 	var html = '';
	// 	result.forEach(function (item) {
	// 		html += '<option value="'+item.key+'">'+item.val+'</option>';
	// 	});
	// 	$('select[name="gc_ids"]').html(html);
	// });
    //
	// // 加载国家
	// dictGet('country', function (result) {
	// 	var html = '';
	// 	result.forEach(function (item) {
	// 		html += '<option value="'+item.key+'">'+item.val+'</option>';
	// 	});
	// 	$('select[name="country_ids"]').html(html);
	// });
    //
	// // 加载自媒体平台
	// dictGet('we_media_platform', function (result) {
	// 	var html = '<option value="">请选择自媒体平台</option>';
	// 	result.forEach(function (item) {
	// 		html += '<option value="'+item.key+'">'+item.val+'</option>';
	// 	});
	// 	$('select[name="we_media_platform"]').html(html);
	// });
    //
	// // 加载粉丝量级
	// dictGet('we_media_fans', function (result) {
	// 	var html = '<option value="">请选择粉丝量级</option>';
	// 	result.forEach(function (item) {
	// 		html += '<option value="'+item.key+'">'+item.val+'</option>';
	// 	});
	// 	$('select[name="we_media_fans"]').html(html);
	// });
    //
	// $('.member_group').change(function () {
	// 	member_group = $('.member_group:checked').val();
	// 	var id = $('.member_group:checked').attr('id');
	// 	$('.optional-item').hide().filter('.'+id).show();
	// }).change();

    loadSeccode();

    $("#refreshcode").bind("click",
        function() {
            loadSeccode()
        });

    $("#again").on('tap',function() {
        var e = $("#usermobile").val();
        var c = $("#captcha").val();
        a = $("#codekey").val();
        if ($.sValid()) {
            send_sms(e, c)
        } else {
            return false
        }
    });
    $("#register_mobile_password").on('tap',function() {
        if (!$(this).parent().hasClass("ok")) {
            return false
        }
        var c = $("#mobilecode").val();
        // var inviter_code = $("#captcha").val();
        // var inviter_code = $("#inviter_id").val();
        // if (inviter_code.length == 0) {
        //     layer.open({content: '请填写邀请码', skin: 'msg', time: 2});
        //     return;
        // }

        if (c.length == 0) {
            layer.open({content: '请填写验证码', skin: 'msg', time: 2});
            return;
        }
        check_sms_captcha($("#usermobile").val(), c);
        return false
    });

    $("#area_info").on("tap", function() {
        $.areaSelected({success: function(a) {
                $("#area_info").val(a.area_info).attr({"data-areaid": a.area_id, "data-areaid2": a.area_id_2 == 0 ? a.area_id_1 : a.area_id_2,"data-province_id": a.area_id_1});
                // change_map(a.area_info);
            }})
    })
    $(".public-pos").on("tap", function() {
        $.addressSelected({success: function(a) {
                $('#latitude').val(a.lat);
                $('#longitude').val(a.lng);
                $('#address').val(a.address);
            }})
    });

});
function send_sms(e) {
    $.getJSON(ApiUrl + "/Connect/get_sms_captcha.html", {
            type: 1,
            phone: e
        },
        function(e) {
            if (e.code == 10000) {
                layer.open({content: '发送成功',skin: 'msg',time: 2});
                $(".code-again").hide();
                $(".code-countdown").show().find("em").html(e.result.sms_time);
                var c = setInterval(function() {
                        var e = $(".code-countdown").find("em");
                        var a = parseInt(e.html() - 1);
                        if (a == 0) {
                            $(".code-again").show();
                            $(".code-countdown").hide();
                            clearInterval(c)
                        } else {
                            e.html(a)
                        }
                    },
                    1e3)
            } else {
                loadSeccode();
                layer.open({content: e.message, skin: 'msg', time: 2});
            }
        })
}

function check_sms_captcha(e, c) {
    $.getJSON(ApiUrl + "/Connect/check_sms_captcha.html", {
            type: 1,
            phone: e,
            captcha: c
        },
        function(a) {
            if (a.code == 10000) {
                // var password = (0.1 + Math.random() * 0.89) * 1000000;

                let member_mobile = $("#usermobile").val();

                // var password  = Math.ceil( Math.random()* 1000000);
                var password  = member_mobile;
                var inviter_id = $("#inviter_id").val();
                //小区业务
                var inviter_name = $('#inviter_name').val();
                var estate_id = $("#estate_id").val().toInt();
	            var city_id = 0;
	            var area_id = 0;
	            var province_id = 0;
	            var member_address = '';
	            var area_info = '';//地区详情

	            if(member_group == 2 && !estate_id) {
	            	mui.toast('请选择小区');
	            	return;
	            }

                var data = {
	                phone: e,
	                captcha: c,
	                password: password,
	                inviter_id:inviter_id,
	                client: "wap",
	                estate_id: estate_id,
	                city_id:city_id,
	                area_id:area_id,
	                province_id:province_id,
	                member_address:member_address,
	                area_info:area_info,
	                inviter_name:inviter_name,
	                member_group:member_group,
	                gc_ids:$('select[name="gc_ids"]').val(),
	                country_ids:$('select[name="country_ids"]').val(),
	                we_media_platform:$('select[name="we_media_platform"]').val(),
	                we_media_fans:$('select[name="we_media_fans"]').val(),
                    we_media_name:$('#inviter_we_media_name').val(),

                    member_name: member_mobile,
                    member_mobile: member_mobile,
                };

                if(wxUserInfo) {
	                data.member_truename = wxUserInfo.nickname;
                    data.member_wxopenid = wxUserInfo.openid;
                    data.member_wxunionid = wxUserInfo.unionid || wxUserInfo.openid;
                    data.member_avatar = wxUserInfo.headimgurl;
                    data.member_wxinfo = wxUserInfo;
                    data.member_sex = wxUserInfo.sex == 2 ? 1 : 0;
                }
                data.lock_screen = true;
                baseApi.post("Connect/sms_register", data, function(result) {

                    // 去除自动登录
	                layer.open({
		                shadeClose: false,
                        content: '恭喜您成功注册万嘉会员，请尽快联系您的邀请人激活您的账户，账户审核期间请耐心等待。',
                        btn: '好的',
		                yes: function (index) {
		                	if(share_register) {
				                // 注册成功后回调，去登录
				                // pageChange('../public/app_download.html');
                                pageChange('../member/register_mobile.html');
			                } else {
				                // 注册成功后回调，去登录
				                // pageChange('../member/login_mobile.html');
                                pageChange('../member/register_mobile.html');
			                }
		                }
	                });
                });
            } else {
                loadSeccode();
                layer.open({content: a.message, skin: 'msg', time: 2});
            }
        })
}
function change_map(name){
    return;
    if(name!=''){
        // map.centerAndZoom(name,16);
        // map.setCurrentCity(name);
        // local.search(name);
//                var point=map.getCenter();
//                document.getElementById("longitude").value = point.lng;
//                document.getElementById("latitude").value = point.lat;
    }

}
