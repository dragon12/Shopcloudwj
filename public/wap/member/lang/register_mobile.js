sc_lang={
    'title':'会员注册',
    'membership_registration':'会员注册',
    'login':'登录',
    'general_registration':'普通注册',
    'mobile_registration':'手机注册',
    'cell_phone_number':'手&nbsp;机&nbsp;号',
    'verification_code':'验&nbsp;证&nbsp;码',
    'agree':'同意',
    'user_registration_protocol':'用户注册协议',
    'get_verification_code':'获取验证码',
    'binding_prompt':'',
    'referee':'推荐人',
};
document.title = sc_lang.title;