/***************** Base Object Extension *******************/
function add0(m){return m<10?'0'+m:m }
function date_format(t)
{
    //t是整数，否则要parseInt转换
    var time = new Date(t);
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y+'-'+add0(m)+'-'+add0(d);
}
function datetime_format(t)
{
    //t是整数，否则要parseInt转换
    var time = new Date(t);
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y+'-'+add0(m)+'-'+add0(d)+' '+add0(h)+':'+add0(mm)+':'+add0(s);
}
function http_build_query (formdata, url) { // eslint-disable-line camelcase
    //  discuss at: http://locutus.io/php/http_build_query/
    // original by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Legaev Andrey
    // improved by: Michael White (http://getsprink.com)
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Brett Zamir (http://brett-zamir.me)
    //  revised by: stag019
    //    input by: Dreamer
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
    //      note 1: If the value is null, key and value are skipped in the
    //      note 1: http_build_query of PHP while in locutus they are not.
    //   example 1: http_build_query({foo: 'bar', php: 'hypertext processor', baz: 'boom', cow: 'milk'}, '', '&amp;')
    //   returns 1: 'foo=bar&amp;php=hypertext+processor&amp;baz=boom&amp;cow=milk'
    //   example 2: http_build_query({'php': 'hypertext processor', 0: 'foo', 1: 'bar', 2: 'baz', 3: 'boom', 'cow': 'milk'}, 'myvar_')
    //   returns 2: 'myvar_0=foo&myvar_1=bar&myvar_2=baz&myvar_3=boom&php=hypertext+processor&cow=milk'

    var value, key, tmp = [], argSeparator = '&';

    var _httpBuildQueryHelper = function (key, val, argSeparator) {
        var k, tmp = [];
        if (val === true) {
            val = '1';
        } else if (val === false) {
            val = '0';
        }
        if (val !== null) {
            if (typeof val === 'object') {
                for (k in val) {
                    if (val[k] !== null) {
                        tmp.push(_httpBuildQueryHelper(key + '[' + k + ']', val[k], argSeparator));
                    }
                }
                return tmp.join(argSeparator);
            } else if (typeof val !== 'function') {
                return encodeURIComponent(key) + '=' + encodeURIComponent(val);
            } else {
                throw new Error('There was an error processing for http_build_query().');
            }
        } else {
            return '';
        }
    };

    if (!argSeparator) {
        argSeparator = '&';
    }
    for (key in formdata) {
        value = formdata[key];
        var query = _httpBuildQueryHelper(key, value, argSeparator);
        if (query !== '') {
            tmp.push(query);
        }
        url = url.replace(new RegExp(key + '=[^&#]*&?', 'ig'), '');
    }

    url && (url = url.replace(/[&?]+$/, ''));
    var paramString = tmp.join(argSeparator);
    var result = !url ? '' : url + (!paramString ? '' : (url.indexOf('?') != -1 ? '&' : '?'));
    result += paramString;
    return result;
}

// Cookie Control
function setCookie(c_name,value,expiredays) {
    var exdate=new Date()
    exdate.setTime(exdate.getTime()+expiredays)
    document.cookie=c_name+ "=" +escape(value)+';path=/'+ ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
}
function getCookie(c_name) {
    if (document.cookie.length>0) {
        c_start=document.cookie.indexOf(c_name + "=")
        if (c_start!=-1)
        {
            c_start=c_start + c_name.length+1
            c_end=document.cookie.indexOf(";",c_start)
            if (c_end==-1) c_end=document.cookie.length
            return unescape(document.cookie.substring(c_start,c_end))
        }
    }
    return ""
};
// 四舍五入
Number.prototype.round = function (n) {
    var digit = Math.pow(10, n);
    return Math.round(this*digit)/digit;
};
// 进位取整
Number.prototype.ceil = function (n) {
    var digit = Math.pow(10, n);
    return Math.ceil(this*digit)/digit;
};
// 退位取整
Number.prototype.floor = function (n) {
    var digit = Math.pow(10, n);
    return Math.floor(this*digit)/digit;
}
// 去除前后空格
String.prototype.trim = function() {
    return this.replace( /(^\s*)|(\s*$)/g, '');
}
// 字符串转化为整型
String.prototype.toInt = function() {
    var num = parseInt(this);
    return isNaN(num) ? 0 : num;
}
// 字符串转化为浮点型
String.prototype.toFloat = function() {
    var num = parseFloat(this);
    return isNaN(num) ? 0 : num;
}
// 字符串截取替换
String.prototype.strCut = function(len, replaceWith) {
    var string = this;
    return string.length > len ? string.substr(0, len - 3) + (replaceWith || '...') : string;
}
// 首字母大写
String.prototype.ucFirst = function() {
    var string = this;
    return string[0].toUpperCase() + string.substr(1);
}
// 替换全部（ 仿 php preg_replace ）
String.prototype.replaceAll = function(from, to) {
    var string = this;
    for(var i in from) {
        var regexp = new RegExp(from[i], 'g');
        string = string.replace(regexp, to[i]);
    }
    return string;
}

// 删除其中一项（,隔开）
String.prototype.remove = function (value) {
    var separator = arguments[1] || ',';
    var dataArray = this.split(separator);

    var index = dataArray.indexOf(value);
    if(index >= 0) {
        dataArray.splice(index, 1);
    }

    return dataArray.join(separator);
}

// 字符格式化
String.prototype.number_format = Number.prototype.number_format = function (decimals, decPoint, thousandsSep) {
    var number = this;
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec);
};
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
$(function() {
    var controller = $('#search ul.tab').attr('sctype');

    $('#search ul.tab li span').eq(0).html('商品');
    $('#search-form').attr("action", HOMESITEURL + "/Search/index.html");

    $('#search').hover(function() {
        $('#search ul.tab li').eq(1).show();
        $('#search ul.tab li i').addClass('over').removeClass('arrow');
    }, function() {
        $('#search ul.tab li').eq(1).hide();
        $('#search ul.tab li i').addClass('arrow').removeClass('over');
    });
    $('#search ul.tab li').eq(1).click(function() {
        $(this).hide();
        $('#keyword').attr('placeholder', '请输入您要搜索的商品关键字');
        $('#search ul.tab li span').eq(0).html('商品');
        $('#search-form').attr("action", HOMESITEURL+"/Search/index.html");

        $("#keyword").focus();
    });
});

/**
 *
 * @param {type} formid   form表单ID
 * @param {type} type     返回类型  reload 刷新当前界面   default 保持默认状态不做刷新
 * @param {type} url      跳转的连接地址
 * @param {type} time     跳转的时间
 * @returns {undefined}
 */
function sc_ajaxpost(formid,type,url,waittime){
	if (typeof(waittime) == "undefined"){
		waittime = 1000;
	}
	var _form = $("#"+formid);

	var layerIndex = layer.load(1, {
		shade: [0.3,'#000'] //0.1透明度的白色背景
	});
	$.ajax({
		type: "POST",
		url: _form.attr('action'),
		data: _form.serialize(),
		dataType:"json",
		success: function (res) {
			layer.close(layerIndex);
			layer.msg(res.message || res.msg, {time: waittime}, function () {
				console.log(res);
				if (res.code == 10000 || res.code == 1) {
					if (typeof (type) == 'undefined' && type == null && type == '') {
						location.reload();
					} else if(typeof type == 'function') {
						type(res);
					} else if(type=='parent_reload') {
						parent.location.reload();
					} else if(type=='back') {
						history.go(-1);
					} else if(type=='url') {
						url = url || res.url;
						if(url) {
							location.href = url;
						}
					} else if(type=='default') {
						//不做操作只显示
					}else{
						location.reload();
					}
				}
			});
		}
	});
}



function sc_ajaxpost_offline_order(formid,waittime){
    if (typeof(waittime) == "undefined"){
        waittime = 1000;
    }
    var _form = $("#"+formid);

    var layerIndex = layer.load(1, {
        shade: [0.3,'#000'] //0.1透明度的白色背景
    });
    $.ajax({
        type: "POST",
        url: _form.attr('action'),
        data: _form.serialize(),
        dataType:"json",
        success: function (res) {
            layer.msg(res.message || res.msg, {time: waittime}, function () {
                if (res.code == 10000 || res.code == 1) {
                    // 刷新子窗口
                    $(window.parent.document).find("iframe")[1].contentWindow.location.reload(true);
                }else{
                    layer.close(layerIndex);
                }
            });
        }
    });
}

/**
 *
 * @param {type} url   URL链接地址
 * @param {type} type  返回类型  reload  remove(移除指定行) default默认不做任何操作
 * @param {type} param 参数
 * @returns {undefined}
 */
function sc_ajaxget(url,type,param)
{
    $.ajax({
        url: url,
        type: "get",
        dataType: "json",
        success: function (data) {
            layer.msg(data.message, {time: 1000}, function () {
                if (data.code == 10000) {
                    if (typeof (type) == "undefined" || type == null || type == '' || type=='reload') {
                        location.reload();
                    } else if (type == "remove") {
                        $("#sc_row_" + param).remove();
                    }else {
                        //不做操作
                    }
                }
            });
        }
    });
}
/**
 *
 * @param {type} url   URL链接地址
 * @param {type} msg   显示提示内容
 * @param {type} type  返回类型  reload  remove(移除指定行) default默认不做任何操作
 * @param {type} param 参数
 * @returns {undefined}
 */
function sc_ajaxget_confirm(url,msg,type,param) {
    if (typeof (msg) != 'undefined' && msg != null && msg != '') {
        layer.confirm(msg, {
            btn: ['确定', '取消'],
            title: false,
        }, function () {
            sc_ajaxget(url,type,param);
        });
    }else{
        sc_ajaxget(url,type,param);
    }
}

/**
 *
 * @param {type} msg  显示提示
 * @param {type} url  跳转URL
 * @returns {undefined}
 */
function sc_get_confirm(msg, url){
    if(msg != ''){
        layer.confirm(msg, {
            btn: ['确定', '取消'],
            title: false,
        }, function () {
            window.location = url;
        });
    }else{
    	window.location = url;
    }
}


function go(url){
    window.location = url;
}


/* 格式化金额 */
function price_format(price){
    if(typeof(PRICE_FORMAT) == 'undefined'){
        PRICE_FORMAT = '&yen;%s';
    }
    price = number_format(price, 2);
    return price;
}
function number_format(num, ext){
    if(ext < 0){
        return num;
    }
    num = Number(num);
    if(isNaN(num)){
        num = 0;
    }
    var _str = num.toString();
    var _arr = _str.split('.');
    var _int = _arr[0];
    var _flt = _arr[1];
    if(_str.indexOf('.') == -1){
        /* 找不到小数点，则添加 */
        if(ext == 0){
            return _str;
        }
        var _tmp = '';
        for(var i = 0; i < ext; i++){
            _tmp += '0';
        }
        _str = _str + '.' + _tmp;
    }else{
        if(_flt.length == ext){
            return _str;
        }
        /* 找得到小数点，则截取 */
        if(_flt.length > ext){
            _str = _str.substr(0, _str.length - (_flt.length - ext));
            if(ext == 0){
                _str = _int;
            }
        }else{
            for(var i = 0; i < ext - _flt.length; i++){
                _str += '0';
            }
        }
    }

    return _str;
}

/* 火狐下取本地全路径 */
function getFullPath(obj)
{
    if(obj)
    {
        //ie
        if (window.navigator.userAgent.indexOf("MSIE")>=1)
        {
            obj.select();
            if(window.navigator.userAgent.indexOf("MSIE") == 25){
                obj.blur();
            }
            return document.selection.createRange().text;
        }
        //firefox
        else if(window.navigator.userAgent.indexOf("Firefox")>=1)
        {
            if(obj.files)
            {
                //return obj.files.item(0).getAsDataURL();
                return window.URL.createObjectURL(obj.files.item(0));
            }
            return obj.value;
        }
        return obj.value;
    }
}
/* 转化JS跳转中的 ＆ */
function transform_char(str)
{
    if(str.indexOf('&'))
    {
        str = str.replace(/&/g, "%26");
    }
    return str;
}

//图片垂直水平缩放裁切显示
(function($){
    $.fn.VMiddleImg = function(options) {
        var defaults={
            "width":null,
            "height":null
        };
        var opts = $.extend({},defaults,options);
        return $(this).each(function() {
            var $this = $(this);
            var objHeight = $this.height(); //图片高度
            var objWidth = $this.width(); //图片宽度
            var parentHeight = opts.height||$this.parent().height(); //图片父容器高度
            var parentWidth = opts.width||$this.parent().width(); //图片父容器宽度
            var ratio = objHeight / objWidth;
            if (objHeight > parentHeight && objWidth > parentWidth) {
                if (objHeight > objWidth) { //赋值宽高
                    $this.width(parentWidth);
                    $this.height(parentWidth * ratio);
                } else {
                    $this.height(parentHeight);
                    $this.width(parentHeight / ratio);
                }
                objHeight = $this.height(); //重新获取宽高
                objWidth = $this.width();
                if (objHeight > objWidth) {
                    $this.css("top", (parentHeight - objHeight) / 2);
                    //定义top属性
                } else {
                    //定义left属性
                    $this.css("left", (parentWidth - objWidth) / 2);
                }
            }
            else {
                if (objWidth > parentWidth) {
                    $this.css("left", (parentWidth - objWidth) / 2);
                }
                $this.css("top", (parentHeight - objHeight) / 2);
            }
        });
    };
})(jQuery);
function ResizeImage(ImgD,FitWidth,FitHeight){
    var image=new Image();
    image.src=ImgD.src;
    if(image.width>0 && image.height>0)
    {
        if(image.width/image.height>= FitWidth/FitHeight)
        {
            if(image.width>FitWidth)
            {
                ImgD.width=FitWidth;
                ImgD.height=(image.height*FitWidth)/image.width;
            }
            else
            {
                ImgD.width=image.width;
                ImgD.height=image.height;
            }
        }
        else
        {
            if(image.height>FitHeight)
            {
                ImgD.height=FitHeight;
                ImgD.width=(image.width*FitHeight)/image.height;
            }
            else
            {
                ImgD.width=image.width;
                ImgD.height=image.height;
            }
        }
    }
}

function trim(str) {
    return (str + '').replace(/(\s+)$/g, '').replace(/^\s+/g, '');
}
//弹出框登录
function login_dialog(){
    CUR_DIALOG = ajax_form('login','登录',HOMESITEURL+'/Login/login.html?inajax=1',360,1);
}

/* 显示Ajax表单 */
function ajax_form(id, title, url, width, model)
{
    if (!width)	width = 480;
    if (!model) model = 1;
    var d = DialogManager.create(id);
    d.setTitle(title);
    d.setContents('ajax', url);
    d.setWidth(width);
    d.show('center',model);
    return d;
}
//显示一个内容为自定义HTML内容的消息
function html_form(id, title, _html, width, model) {
    if (!width)	width = 480;
    if (!model) model = 0;
    var d = DialogManager.create(id);
    d.setTitle(title);
    d.setContents(_html);
    d.setWidth(width);
    d.show('center',model);
    return d;
}
//收藏商品js
function collect_goods(fav_id, jstype, jsobj) {
    $.get(HOMESITEURL+'/Index/login.html', function(result) {
        if (result == '0') {
            login_dialog();
        } else {
            var url = HOMESITEURL+'/Memberfavorites/favoritesgoods';
            $.getJSON(url, {'fid': fav_id}, function(data) {
                $('.sc-handle a i').addClass("favorite-color");
                if (data.done)
                {
                    layer.msg(data.msg);
                    if (jstype == 'count') {
                        $('[sctype="' + jsobj + '"]').each(function() {
                            $(this).html(parseInt($(this).text()) + 1);
                        });
                    }
                    if (jstype == 'succ') {
                        $('[sctype="' + jsobj + '"]').each(function() {
                            $(this).html("收藏成功");
                        });
                    }
                }
                else
                {
                    layer.msg(data.msg);
                }
            });
        }
    });
}

//加载购物车信息
function load_cart_information() {
    $.getJSON(HOMESITEURL + '/Cart/ajax_load', function(result) {
        var obj = $('.header .user_menu .my-cart');
        var mini =$('#rtoolbar_cartlist');
        if (result) {
            var html = '';
            if (result.cart_goods_num > 0) {
                for (var i in result['list']) {
                    var goods = result['list'][i];
                    html += '<dl id="cart_item_' + goods['cart_id'] + '"><dt class="goods-name"><a href="' + goods['goods_url'] + '">' + goods['goods_name'] + '</a></dt>';
                    html += '<dd class="goods-thumb"><a href="' + goods['goods_url'] + '" title="' + goods['goods_name'] + '"><img src="' + goods['goods_image'] + '"></a></dd>';
                    html += '<dd class="goods-sales"></dd>';
                    html += '<dd class="goods-price"><em>&yen;' + goods['goods_price'] + '×' + goods['goods_num'] + '</dd>';
                    html += '<dd class="handle"><a href="javascript:void(0);" onClick="drop_topcart_item(' + goods['cart_id'] + ',' + goods['goods_id'] + ');">删除</a></dd>';
                    html += "</dl>";
                }
                obj.find('.incart-goods').html(html);
                obj.find('.incart-goods-box').perfectScrollbar('destroy');
                obj.find('.incart-goods-box').perfectScrollbar({suppressScrollX: true});
                html = "共<i>" + result.cart_goods_num + "</i>种商品&nbsp;&nbsp;总计金额：<em>&yen;" + result.cart_all_price + "</em>";
                obj.find('.total-price').html(html);
                mini.find('.total-price').html('<p>共<em class="goods-price" style="margin-left: 5px">' + result.cart_goods_num + '</em>种商品</p><p>共计：<em class="goods-price">&yen;' + result.cart_all_price + '</em></p>');
                if (obj.find('.addcart-goods-num').size() == 0) {
                    obj.append('<div class="addcart-goods-num">0</div>');
                }
                obj.find('.addcart-goods-num').html(result.cart_goods_num);
                $('#rtoobar_cart_count').html(result.cart_goods_num).show();
            } else {
                html = "<div class='no-order'><span>您的购物车中暂无商品，赶快选择心爱的商品吧！</span></div>";
                obj.find('.incart-goods').html(html);
                mini.find('.total-price').html(html);
                obj.find('.total-price').html('');
                if (obj.find('.addcart-goods-num').size() == 0) {
                    obj.append('<div class="addcart-goods-num">0</div>');
                }
                obj.find('.addcart-goods-num').html(result.cart_goods_num);
                $('#rtoobar_cart_count').html('').hide();
            }
        }
    });
}

//头部删除购物车信息，登录前使用goods_id,登录后使用cart_id
function drop_topcart_item(cart_id, goods_id) {
    $.getJSON(HOMESITEURL + '/Cart/del',{'cart_id': cart_id, 'goods_id': goods_id}, function(result) {
        if (result.state == 'true') {
            $("[sc_type='cart_item_"+cart_id+"']").remove();
            load_cart_information();
        } else {
            alert(result.msg);
        }
    });
}

//加载最近浏览的商品
function load_history_information(){
    $.getJSON(HOMESITEURL+'/Index/viewed_info.html', function(result){
        var obj = $('.header .user_menu .my-mall');
        if(result['m_id'] >0){
            if (typeof result['consult'] !== 'undefined') obj.find('#member_consult').html(result['consult']);
            if (typeof result['consult'] !== 'undefined') obj.find('#member_voucher').html(result['voucher']);
        }
        var goods_id = 0;
        var text_append = '';
        var n = 0;
        if (typeof result['viewed_goods'] !== 'undefined') {
            for (goods_id in result['viewed_goods']) {
                var goods = result['viewed_goods'][goods_id];
                text_append += '<li class="goods-thumb"><a href="'+goods['url']+'" title="'+goods['goods_name']+
                '" target="_blank"><img src="'+goods['goods_image']+'" alt="'+goods['goods_name']+'"></a>';
                text_append += '</li>';
                n++;
                if (n > 4) break;
            }
        }
        if (text_append == '') text_append = '<li class="no-goods">暂无商品</li>';;
        obj.find('.browse-history ul').html(text_append);
    });
}

/*
 * 弹出窗口
 */
(function($) {
    $.fn.sc_show_dialog = function(options) {

        var that = $(this);
        var settings = $.extend({}, {width: 480, title: '', close_callback: function() {}}, options);

        var init_dialog = function(title) {
            var _div = that;
            that.addClass("dialog_wrapper");
            that.wrapInner(function(){
                return '<div class="dialog_content">';
            });
            that.wrapInner(function(){
                return '<div class="dialog_body" style="position: relative;">';
            });
            that.find('.dialog_body').prepend('<h3 class="dialog_head" style="cursor: move;"><span class="dialog_title"><span class="dialog_title_icon">'+settings.title+'</span></span><span class="dialog_close_button">X</span></h3>');
            that.append('<div style="clear:both;"></div>');

            $(".dialog_close_button").click(function(){
                settings.close_callback();
                _div.hide();
            });

            that.draggable({handle: ".dialog_head"});
        };

        if(!$(this).hasClass("dialog_wrapper")) {
            init_dialog(settings.title);
        }
        settings.left = $(window).scrollLeft() + ($(window).width() - settings.width) / 2;
        settings.top  = ($(window).height() - $(this).height()) / 2;
        $(this).attr("style","display:none; z-index: 1100; position: fixed; width: "+settings.width+"px; left: "+settings.left+"px; top: "+settings.top+"px;");
        $(this).show();

    };
})(jQuery);





(function($) {
    $.fn.sc_region = function(options) {
        var $region = $(this);
        var settings = $.extend({}, {
            area_id: 0,
            region_span_class: "_region_value",
            src: "cache",
            show_deep: 0,
            btn_style_html: "",
            tip_type: ""
        }, options);
        settings.islast = false;
        settings.selected_deep = 0;
        settings.last_text = "";
        this.each(function() {
            var $inputArea = $(this);
            if ($inputArea.val() === "") {
                initArea($inputArea)
            } else {
                var $region_span = $('<span id="_area_span" class="' + settings.region_span_class + '">' + $inputArea.val() + "</span>");
                var $region_btn = $('<input type="button" class="input-btn" ' + settings.btn_style_html + ' value="编辑" />');
                $inputArea.after($region_span);
                $region_span.after($region_btn);
                $region_btn.on("click", function() {
                    $region_span.remove();
                    $region_btn.remove();
                    initArea($inputArea)
                });
                settings.islast = true
            }
            this.settings = settings;
            if ($inputArea.val() && /^\d+$/.test($inputArea.val())) {
                $.getJSON(HOMESITEURL  + "/Index/json_area_show?area_id=" + $inputArea.val() + "&callback=?", function(data) {
                    $("#_area_span").html(data.text == null ? "无" : data.text)
                })
            }
        });

        function initArea($inputArea) {
            settings.$area = $("<select></select>");
            $inputArea.before(settings.$area);
            loadAreaArray(function() {
                loadArea(settings.$area, settings.area_id)
            })
        }
        function loadArea($area, area_id) {
            if ($area && sc_a[area_id].length > 0) {
                var areas = [];
                areas = sc_a[area_id];
                if (settings.tip_type && settings.last_text != "") {
                    $area.append("<option value=''>" + settings.last_text + "(*)</option>")
                } else {
                    $area.append("<option value=''>-请选择-</option>")
                }
                for (i = 0; i < areas.length; i++) {
                    $area.append("<option value='" + areas[i][0] + "'>" + areas[i][1] + "</option>")
                }
                settings.islast = false
            }
            $area.on("change", function() {
                var region_value = "",
                    area_ids = [],
                    selected_deep = 1;
                $(this).nextAll("select").remove();
                $region.parent().find("select").each(function() {
                    if ($(this).find("option:selected").val() != "") {
                        region_value += $(this).find("option:selected").text() + " ";
                        area_ids.push($(this).find("option:selected").val())
                    }
                });
                settings.selected_deep = area_ids.length;
                settings.area_ids = area_ids.join(" ");
                $region.val(region_value);
                settings.area_id_1 = area_ids[0] ? area_ids[0] : "";
                settings.area_id_2 = area_ids[1] ? area_ids[1] : "";
                settings.area_id_3 = area_ids[2] ? area_ids[2] : "";
                settings.area_id_4 = area_ids[3] ? area_ids[3] : "";
                settings.last_text = $region.prevAll("select").find("option:selected").last().text();
                var area_id = settings.area_id = $(this).val();
                if ($('#_area_1').length > 0) $("#_area_1").val(settings.area_id_1);
                if ($('#_area_2').length > 0) $("#_area_2").val(settings.area_id_2);
                if ($('#_area_3').length > 0) $("#_area_3").val(settings.area_id_3);
                if ($('#_area_4').length > 0) $("#_area_4").val(settings.area_id_4);
                if ($('#_area').length > 0) $("#_area").val(settings.area_id);
                if ($('#_areas').length > 0) $("#_areas").val(settings.area_ids);
                if (settings.show_deep > 0 && $region.prevAll("select").size() == settings.show_deep) {
                    settings.islast = true;
                    if (typeof settings.last_click == 'function') {
                        settings.last_click(area_id);
                    }
                    return
                }
                if (area_id > 0) {
                    if (sc_a[area_id] && sc_a[area_id].length > 0) {
                        var $newArea = $("<select></select>");
                        $(this).after($newArea);
                        loadArea($newArea, area_id);
                        settings.islast = false
                    } else {
                        settings.islast = true;
                        if (typeof settings.last_click == 'function') {
                            settings.last_click(area_id);
                        }
                    }
                } else {
                    settings.islast = false
                }
                if ($('#islast').length > 0) $("#islast").val("");
            })
        }
        function loadAreaArray(callback) {
            if (typeof sc_a === "undefined") {
                $.getJSON(HOMESITEURL  + "/Index/json_area.html?src=" + settings.src + "&callback=?", function(data) {
                    sc_a = data;
                    callback()
                })
            } else {
                callback()
            }
        }
        if (typeof jQuery.validator != 'undefined') {
            jQuery.validator.addMethod("checklast", function(value, element) {
                return $(element).fetch('islast');
            }, "请将地区选择完整");
        }
    };
    $.fn.fetch = function(k) {
        var p;
        this.each(function() {
            if (this.settings) {
                p = eval("this.settings." + k);
                return false
            }
        });
        return p
    }


})(jQuery);

/* 加入购物车 */
function addcart(goods_id, quantity, callbackfunc,dir) {
    if(!quantity) return;
    var url = HOMESITEURL + '/Cart/add.html';
    quantity = parseInt(quantity);
    $.getJSON(url, {'goods_id': goods_id, 'quantity': quantity}, function(data) {
        if (data != null) {
            if (data.state) {
                if (callbackfunc) {
                    eval(callbackfunc + "(data)");
                }
                // 头部加载购物车信息
                load_cart_information();
                $("#rtoolbar_cartlist").load(HOMESITEURL + '/Cart/ajax_load?type=html');
                if(dir) {
                    layer.msg('添加购物车成功');
                }
            } else {
                layer.msg(data.msg);
            }
        }
    });
}

function setCookie(name, value, days) {
    var exp = new Date();
    exp.setTime(exp.getTime() + days * 24 * 60 * 60 * 1000);
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}
function getCookie(name) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (arr != null) {
        return unescape(arr[2]);
        return null;
    }
}
function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null) {
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
    }
}


(function($) {
    $.fn.F_slider = function(options){
        var defaults = {
            page : 1,
            len : 0,		// 滚动篇幅
            axis : 'y'		// y为上下滚动，x为左右滚动
        }
        var options = $.extend(defaults,options);
        return this.each(function(){
            var $this = $(this);
            var len = options.len;
            var page = options.page;
            if(options.axis == 'y'){
                var Val = $(this).find('.F-center').height();
                var Param = 'top';
            }else if(options.axis == 'x'){
                var Val = $(this).find('.F-center').parent().width();
                var Param = 'left';
            }
            $this.find('.F-prev').click(function(){
                if( page == 1){
                    eval("$this.find('.F-center').animate({"+Param+":'-=' + Val*(len-1)},'slow');");
                    page=len;
                }else{
                    eval("$this.find('.F-center').animate({"+Param+":'+=' + Val},'slow');");
                    page--;
                }
            });
            $this.find('.F-next').click(function(){
                if(page == len){
                    eval("$this.find('.F-center').animate({"+Param+":0},'slow');");
                    page=1;
                }else{
                    eval("$this.find('.F-center').animate({"+Param+":'-=' + Val},'show');");
                    page++;
                }
            });
        });
    }
})(jQuery);

/**
 * floatObj 包含加减乘除四个方法，能确保浮点数运算不丢失精度
 *
 * 我们知道计算机编程语言里浮点数计算会存在精度丢失问题（或称舍入误差），其根本原因是二进制和实现位数限制有些数无法有限表示
 * 以下是十进制小数对应的二进制表示
 *      0.1 >> 0.0001 1001 1001 1001…（1001无限循环）
 *      0.2 >> 0.0011 0011 0011 0011…（0011无限循环）
 * 计算机里每种数据类型的存储是一个有限宽度，比如 JavaScript 使用 64 位存储数字类型，因此超出的会舍去。舍去的部分就是精度丢失的部分。
 *
 * ** method **
 *  add / subtract / multiply /divide
 *
 * ** explame **
 *  0.1 + 0.2 == 0.30000000000000004 （多了 0.00000000000004）
 *  0.2 + 0.4 == 0.6000000000000001  （多了 0.0000000000001）
 *  19.9 * 100 == 1989.9999999999998 （少了 0.0000000000002）
 *
 * floatObj.add(0.1, 0.2) >> 0.3
 * floatObj.multiply(19.9, 100) >> 1990
 *
 */
var floatObj = function () {

    /*
     * 判断obj是否为一个整数
     */
    function isInteger(obj) {
        return Math.floor(obj) === obj
    }

    /*
     * 将一个浮点数转成整数，返回整数和倍数。如 3.14 >> 314，倍数是 100
     * @param floatNum {number} 小数
     * @return {object}
     *   {times:100, num: 314}
     */
    function toInteger(floatNum) {
        var ret = { times: 1, num: 0 }
        if (isInteger(floatNum)) {
            ret.num = floatNum
            return ret
        }
        var strfi = floatNum + ''
        var dotPos = strfi.indexOf('.')
        var len = strfi.substr(dotPos + 1).length
        var times = Math.pow(10, len)
        var intNum = parseInt(floatNum * times + 0.5, 10)
        ret.times = times
        ret.num = intNum
        return ret
    }

    /*
     * 核心方法，实现加减乘除运算，确保不丢失精度
     * 思路：把小数放大为整数（乘），进行算术运算，再缩小为小数（除）
     *
     * @param a {number} 运算数1
     * @param b {number} 运算数2
     * @param digits {number} 精度，保留的小数点数，比如 2, 即保留为两位小数
     * @param op {string} 运算类型，有加减乘除（add/subtract/multiply/divide）
     *
     */
    function operation(a, b, digits, op) {
        var o1 = toInteger(a)
        var o2 = toInteger(b)
        var n1 = o1.num
        var n2 = o2.num
        var t1 = o1.times
        var t2 = o2.times
        var max = t1 > t2 ? t1 : t2
        var result = null
        switch (op) {
            case 'add':
                if (t1 === t2) { // 两个小数位数相同
                    result = n1 + n2
                } else if (t1 > t2) { // o1 小数位 大于 o2
                    result = n1 + n2 * (t1 / t2)
                } else { // o1 小数位 小于 o2
                    result = n1 * (t2 / t1) + n2
                }
                return result / max
            case 'subtract':
                if (t1 === t2) {
                    result = n1 - n2
                } else if (t1 > t2) {
                    result = n1 - n2 * (t1 / t2)
                } else {
                    result = n1 * (t2 / t1) - n2
                }
                return result / max
            case 'multiply':
                result = (n1 * n2) / (t1 * t2)
                return result
            case 'divide':
                result = (n1 / n2) * (t2 / t1)
                return result
        }
    }

    // 加减乘除的四个接口
    function add(a, b, digits) { // 加
        return operation(a, b, digits, 'add')
    }
    function subtract(a, b, digits) { // 减
        return operation(a, b, digits, 'subtract')
    }
    function multiply(a, b, digits) { // 乘
        return operation(a, b, digits, 'multiply')
    }
    function divide(a, b, digits) { // 除
        return operation(a, b, digits, 'divide')
    }

    // exports
    return {
        add: add,
        subtract: subtract,
        multiply: multiply,
        divide: divide
    }
}();
